#include "CommandBuffer.hpp"
#include "Functions.hpp"
#include "VulkanError.hpp"

using namespace vulkan;


void CommandBuffer::Begin(const CommandBufferBeginInfo& info) const
{
	Result result = vkBeginCommandBuffer(*this, &info);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to begin recording for command buffer");
}

void CommandBuffer::End() const
{
	Result result = vkEndCommandBuffer(*this);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to end recording for command buffer");
}

void CommandBuffer::Reset(CommandBufferResetFlags flags) const
{
	Result result = vkResetCommandBuffer(*this, flags);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to reset command buffer");
}

void CommandBuffer::BindPipeline(PipelineBindPoint bindPoint, Pipeline pipeline) const
{
	vkCmdBindPipeline(*this, bindPoint, pipeline);
}

void CommandBuffer::SetViewport(uint32_t first, uint32_t count, const Viewport* pViewports) const
{
	vkCmdSetViewport(*this, first, count, pViewports);
}

void CommandBuffer::SetScissor(uint32_t first, uint32_t count, const Rectangle* pScissors) const
{
	vkCmdSetScissor(*this, first, count, pScissors);
}

void CommandBuffer::SetLineWidth(float width) const
{
	vkCmdSetLineWidth(*this, width);
}

void CommandBuffer::SetDepthBias(float constant, float clamp, float slope) const
{
	vkCmdSetDepthBias(*this, constant, clamp, slope);
}

void CommandBuffer::SetBlendConstants(float r, float g, float b, float a) const
{
	float blend_constants[4] = { r, g, b, a };
	vkCmdSetBlendConstants(*this, blend_constants);
}

void CommandBuffer::SetDepthBounds(float min, float max) const
{
	vkCmdSetDepthBounds(*this, min, max);
}

void CommandBuffer::SetStencilCompareMask(StencilFaceFlags faceMask, uint32_t compareMask) const
{
	vkCmdSetStencilCompareMask(*this, faceMask, compareMask);
}

void CommandBuffer::SetStencilWriteMask(StencilFaceFlags faceMask, uint32_t writeMask) const
{
	vkCmdSetStencilWriteMask(*this, faceMask, writeMask);
}

void CommandBuffer::SetStencilReference(StencilFaceFlags faceMask, uint32_t reference) const
{
	vkCmdSetStencilReference(*this, faceMask, reference);
}

void CommandBuffer::BindDescriptorSets(
	PipelineBindPoint                           bindPoint,
	PipelineLayout                              layout,
	uint32_t                                    firstSet,
	uint32_t                                    descriptorSetCount,
	const DescriptorSet*                        pSets,
	uint32_t                                    dynamicOffsetCount,
	const uint32_t*                             pDynamicOffsets) const
{
	vkCmdBindDescriptorSets(
		*this,
		bindPoint,
		layout,
		firstSet,
		descriptorSetCount,
		pSets,
		dynamicOffsetCount,
		pDynamicOffsets);
}

void CommandBuffer::BindIndexBuffer(Buffer buffer, DeviceSize offset, IndexType type) const
{
	vkCmdBindIndexBuffer(*this, buffer, offset, type);
}

void CommandBuffer::BindVertexBuffers(uint32_t first, uint32_t count, const Buffer* pBuffers, const DeviceSize* pOffsets) const
{
	vkCmdBindVertexBuffers(*this, first, count, pBuffers, pOffsets);
}

void CommandBuffer::Draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance) const
{
	vkCmdDraw(*this, vertexCount, instanceCount, firstVertex, firstInstance);
}

void CommandBuffer::DrawIndexed(
	uint32_t                                    indexCount,
	uint32_t                                    instanceCount,
	uint32_t                                    firstIndex,
	int32_t                                     vertexOffset,
	uint32_t                                    firstInstance) const
{
	vkCmdDrawIndexed(*this, indexCount, instanceCount, firstIndex, vertexOffset, firstInstance);
}

void CommandBuffer::DrawIndirect(Buffer buffer, DeviceSize offset, uint32_t drawCount, uint32_t stride) const
{
	vkCmdDrawIndirect(*this, buffer, offset, drawCount, stride);
}

void CommandBuffer::DrawIndexedIndirect(Buffer buffer, DeviceSize offset, uint32_t drawCount, uint32_t stride) const
{
	vkCmdDrawIndexedIndirect(*this, buffer, offset, drawCount, stride);
}

void CommandBuffer::Dispatch(uint32_t x, uint32_t y, uint32_t z) const
{
	vkCmdDispatch(*this, x, y, z);
}

void CommandBuffer::DispatchIndirect(Buffer buffer, DeviceSize offset) const
{
	vkCmdDispatchIndirect(*this, buffer, offset);
}

void CommandBuffer::CopyBuffer(Buffer srcBuffer, Buffer dstBuffer, uint32_t regionCount, const BufferCopy* pRegions) const
{
	vkCmdCopyBuffer(*this, srcBuffer, dstBuffer, regionCount, pRegions);
}

void CommandBuffer::CopyImage(
	Image                                       srcImage,
	ImageLayout                                 srcImageLayout,
	Image                                       dstImage,
	ImageLayout                                 dstImageLayout,
	uint32_t                                    regionCount,
	const ImageCopy*                            pRegions) const
{
	vkCmdCopyImage(*this, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
}

void CommandBuffer::BlitImage(
	Image                                       srcImage,
	ImageLayout                                 srcImageLayout,
	Image                                       dstImage,
	ImageLayout                                 dstImageLayout,
	uint32_t                                    regionCount,
	const ImageBlit*                            pRegions,
	Filter                                      filter) const
{
	vkCmdBlitImage(*this, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions, filter);
}

void CommandBuffer::CopyBufferToImage(
	Buffer                                      srcBuffer,
	Image                                       dstImage,
	ImageLayout                                 dstImageLayout,
	uint32_t                                    regionCount,
	const BufferImageCopy*                      pRegions) const
{
	vkCmdCopyBufferToImage(*this, srcBuffer, dstImage, dstImageLayout, regionCount, pRegions);
}

void CommandBuffer::CopyImageToBuffer(
	Image                                       srcImage,
	ImageLayout                                 srcImageLayout,
	Buffer                                      dstBuffer,
	uint32_t                                    regionCount,
	const BufferImageCopy*                      pRegions) const
{
	vkCmdCopyImageToBuffer(*this, srcImage, srcImageLayout, dstBuffer, regionCount, pRegions);
}

void CommandBuffer::UpdateBuffer(
	Buffer                                      dstBuffer,
	DeviceSize                                  dstOffset,
	DeviceSize                                  dataSize,
	const uint32_t*                             pData) const
{
	vkCmdUpdateBuffer(*this, dstBuffer, dstOffset, dataSize, pData);
}

void CommandBuffer::FillBuffer(
	Buffer                                      dstBuffer,
	DeviceSize                                  dstOffset,
	DeviceSize                                  size,
	uint32_t                                    data) const
{
	vkCmdFillBuffer(*this, dstBuffer, dstOffset, size, data);
}

void CommandBuffer::ClearColorImage(
	Image                                       image,
	ImageLayout                                 imageLayout,
	const ClearColorValue*                      pColor,
	uint32_t                                    rangeCount,
	const ImageSubresourceRange*                pRanges) const
{
	vkCmdClearColorImage(*this, image, imageLayout, pColor, rangeCount, pRanges);
}

void CommandBuffer::ClearDepthStencilImage(
	Image                                       image,
	ImageLayout                                 imageLayout,
	const ClearDepthStencilValue*               pDepthStencil,
	uint32_t                                    rangeCount,
	const ImageSubresourceRange*                pRanges) const
{
	vkCmdClearDepthStencilImage(*this, image, imageLayout, pDepthStencil, rangeCount, pRanges);
}

void CommandBuffer::ClearAttachments(
	uint32_t                                    attachmentCount,
	const ClearAttachment*                      pAttachments,
	uint32_t                                    rectCount,
	const ClearRectangle*                       pRects) const
{
	vkCmdClearAttachments(*this, attachmentCount, pAttachments, rectCount, pRects);
}

void CommandBuffer::ResolveImage(
	Image                                       srcImage,
	ImageLayout                                 srcImageLayout,
	Image                                       dstImage,
	ImageLayout                                 dstImageLayout,
	uint32_t                                    regionCount,
	const ImageResolve*                         pRegions) const
{
	vkCmdResolveImage(*this, srcImage, srcImageLayout, dstImage, dstImageLayout, regionCount, pRegions);
}

void CommandBuffer::SetEvent(Event event, PipelineStageFlags stageMask) const
{
	vkCmdSetEvent(*this, event, stageMask);
}

void CommandBuffer::ResetEvent(Event event, PipelineStageFlags stageMask) const
{
	vkCmdResetEvent(*this, event, stageMask);
}

void CommandBuffer::WaitEvents(
	uint32_t                                    eventCount,
	const Event*                                pEvents,
	PipelineStageFlags                          srcStageMask,
	PipelineStageFlags                          dstStageMask,
	uint32_t                                    memoryBarrierCount,
	const MemoryBarrier*                        pMemoryBarriers,
	uint32_t                                    bufferMemoryBarrierCount,
	const BufferMemoryBarrier*                  pBufferMemoryBarriers,
	uint32_t                                    imageMemoryBarrierCount,
	const ImageMemoryBarrier*                   pImageMemoryBarriers) const
{
	vkCmdWaitEvents(*this, eventCount, pEvents, srcStageMask, dstStageMask, memoryBarrierCount, pMemoryBarriers,
		bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
}

void CommandBuffer::PipelineBarrier(
	PipelineStageFlags                          srcStageMask,
	PipelineStageFlags                          dstStageMask,
	DependencyFlags                             dependencyFlags,
	uint32_t                                    memoryBarrierCount,
	const MemoryBarrier*                        pMemoryBarriers,
	uint32_t                                    bufferMemoryBarrierCount,
	const BufferMemoryBarrier*                  pBufferMemoryBarriers,
	uint32_t                                    imageMemoryBarrierCount,
	const ImageMemoryBarrier*                   pImageMemoryBarriers) const
{
	vkCmdPipelineBarrier(*this, srcStageMask, dstStageMask, dependencyFlags, memoryBarrierCount, pMemoryBarriers,
		bufferMemoryBarrierCount, pBufferMemoryBarriers, imageMemoryBarrierCount, pImageMemoryBarriers);
}

void CommandBuffer::BeginQuery(QueryPool queryPool, uint32_t query, QueryControlFlags flags) const
{
	vkCmdBeginQuery(*this, queryPool, query, flags);
}

void CommandBuffer::EndQuery(QueryPool queryPool, uint32_t query) const
{
	vkCmdEndQuery(*this, queryPool, query);
}

void CommandBuffer::ResetQueryPool(QueryPool queryPool, uint32_t firstQuery, uint32_t queryCount) const
{
	vkCmdResetQueryPool(*this, queryPool, firstQuery, queryCount);
}

void CommandBuffer::WriteTimestamp(PipelineStageFlagBits pipelineStage, QueryPool queryPool, uint32_t query) const
{
	vkCmdWriteTimestamp(*this, pipelineStage, queryPool, query);
}

void CommandBuffer::CopyQueryPoolResults(
	QueryPool                                   queryPool,
	uint32_t                                    firstQuery,
	uint32_t                                    queryCount,
	Buffer                                      dstBuffer,
	DeviceSize                                  dstOffset,
	DeviceSize                                  stride,
	QueryResultFlags                            flags) const
{
	vkCmdCopyQueryPoolResults(*this, queryPool, firstQuery, queryCount, dstBuffer, dstOffset, stride, flags);
}

void CommandBuffer::PushConstants(
	PipelineLayout                              layout,
	ShaderStageFlags                            stageFlags,
	uint32_t                                    offset,
	uint32_t                                    size,
	const void*                                 pValues) const
{
	vkCmdPushConstants(*this, layout, stageFlags, offset, size, pValues);
}

void CommandBuffer::BeginRenderPass(const RenderPassBeginInfo& pRenderPassBegin, SubpassContents contents) const
{
	vkCmdBeginRenderPass(*this, &pRenderPassBegin, contents);
}

void CommandBuffer::NextSubpass(SubpassContents contents) const
{
	vkCmdNextSubpass(*this, contents);
}

void CommandBuffer::EndRenderPass() const
{
	vkCmdEndRenderPass(*this);
}

void CommandBuffer::ExecuteCommands(uint32_t commandBufferCount, const CommandBuffer* pCommandBuffers) const
{
	vkCmdExecuteCommands(*this, commandBufferCount, pCommandBuffers);
}
