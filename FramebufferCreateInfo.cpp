#include "FramebufferCreateInfo.hpp"

using namespace vulkan;

FramebufferCreateInfo::FramebufferCreateInfo() :
	Structure(StructureType::FRAMEBUFFER_CREATE_INFO),
	flags(),
	renderPass(),
	attachmentCount(0),
	pAttachments(nullptr),
	width(0),
	height(0),
	layers(0)
{ }

FramebufferCreateInfo& FramebufferCreateInfo::Flags(FramebufferCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

FramebufferCreateInfo& FramebufferCreateInfo::RenderPass(vulkan::RenderPass handle)
{
	this->renderPass = handle;
	return *this;
}

FramebufferCreateInfo& FramebufferCreateInfo::Attachments(Extent2D extent, uint32_t layers, uint32_t count, const ImageView* pAttachments)
{
	this->width = extent.width;
	this->height = extent.height;
	this->layers = layers;
	this->attachmentCount = count;
	this->pAttachments = pAttachments;
	return *this;
}
