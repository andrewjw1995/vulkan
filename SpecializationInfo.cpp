#include "SpecializationInfo.hpp"

using namespace vulkan;

SpecializationInfo::SpecializationInfo() :
	mapEntryCount(0),
	pMapEntries(nullptr),
	dataSize(0),
	pData(nullptr)
{ }

SpecializationInfo& SpecializationInfo::MapEntries(uint32_t count, const SpecializationMapEntry* pEntries)
{
	this->mapEntryCount = count;
	this->pMapEntries = pEntries;
	return *this;
}

SpecializationInfo& SpecializationInfo::Data(size_t size, const void* pData)
{
	this->dataSize = size;
	this->pData = pData;
	return *this;
}