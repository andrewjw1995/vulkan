#include "ApplicationInfo.hpp"

using namespace std;
using namespace vulkan;

ApplicationInfo::ApplicationInfo(Version apiVersion) :
	Structure(StructureType::APPLICATION_INFO),
	pApplicationName(nullptr),
	applicationVersion(0),
	pEngineName(nullptr),
	engineVersion(0),
	apiVersion(apiVersion)
{ }

ApplicationInfo& ApplicationInfo::Application(const char* pName, Version version)
{
	pApplicationName = pName;
	applicationVersion = version;
	return *this;
}

ApplicationInfo& ApplicationInfo::ApplicationName(const char* pName)
{
	pApplicationName = pName;
	return *this;
}

ApplicationInfo& ApplicationInfo::ApplicationVersion(Version version)
{
	applicationVersion = version;
	return *this;
}

ApplicationInfo& ApplicationInfo::Engine(const char* pName, Version version)
{
	pEngineName = pName;
	engineVersion = version;
	return *this;
}

ApplicationInfo& ApplicationInfo::EngineName(const char* pName)
{
	pEngineName = pName;
	return *this;
}

ApplicationInfo& ApplicationInfo::EngineVersion(Version version)
{
	engineVersion = version;
	return *this;
}

const char* const& ApplicationInfo::ApplicationName() const
{
	return pApplicationName;
}

const Version& ApplicationInfo::ApplicationVersion() const
{
	return reinterpret_cast<const Version&>(applicationVersion);
}

const char* const& ApplicationInfo::EngineName() const
{
	return pEngineName;
}

const Version& ApplicationInfo::EngineVersion() const
{
	return reinterpret_cast<const Version&>(engineVersion);
}

const Version& ApplicationInfo::APIVersion() const
{
	return reinterpret_cast<const Version&>(apiVersion);
}
