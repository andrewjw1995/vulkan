#pragma once

#include "Instance.hpp"
#include "Win32Window.hpp"
#include "WindowCreateInfo.hpp"
#include "WindowListener.hpp"

namespace vulkan
{
	class Win32WindowFactory
	{
	public:
		Win32WindowFactory(Instance instance);
		Win32Window* CreateWindow(const WindowCreateInfo& info, WindowListener& listener);

		Instance instance;
		HINSTANCE hinstance;
	};
}