#pragma once

#include <cstdint>

namespace vulkan
{
	struct Offset2D
	{
	public:
		Offset2D();
		Offset2D(uint32_t x, uint32_t y);

		uint32_t x;
		uint32_t y;
	};
}