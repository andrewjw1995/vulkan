#pragma once

#include "Extent3D.hpp"
#include "ImageSubresourceLayers.hpp"
#include "Offset3D.hpp"

namespace vulkan
{
	class ImageCopy
	{
	public:
		ImageCopy();

		ImageCopy& Source(ImageSubresourceLayers subresource, Offset3D offset);
		ImageCopy& Destination(ImageSubresourceLayers subresource, Offset3D offset);
		ImageCopy& Extent(Extent3D extent);

	private:
		ImageSubresourceLayers    srcSubresource;
		Offset3D                  srcOffset;
		ImageSubresourceLayers    dstSubresource;
		Offset3D                  dstOffset;
		Extent3D                  extent;
	};
}