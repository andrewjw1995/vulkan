#pragma once

#include "DeviceSize.hpp"
#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"

namespace vulkan
{
	class SparseMemoryBind
	{
	public:
		SparseMemoryBind();

		SparseMemoryBind& ResourceOffset(DeviceSize offset);
		SparseMemoryBind& Size(DeviceSize size);
		SparseMemoryBind& Memory(DeviceMemory memory);
		SparseMemoryBind& MemoryOffset(DeviceSize offset);
		SparseMemoryBind& Flags(SparseMemoryBindFlags flags);
		DeviceSize               ResourceOffset() const;
		DeviceSize               Size() const;
		DeviceMemory             Memory() const;
		DeviceSize               MemoryOffset() const;
		SparseMemoryBindFlags    Flags() const;

	private:
		DeviceSize               resourceOffset;
		DeviceSize               size;
		DeviceMemory             memory;
		DeviceSize               memoryOffset;
		SparseMemoryBindFlags    flags;
	};
}
