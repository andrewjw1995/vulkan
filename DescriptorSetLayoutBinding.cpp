#include "DescriptorSetLayoutBinding.hpp"

using namespace vulkan;

DescriptorSetLayoutBinding::DescriptorSetLayoutBinding() :
	binding(0),
	descriptorType(DescriptorType::SAMPLER),
	descriptorCount(0),
	stageFlags(),
	pImmutableSamplers(nullptr)
{ }

DescriptorSetLayoutBinding& DescriptorSetLayoutBinding::Binding(uint32_t index)
{
	this->binding = binding;
	return *this;
}

DescriptorSetLayoutBinding& DescriptorSetLayoutBinding::Descriptors(DescriptorType type, uint32_t count)
{
	this->descriptorType = type;
	this->descriptorCount = count;
	this->pImmutableSamplers = nullptr;
	return *this;
}

DescriptorSetLayoutBinding& DescriptorSetLayoutBinding::Descriptors(DescriptorType type, uint32_t count, const Sampler* pImmutableSamplers)
{

	this->descriptorType = type;
	this->descriptorCount = count;
	this->pImmutableSamplers = pImmutableSamplers;
	return *this;
}

DescriptorSetLayoutBinding& DescriptorSetLayoutBinding::StageFlags(ShaderStageFlags flags)
{
	this->stageFlags = flags;
	return *this;
}
