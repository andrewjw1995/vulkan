#pragma once

#include "NonDispatchableHandle.hpp"
#include "SparseMemoryBind.hpp"

namespace vulkan
{
	class SparseBufferMemoryBindInfo
	{
	public:
		SparseBufferMemoryBindInfo();

		SparseBufferMemoryBindInfo& Buffer(Buffer buffer);
		SparseBufferMemoryBindInfo& Binds(uint32_t count, const SparseMemoryBind* pBinds);
		vulkan::Buffer Buffer() const;
		uint32_t BindCount() const;
		const SparseMemoryBind* Binds() const;

	private:
		vulkan::Buffer             buffer;
		uint32_t                   bindCount;
		const SparseMemoryBind*    pBinds;
	};
}