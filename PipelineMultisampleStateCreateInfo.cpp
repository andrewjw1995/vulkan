#include "PipelineMultisampleStateCreateInfo.hpp"

using namespace vulkan;

PipelineMultisampleStateCreateInfo::PipelineMultisampleStateCreateInfo() :
	Structure(StructureType::PIPELINE_MULTISAMPLE_STATE_CREATE_INFO),
	flags(),
	rasterizationSamples(SampleCountFlagBits::X1),
	sampleShadingEnable(false),
	minSampleShading(0),
	pSampleMask(nullptr),
	alphaToCoverageEnable(false),
	alphaToOneEnable(false)
{ }

PipelineMultisampleStateCreateInfo& PipelineMultisampleStateCreateInfo::Flags(PipelineMultisampleStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineMultisampleStateCreateInfo& PipelineMultisampleStateCreateInfo::RasterizationSamples(SampleCountFlagBits samples, const uint32_t* pSampleMask)
{
	this->rasterizationSamples = samples;
	this->pSampleMask = pSampleMask;
	return *this;
}

PipelineMultisampleStateCreateInfo& PipelineMultisampleStateCreateInfo::EnableSampleShading(float min)
{
	this->sampleShadingEnable = true;
	this->minSampleShading = min;
	return *this;
}

PipelineMultisampleStateCreateInfo& PipelineMultisampleStateCreateInfo::DisableSampleShading()
{
	this->sampleShadingEnable = false;
	return *this;
}

PipelineMultisampleStateCreateInfo& PipelineMultisampleStateCreateInfo::EnableAlphaToCoverage()
{
	this->alphaToCoverageEnable = true;
	return *this;
}

PipelineMultisampleStateCreateInfo& PipelineMultisampleStateCreateInfo::DisableAlphaToCoverage()
{
	this->alphaToCoverageEnable = false;
	return *this;
}

PipelineMultisampleStateCreateInfo& PipelineMultisampleStateCreateInfo::EnableAlphaToOne()
{
	this->alphaToOneEnable = true;
	return *this;
}

PipelineMultisampleStateCreateInfo& PipelineMultisampleStateCreateInfo::DisableAlphaToOne()
{
	this->alphaToOneEnable = false;
	return *this;
}
