#include "SwapchainCreateInfo.hpp"

using namespace vulkan;

SwapchainCreateInfo::SwapchainCreateInfo() :
	Structure(StructureType::SWAPCHAIN_CREATE_INFO_KHR),
	flags(),
	surface(),
	minImageCount(0),
	imageFormat(Format::UNDEFINED),
	imageColorSpace(ColorSpace::SRGB_NONLINEAR),
	imageExtent(),
	imageArrayLayers(0),
	imageUsage(),
	imageSharingMode(SharingMode::EXCLUSIVE),
	queueFamilyIndexCount(0),
	pQueueFamilyIndices(nullptr),
	preTransform(SurfaceTransformFlagBits::IDENTITY),
	compositeAlpha(CompositeAlphaFlagBits::OPAQUE),
	presentMode(PresentMode::IMMEDIATE),
	clipped(false),
	oldSwapchain()
{ }

SwapchainCreateInfo& SwapchainCreateInfo::Flags(SwapchainCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::Surface(vulkan::Surface surface)
{
	this->surface = surface;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::MinImageCount(uint32_t count)
{
	this->minImageCount = count;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::Image(SurfaceFormat format, Extent2D extent, uint32_t arrayLayers, ImageUsageFlags usage)
{
	this->imageFormat = format.Format();
	this->imageColorSpace = format.ColorSpace();
	this->imageExtent = extent;
	this->imageArrayLayers = arrayLayers;
	this->imageUsage = usage;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::Image(Format format, ColorSpace colorSpace, Extent2D extent, uint32_t arrayLayers, ImageUsageFlags usage)
{
	this->imageFormat = format;
	this->imageColorSpace = colorSpace;
	this->imageExtent = extent;
	this->imageArrayLayers = arrayLayers;
	this->imageUsage = usage;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::Exclusive()
{
	this->imageSharingMode = SharingMode::EXCLUSIVE;
	this->queueFamilyIndexCount = 0;
	this->pQueueFamilyIndices = nullptr;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::Concurrent(uint32_t count, const uint32_t* pQueueFamilyIndices)
{
	this->imageSharingMode = SharingMode::CONCURRENT;
	this->queueFamilyIndexCount = count;
	this->pQueueFamilyIndices = pQueueFamilyIndices;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::PreTransform(SurfaceTransformFlagBits transform)
{
	this->preTransform = transform;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::CompositeAlpha(CompositeAlphaFlagBits alpha)
{
	this->compositeAlpha = alpha;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::PresentMode(vulkan::PresentMode mode)
{
	this->presentMode = mode;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::Clipped(bool clipped)
{
	this->clipped = clipped;
	return *this;
}

SwapchainCreateInfo& SwapchainCreateInfo::OldSwapchain(Swapchain handle)
{
	this->oldSwapchain = handle;
	return *this;
}
