#pragma once

#include "Flags.hpp"

namespace vulkan
{
	class ImageSubresource
	{
	public:
		ImageSubresource();
		ImageSubresource(ImageAspectFlags aspectMask, uint32_t mipLevel, uint32_t arrayLayer);

		ImageSubresource& AspectMask(ImageAspectFlags mask);
		ImageSubresource& MipLevel(uint32_t level);
		ImageSubresource& ArrayLayer(uint32_t layer);
		ImageAspectFlags AspectMask() const;
		uint32_t         MipLevel() const;
		uint32_t         ArrayLayer() const;

	private:
		ImageAspectFlags aspectMask;
		uint32_t         mipLevel;
		uint32_t         arrayLayer;
	};
}