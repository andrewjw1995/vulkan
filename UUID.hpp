#pragma once

#include <cstdint>
#include <iostream>

namespace vulkan
{
	class UUID
	{
	public:
		static const unsigned int SIZE = 16;

		UUID();
		UUID(const uint8_t value[SIZE]);

		friend std::ostream& operator <<(std::ostream& out, const UUID& value);

	private:
		uint8_t value[SIZE];
	};
}
