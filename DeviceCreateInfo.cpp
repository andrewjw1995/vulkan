#include "DeviceCreateInfo.hpp"

using namespace vulkan;

DeviceCreateInfo::DeviceCreateInfo() :
	Structure(StructureType::DEVICE_CREATE_INFO),
	flags(0),
	queueCreateInfoCount(0),
	pQueueCreateInfos(nullptr),
	enabledLayerCount(0),
	ppEnabledLayerNames(nullptr),
	enabledExtensionCount(0),
	ppEnabledExtensionNames(nullptr),
	pEnabledFeatures(nullptr)
{ }

DeviceCreateInfo& DeviceCreateInfo::Flags(DeviceCreateFlags value)
{
	flags = value;
	return *this;
}

DeviceCreateInfo& DeviceCreateInfo::QueueCreateInfos(uint32_t count, const DeviceQueueCreateInfo* pInfos)
{
	queueCreateInfoCount = count;
	pQueueCreateInfos = pInfos;
	return *this;
}

DeviceCreateInfo& DeviceCreateInfo::EnabledLayers(uint32_t count, const char* const* ppNames)
{
	enabledLayerCount = count;
	ppEnabledLayerNames = ppNames;
	return *this;
}

DeviceCreateInfo& DeviceCreateInfo::EnabledExtensions(uint32_t count, const char* const* ppNames)
{
	enabledExtensionCount = count;
	ppEnabledExtensionNames = ppNames;
	return *this;
}

DeviceCreateInfo& DeviceCreateInfo::EnabledFeatures(const PhysicalDeviceFeatures* pFeatures)
{
	pEnabledFeatures = pFeatures;
	return *this;
}
