#include "QueueFamily.hpp"

using namespace vulkan;

QueueFlags QueueFamily::Flags() const
{
	return queueFlags;
}

uint32_t QueueFamily::QueueCount() const
{
	return queueCount;
}

uint32_t QueueFamily::TimestampValidBits() const
{
	return timestampValidBits;
}

Extent3D QueueFamily::MinImageTransferGranularity() const
{
	return minImageTransferGranularity;
}
