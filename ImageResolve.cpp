#include "ImageResolve.hpp"

using namespace vulkan;

ImageResolve::ImageResolve() :
	srcSubresource(),
	srcOffset(),
	dstSubresource(),
	dstOffset(),
	extent()
{ }

ImageResolve& ImageResolve::Source(ImageSubresourceLayers subresource, Offset3D offset)
{
	this->srcSubresource = subresource;
	this->srcOffset = offset;
	return *this;
}

ImageResolve& ImageResolve::Destination(ImageSubresourceLayers subresource, Offset3D offset)
{
	this->dstSubresource = subresource;
	this->dstOffset = offset;
	return *this;
}

ImageResolve& ImageResolve::Extent(Extent3D extent)
{
	this->extent = extent;
	return *this;
}
