#pragma once

#include "Enums.hpp"

namespace vulkan
{
	class StencilOpState
	{
	public:
		StencilOpState();

		StencilOpState& FailOp(StencilOp op);
		StencilOpState& PassOp(StencilOp op);
		StencilOpState& DepthFailOp(StencilOp op);
		StencilOpState& CompareOp(CompareOp op);
		StencilOpState& CompareMask(uint32_t mask);
		StencilOpState& WriteMask(uint32_t mask);
		StencilOpState& Reference(uint32_t reference);

	private:
		StencilOp         failOp;
		StencilOp         passOp;
		StencilOp         depthFailOp;
		vulkan::CompareOp compareOp;
		uint32_t          compareMask;
		uint32_t          writeMask;
		uint32_t          reference;
	};
}