#pragma once

#include "BindSparseInfo.hpp"
#include "DispatchableHandle.hpp"
#include "PresentInfo.hpp"
#include "SubmitInfo.hpp"

namespace vulkan
{
	class Queue : public DispatchableHandle
	{
	public:
		void Submit(uint32_t count, const SubmitInfo* pInfos, Fence fence) const;
		void BindSparse(uint32_t count, const BindSparseInfo* pBinds, Fence fence) const;
		void WaitIdle() const;
		void Present(const PresentInfo& info) const;
	};
}
