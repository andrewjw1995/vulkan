#pragma once

#include <iostream>

namespace vulkan
{
	class Version
	{
	public:
		Version(uint32_t value);
		Version(uint32_t major, uint32_t minor, uint32_t patch);

		uint32_t Major() const;
		uint32_t Minor() const;
		uint32_t Patch() const;

		operator uint32_t() const;
		friend std::ostream& operator <<(std::ostream& out, const Version& value);

		uint32_t value;
	};

	static_assert(sizeof(Version) == sizeof(uint32_t), "Size of Version wrapper does not match size of native type");
}
