#include "Layer.hpp"

using namespace vulkan;

Layer::Layer() :
	specVersion(0),
	implementationVersion(0)
{
	for (size_t i = 0; i < MAX_NAME_SIZE; i++)
		layerName[i] = '\0';
	for (size_t i = 0; i < MAX_DESCRIPTION_SIZE; i++)
		description[i] = '\0';
}

const char* Layer::Name() const
{
	return layerName;
}

Version Layer::SpecificationVersion() const
{
	return specVersion;
}

uint32_t Layer::ImplementationVersion() const
{
	return implementationVersion;
}

const char* Layer::Description() const
{
	return description;
}
