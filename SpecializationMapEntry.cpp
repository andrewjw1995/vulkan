#include "SpecializationMapEntry.hpp"

using namespace vulkan;

SpecializationMapEntry::SpecializationMapEntry(uint32_t constantID, uint32_t offset, size_t size) :
	constantID(constantID),
	offset(offset),
	size(size)
{ }
