#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class CommandPoolCreateInfo : public Structure
	{
	public:
		CommandPoolCreateInfo();

		CommandPoolCreateInfo& Flags(CommandPoolCreateFlags flags);
		CommandPoolCreateInfo& QueueFamily(uint32_t index);

	private:
		CommandPoolCreateFlags flags;
		uint32_t               queueFamilyIndex;
	};
}