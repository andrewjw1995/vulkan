#include "ImageCreateInfo.hpp"

using namespace vulkan;


ImageCreateInfo::ImageCreateInfo() :
	Structure(StructureType::IMAGE_CREATE_INFO),
	flags(),
	imageType(ImageType::X1D),
	extent(1, 1, 1),
	format(Format::UNDEFINED),
	mipLevels(1),
	arrayLayers(1),
	samples(SampleCountFlagBits::X1),
	tiling(ImageTiling::OPTIMAL),
	usage(),
	sharingMode(SharingMode::EXCLUSIVE),
	queueFamilyIndexCount(0),
	pQueueFamilyIndices(nullptr),
	initialLayout(ImageLayout::UNDEFINED)
{ }

ImageCreateInfo& ImageCreateInfo::Flags(ImageCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::X1D(uint32_t width)
{
	this->imageType = ImageType::X1D;
	this->extent = Extent3D(width, 1, 1);
	return *this;
}

ImageCreateInfo& ImageCreateInfo::X2D(uint32_t width, uint32_t height)
{
	this->imageType = ImageType::X2D;
	this->extent = Extent3D(width, height, 1);
	return *this;
}

ImageCreateInfo& ImageCreateInfo::X3D(uint32_t width, uint32_t height, uint32_t depth)
{
	this->imageType = ImageType::X3D;
	this->extent = Extent3D(width, height, depth);
	return *this;
}

ImageCreateInfo& ImageCreateInfo::Format(vulkan::Format format)
{
	this->format = format;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::MipLevels(uint32_t levels)
{
	this->mipLevels = levels;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::ArrayLayers(uint32_t layers)
{
	this->arrayLayers = layers;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::Samples(SampleCountFlagBits samples)
{
	this->samples = samples;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::Tiling(ImageTiling tiling)
{
	this->tiling = tiling;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::Usage(ImageUsageFlags usage)
{
	this->usage = usage;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::Exclusive()
{
	this->sharingMode = SharingMode::EXCLUSIVE;
	this->queueFamilyIndexCount = 0;
	this->pQueueFamilyIndices = nullptr;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::Concurrent(uint32_t count, uint32_t* pQueueFamilyIndices)
{
	this->sharingMode = SharingMode::CONCURRENT;
	this->queueFamilyIndexCount = count;
	this->pQueueFamilyIndices = pQueueFamilyIndices;
	return *this;
}

ImageCreateInfo& ImageCreateInfo::InitialLayout(ImageLayout layout)
{
	this->initialLayout = layout;
	return *this;
}