#include "InstanceCreateInfo.hpp"

using namespace std;
using namespace vulkan;

InstanceCreateInfo::InstanceCreateInfo(const ApplicationInfo& applicationInfo) :
	Structure(StructureType::INSTANCE_CREATE_INFO),
	flags(0),
	pApplicationInfo(&applicationInfo),
	enabledLayerCount(0),
	ppEnabledLayerNames(nullptr),
	enabledExtensionCount(0),
	ppEnabledExtensionNames(nullptr)
{ }

InstanceCreateInfo& InstanceCreateInfo::Flags(InstanceCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

InstanceCreateInfo& InstanceCreateInfo::EnabledLayers(uint32_t count, const char* const* ppNames)
{
	enabledLayerCount = count;
	ppEnabledLayerNames = ppNames;
	return *this;
}

InstanceCreateInfo& InstanceCreateInfo::EnabledExtensions(uint32_t count, const char* const* ppNames)
{
	enabledExtensionCount = count;
	ppEnabledExtensionNames = ppNames;
	return *this;
}
