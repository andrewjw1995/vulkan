#include "ComponentMapping.hpp"

using namespace vulkan;

ComponentMapping::ComponentMapping() :
	r(ComponentSwizzle::IDENTITY),
	g(ComponentSwizzle::IDENTITY),
	b(ComponentSwizzle::IDENTITY),
	a(ComponentSwizzle::IDENTITY)
{ }

ComponentMapping::ComponentMapping(ComponentSwizzle r, ComponentSwizzle g, ComponentSwizzle b, ComponentSwizzle a) :
	r(r),
	g(g),
	b(b),
	a(a)
{ }