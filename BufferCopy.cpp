#include "BufferCopy.hpp"

using namespace vulkan;

BufferCopy::BufferCopy(DeviceSize srcOffset, DeviceSize dstOffset, DeviceSize size) :
	srcOffset(srcOffset),
	dstOffset(dstOffset),
	size(size)
{ }
