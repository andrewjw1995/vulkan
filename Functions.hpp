#pragma once

#include "AllocationCallbacks.hpp"
#include "ApplicationInfo.hpp"
#include "BindSparseInfo.hpp"
#include "BufferCopy.hpp"
#include "BufferCreateInfo.hpp"
#include "BufferImageCopy.hpp"
#include "BufferMemoryBarrier.hpp"
#include "BufferViewCreateInfo.hpp"
#include "ClearAttachment.hpp"
#include "ClearRectangle.hpp"
#include "ClearValue.hpp"
#include "CommandBufferAllocateInfo.hpp"
#include "CommandBufferBeginInfo.hpp"
#include "CommandPoolCreateInfo.hpp"
#include "ComputePipelineCreateInfo.hpp"
#include "CopyDescriptorSet.hpp"
#include "DescriptorPoolCreateInfo.hpp"
#include "DescriptorSetAllocateInfo.hpp"
#include "DescriptorSetLayoutCreateInfo.hpp"
#include "Device.hpp"
#include "DeviceCreateInfo.hpp"
#include "Enums.hpp"
#include "EventCreateInfo.hpp"
#include "Extension.hpp"
#include "Extent2D.hpp"
#include "FenceCreateInfo.hpp"
#include "FormatProperties.hpp"
#include "FramebufferCreateInfo.hpp"
#include "GraphicsPipelineCreateInfo.hpp"
#include "ImageBlit.hpp"
#include "ImageCopy.hpp"
#include "ImageCreateInfo.hpp"
#include "ImageFormatProperties.hpp"
#include "ImageMemoryBarrier.hpp"
#include "ImageResolve.hpp"
#include "ImageViewCreateInfo.hpp"
#include "Instance.hpp"
#include "InstanceCreateInfo.hpp"
#include "Layer.hpp"
#include "MappedMemoryRange.hpp"
#include "MemoryAllocateInfo.hpp"
#include "MemoryBarrier.hpp"
#include "MemoryRequirements.hpp"
#include "NonDispatchableHandle.hpp"
#include "Offset2D.hpp"
#include "PhysicalDevice.hpp"
#include "PhysicalDeviceFeatures.hpp"
#include "PhysicalDeviceMemoryProperties.hpp"
#include "PhysicalDeviceProperties.hpp"
#include "PipelineCacheCreateInfo.hpp"
#include "PipelineLayoutCreateInfo.hpp"
#include "Platforms.hpp"
#include "PresentInfo.hpp"
#include "Queue.hpp"
#include "QueueFamily.hpp"
#include "Rectangle.hpp"
#include "RenderPassBeginInfo.hpp"
#include "RenderPassCreateInfo.hpp"
#include "SamplerCreateInfo.hpp"
#include "SemaphoreCreateInfo.hpp"
#include "ShaderModuleCreateInfo.hpp"
#include "SparseImageFormatProperties.hpp"
#include "SparseImageMemoryRequirements.hpp"
#include "SubmitInfo.hpp"
#include "SubresourceLayout.hpp"
#include "SurfaceCapabilities.hpp"
#include "SurfaceFormat.hpp"
#include "SwapchainCreateInfo.hpp"
#include "Win32SurfaceCreateInfo.hpp"
#include "WriteDescriptorSet.hpp"

#include <cstdint>

namespace vulkan
{
	extern "C"
	{
		VKAPI_ATTR Result VKAPI_CALL vkEnumerateInstanceExtensionProperties(
			const char*                                 pLayerName,
			uint32_t*                                   pPropertyCount,
			Extension*                                  pProperties);

		VKAPI_ATTR Result VKAPI_CALL vkEnumerateInstanceLayerProperties(
			uint32_t*                                   pPropertyCount,
			Layer*                                      pProperties);

		VKAPI_ATTR Result VKAPI_CALL vkCreateInstance(
			const InstanceCreateInfo*                   pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Instance*                                   pInstance);

		VKAPI_ATTR void VKAPI_CALL vkDestroyInstance(
			Instance                                    instance,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkEnumeratePhysicalDevices(
			Instance                                    instance,
			uint32_t*                                   pPhysicalDeviceCount,
			PhysicalDevice*                             pPhysicalDevices);

		VKAPI_ATTR Result VKAPI_CALL vkEnumerateDeviceExtensionProperties(
			PhysicalDevice                              physicalDevice,
			const char*                                 pLayerName,
			uint32_t*                                   pPropertyCount,
			Extension*                                  pProperties);

		VKAPI_ATTR Result VKAPI_CALL vkEnumerateDeviceLayerProperties(
			PhysicalDevice                              physicalDevice,
			uint32_t*                                   pPropertyCount,
			Layer*                                      pProperties);

		VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceFeatures(
			PhysicalDevice                              physicalDevice,
			PhysicalDeviceFeatures*                     pFeatures);

		VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceFormatProperties(
			PhysicalDevice                              physicalDevice,
			Format                                      format,
			FormatProperties*                           pFormatProperties);

		VKAPI_ATTR Result VKAPI_CALL vkGetPhysicalDeviceImageFormatProperties(
			PhysicalDevice                              physicalDevice,
			Format                                      format,
			ImageType                                   type,
			ImageTiling                                 tiling,
			ImageUsageFlags                             usage,
			ImageCreateFlags                            flags,
			ImageFormatProperties*                      pImageFormatProperties);

		VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceProperties(
			PhysicalDevice                              physicalDevice,
			PhysicalDeviceProperties*                   pProperties);

		VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceQueueFamilyProperties(
			PhysicalDevice                              physicalDevice,
			uint32_t*                                   pQueueFamilyPropertyCount,
			QueueFamily*                                pQueueFamilyProperties);

		VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceMemoryProperties(
			PhysicalDevice                              physicalDevice,
			PhysicalDeviceMemoryProperties*             pMemoryProperties);

		VKAPI_ATTR void VKAPI_CALL vkGetPhysicalDeviceSparseImageFormatProperties(
			PhysicalDevice                              physicalDevice,
			Format                                      format,
			ImageType                                   type,
			SampleCountFlagBits                         samples,
			ImageUsageFlags                             usage,
			ImageTiling                                 tiling,
			uint32_t*                                   pPropertyCount,
			SparseImageFormatProperties*                pProperties);

		VKAPI_ATTR Result VKAPI_CALL vkCreateDevice(
			PhysicalDevice                              physicalDevice,
			const DeviceCreateInfo*                     pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Device*                                     pDevice);

		VKAPI_ATTR void VKAPI_CALL vkDestroyDevice(
			Device                                      device,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR void VKAPI_CALL vkGetDeviceQueue(
			Device                                      device,
			uint32_t                                    queueFamilyIndex,
			uint32_t                                    queueIndex,
			Queue*                                      pQueue);

		VKAPI_ATTR Result VKAPI_CALL vkAllocateMemory(
			Device                                      device,
			const MemoryAllocateInfo*                   pAllocateInfo,
			const AllocationCallbacks*                  pAllocator,
			DeviceMemory*                               pMemory);

		VKAPI_ATTR void VKAPI_CALL vkFreeMemory(
			Device                                      device,
			DeviceMemory                                memory,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkMapMemory(
			Device                                      device,
			DeviceMemory                                memory,
			DeviceSize                                  offset,
			DeviceSize                                  size,
			MemoryMapFlags                              flags,
			void**                                      ppData);

		VKAPI_ATTR void VKAPI_CALL vkUnmapMemory(
			Device                                      device,
			DeviceMemory                                memory);

		VKAPI_ATTR Result VKAPI_CALL vkFlushMappedMemoryRanges(
			Device                                      device,
			uint32_t                                    memoryRangeCount,
			const MappedMemoryRange*                    pMemoryRanges);

		VKAPI_ATTR Result VKAPI_CALL vkInvalidateMappedMemoryRanges(
			Device                                      device,
			uint32_t                                    memoryRangeCount,
			const MappedMemoryRange*                    pMemoryRanges);

		VKAPI_ATTR void VKAPI_CALL vkGetDeviceMemoryCommitment(
			Device                                      device,
			DeviceMemory                                memory,
			DeviceSize*                                 pCommittedMemoryInBytes);

		VKAPI_ATTR Result VKAPI_CALL vkBindBufferMemory(
			Device                                      device,
			Buffer                                      buffer,
			DeviceMemory                                memory,
			DeviceSize                                  memoryOffset);

		VKAPI_ATTR Result VKAPI_CALL vkBindImageMemory(
			Device                                      device,
			Image                                       image,
			DeviceMemory                                memory,
			DeviceSize                                  memoryOffset);

		VKAPI_ATTR void VKAPI_CALL vkGetBufferMemoryRequirements(
			Device                                      device,
			Buffer                                      buffer,
			MemoryRequirements*                         pMemoryRequirements);

		VKAPI_ATTR void VKAPI_CALL vkGetImageMemoryRequirements(
			Device                                      device,
			Image                                       image,
			MemoryRequirements*                         pMemoryRequirements);

		VKAPI_ATTR void VKAPI_CALL vkGetImageSparseMemoryRequirements(
			Device                                      device,
			Image                                       image,
			uint32_t*                                   pSparseMemoryRequirementCount,
			SparseImageMemoryRequirements*              pSparseMemoryRequirements);

		VKAPI_ATTR Result VKAPI_CALL vkCreateFence(
			Device                                      device,
			const FenceCreateInfo*                      pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Fence*                                      pFence);

		VKAPI_ATTR void VKAPI_CALL vkDestroyFence(
			Device                                      device,
			Fence                                       fence,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkResetFences(
			Device                                      device,
			uint32_t                                    fenceCount,
			const Fence*                                pFences);

		VKAPI_ATTR Result VKAPI_CALL vkGetFenceStatus(
			Device                                      device,
			Fence                                       fence);

		VKAPI_ATTR Result VKAPI_CALL vkWaitForFences(
			Device                                      device,
			uint32_t                                    fenceCount,
			const Fence*                                pFences,
			Bool32                                      waitAll,
			uint64_t                                    timeout);

		VKAPI_ATTR Result VKAPI_CALL vkCreateSemaphore(
			Device                                      device,
			const SemaphoreCreateInfo*                  pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Semaphore*                                  pSemaphore);

		VKAPI_ATTR void VKAPI_CALL vkDestroySemaphore(
			Device                                      device,
			Semaphore                                   semaphore,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreateEvent(
			Device                                      device,
			const EventCreateInfo*                      pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Event*                                      pEvent);

		VKAPI_ATTR void VKAPI_CALL vkDestroyEvent(
			Device                                      device,
			Event                                       event,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkGetEventStatus(
			Device                                      device,
			Event                                       event);

		VKAPI_ATTR Result VKAPI_CALL vkSetEvent(
			Device                                      device,
			Event                                       event);

		VKAPI_ATTR Result VKAPI_CALL vkResetEvent(
			Device                                      device,
			Event                                       event);

		/*
		VKAPI_ATTR VkResult VKAPI_CALL vkCreateQueryPool(
			VkDevice                                    device,
			const VkQueryPoolCreateInfo*                pCreateInfo,
			const VkAllocationCallbacks*                pAllocator,
			VkQueryPool*                                pQueryPool);

		VKAPI_ATTR void VKAPI_CALL vkDestroyQueryPool(
			VkDevice                                    device,
			VkQueryPool                                 queryPool,
			const VkAllocationCallbacks*                pAllocator);

		VKAPI_ATTR VkResult VKAPI_CALL vkGetQueryPoolResults(
			VkDevice                                    device,
			VkQueryPool                                 queryPool,
			uint32_t                                    firstQuery,
			uint32_t                                    queryCount,
			size_t                                      dataSize,
			void*                                       pData,
			VkDeviceSize                                stride,
			VkQueryResultFlags                          flags);
		*/

		VKAPI_ATTR Result VKAPI_CALL vkCreateBuffer(
			Device                                      device,
			const BufferCreateInfo*                     pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Buffer*                                     pBuffer);

		VKAPI_ATTR void VKAPI_CALL vkDestroyBuffer(
			Device                                      device,
			Buffer                                      buffer,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreateBufferView(
			Device                                      device,
			const BufferViewCreateInfo*                 pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			BufferView*                                 pView);

		VKAPI_ATTR void VKAPI_CALL vkDestroyBufferView(
			Device                                      device,
			BufferView                                  bufferView,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreateImage(
			Device                                      device,
			const ImageCreateInfo*                      pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Image*                                      pImage);

		VKAPI_ATTR void VKAPI_CALL vkDestroyImage(
			Device                                      device,
			Image                                       image,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR void VKAPI_CALL vkGetImageSubresourceLayout(
			Device                                      device,
			Image                                       image,
			const ImageSubresource*                     pSubresource,
			SubresourceLayout*                          pLayout);

		VKAPI_ATTR Result VKAPI_CALL vkCreateImageView(
			Device                                      device,
			const ImageViewCreateInfo*                  pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			ImageView*                                  pView);

		VKAPI_ATTR void VKAPI_CALL vkDestroyImageView(
			Device                                      device,
			ImageView                                   imageView,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkDeviceWaitIdle(
			Device                                      device);

		VKAPI_ATTR Result VKAPI_CALL vkQueueSubmit(
			Queue                                       queue,
			uint32_t                                    submitCount,
			const SubmitInfo*                           pSubmits,
			Fence                                       fence);

		VKAPI_ATTR Result VKAPI_CALL vkQueueBindSparse(
			Queue                                       queue,
			uint32_t                                    bindInfoCount,
			const BindSparseInfo*                       pBindInfo,
			Fence                                       fence);

		VKAPI_ATTR Result VKAPI_CALL vkQueueWaitIdle(
			Queue                                       queue);

		VKAPI_ATTR Result VKAPI_CALL vkCreateShaderModule(
			Device                                      device,
			const ShaderModuleCreateInfo*               pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			ShaderModule*                               pShaderModule);

		VKAPI_ATTR void VKAPI_CALL vkDestroyShaderModule(
			Device                                      device,
			ShaderModule                                shaderModule,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreatePipelineCache(
			Device                                      device,
			const PipelineCacheCreateInfo*              pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			PipelineCache*                              pPipelineCache);

		VKAPI_ATTR void VKAPI_CALL vkDestroyPipelineCache(
			Device                                      device,
			PipelineCache                               pipelineCache,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkGetPipelineCacheData(
			Device                                      device,
			PipelineCache                               pipelineCache,
			size_t*                                     pDataSize,
			void*                                       pData);

		VKAPI_ATTR Result VKAPI_CALL vkMergePipelineCaches(
			Device                                      device,
			PipelineCache                               dstCache,
			uint32_t                                    srcCacheCount,
			const PipelineCache*                        pSrcCaches);

		VKAPI_ATTR Result VKAPI_CALL vkCreateGraphicsPipelines(
			Device                                      device,
			PipelineCache                               pipelineCache,
			uint32_t                                    createInfoCount,
			const GraphicsPipelineCreateInfo*           pCreateInfos,
			const AllocationCallbacks*                  pAllocator,
			Pipeline*                                   pPipelines);

		VKAPI_ATTR Result VKAPI_CALL vkCreateComputePipelines(
			Device                                      device,
			PipelineCache                               pipelineCache,
			uint32_t                                    createInfoCount,
			const ComputePipelineCreateInfo*            pCreateInfos,
			const AllocationCallbacks*                  pAllocator,
			Pipeline*                                   pPipelines);

		VKAPI_ATTR void VKAPI_CALL vkDestroyPipeline(
			Device                                      device,
			Pipeline                                    pipeline,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreatePipelineLayout(
			Device                                      device,
			const PipelineLayoutCreateInfo*             pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			PipelineLayout*                             pPipelineLayout);

		VKAPI_ATTR void VKAPI_CALL vkDestroyPipelineLayout(
			Device                                      device,
			PipelineLayout                              pipelineLayout,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreateSampler(
			Device                                      device,
			const SamplerCreateInfo*                    pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Sampler*                                    pSampler);

		VKAPI_ATTR void VKAPI_CALL vkDestroySampler(
			Device                                      device,
			Sampler                                     sampler,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreateDescriptorSetLayout(
			Device                                      device,
			const DescriptorSetLayoutCreateInfo*        pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			DescriptorSetLayout*                        pSetLayout);

		VKAPI_ATTR void VKAPI_CALL vkDestroyDescriptorSetLayout(
			Device                                      device,
			DescriptorSetLayout                         descriptorSetLayout,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreateDescriptorPool(
			Device                                      device,
			const DescriptorPoolCreateInfo*             pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			DescriptorPool*                             pDescriptorPool);

		VKAPI_ATTR void VKAPI_CALL vkDestroyDescriptorPool(
			Device                                      device,
			DescriptorPool                              descriptorPool,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkResetDescriptorPool(
			Device                                      device,
			DescriptorPool                              descriptorPool,
			DescriptorPoolResetFlags                    flags);

		VKAPI_ATTR Result VKAPI_CALL vkAllocateDescriptorSets(
			Device                                      device,
			const DescriptorSetAllocateInfo*            pAllocateInfo,
			DescriptorSet*                              pDescriptorSets);

		VKAPI_ATTR Result VKAPI_CALL vkFreeDescriptorSets(
			Device                                      device,
			DescriptorPool                              descriptorPool,
			uint32_t                                    descriptorSetCount,
			const DescriptorSet*                        pDescriptorSets);

		VKAPI_ATTR void VKAPI_CALL vkUpdateDescriptorSets(
			Device                                      device,
			uint32_t                                    descriptorWriteCount,
			const WriteDescriptorSet*                   pDescriptorWrites,
			uint32_t                                    descriptorCopyCount,
			const CopyDescriptorSet*                    pDescriptorCopies);

		VKAPI_ATTR Result VKAPI_CALL vkCreateFramebuffer(
			Device                                      device,
			const FramebufferCreateInfo*                pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Framebuffer*                                pFramebuffer);

		VKAPI_ATTR void VKAPI_CALL vkDestroyFramebuffer(
			Device                                      device,
			Framebuffer                                 framebuffer,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkCreateRenderPass(
			Device                                      device,
			const RenderPassCreateInfo*                 pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			RenderPass*                                 pRenderPass);

		VKAPI_ATTR void VKAPI_CALL vkDestroyRenderPass(
			Device                                      device,
			RenderPass                                  renderPass,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR void VKAPI_CALL vkGetRenderAreaGranularity(
			Device                                      device,
			RenderPass                                  renderPass,
			Extent2D*                                   pGranularity);

		VKAPI_ATTR Result VKAPI_CALL vkCreateCommandPool(
			Device                                      device,
			const CommandPoolCreateInfo*                pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			CommandPool*                                pCommandPool);

		VKAPI_ATTR void VKAPI_CALL vkDestroyCommandPool(
			Device                                      device,
			CommandPool                                 commandPool,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkResetCommandPool(
			Device                                      device,
			CommandPool                                 commandPool,
			CommandPoolResetFlags                       flags);

		VKAPI_ATTR Result VKAPI_CALL vkAllocateCommandBuffers(
			Device                                      device,
			const CommandBufferAllocateInfo*            pAllocateInfo,
			CommandBuffer*                              pCommandBuffers);

		VKAPI_ATTR void VKAPI_CALL vkFreeCommandBuffers(
			Device                                      device,
			CommandPool                                 commandPool,
			uint32_t                                    commandBufferCount,
			const CommandBuffer*                        pCommandBuffers);

		VKAPI_ATTR Result VKAPI_CALL vkBeginCommandBuffer(
			CommandBuffer                               commandBuffer,
			const CommandBufferBeginInfo*               pBeginInfo);

		VKAPI_ATTR Result VKAPI_CALL vkEndCommandBuffer(
			CommandBuffer                               commandBuffer);

		VKAPI_ATTR Result VKAPI_CALL vkResetCommandBuffer(
			CommandBuffer                               commandBuffer,
			CommandBufferResetFlags                     flags);

		VKAPI_ATTR void VKAPI_CALL vkCmdBindPipeline(
			CommandBuffer                               commandBuffer,
			PipelineBindPoint                           pipelineBindPoint,
			Pipeline                                    pipeline);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetViewport(
			CommandBuffer                               commandBuffer,
			uint32_t                                    firstViewport,
			uint32_t                                    viewportCount,
			const Viewport*                             pViewports);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetScissor(
			CommandBuffer                               commandBuffer,
			uint32_t                                    firstScissor,
			uint32_t                                    scissorCount,
			const Rectangle*                            pScissors);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetLineWidth(
			CommandBuffer                               commandBuffer,
			float                                       lineWidth);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetDepthBias(
			CommandBuffer                               commandBuffer,
			float                                       depthBiasConstantFactor,
			float                                       depthBiasClamp,
			float                                       depthBiasSlopeFactor);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetBlendConstants(
			CommandBuffer                               commandBuffer,
			const float                                 blendConstants[4]);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetDepthBounds(
			CommandBuffer                               commandBuffer,
			float                                       minDepthBounds,
			float                                       maxDepthBounds);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetStencilCompareMask(
			CommandBuffer                               commandBuffer,
			StencilFaceFlags                            faceMask,
			uint32_t                                    compareMask);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetStencilWriteMask(
			CommandBuffer                               commandBuffer,
			StencilFaceFlags                            faceMask,
			uint32_t                                    writeMask);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetStencilReference(
			CommandBuffer                               commandBuffer,
			StencilFaceFlags                            faceMask,
			uint32_t                                    reference);

		VKAPI_ATTR void VKAPI_CALL vkCmdBindDescriptorSets(
			CommandBuffer                               commandBuffer,
			PipelineBindPoint                           pipelineBindPoint,
			PipelineLayout                              layout,
			uint32_t                                    firstSet,
			uint32_t                                    descriptorSetCount,
			const DescriptorSet*                        pDescriptorSets,
			uint32_t                                    dynamicOffsetCount,
			const uint32_t*                             pDynamicOffsets);

		VKAPI_ATTR void VKAPI_CALL vkCmdBindIndexBuffer(
			CommandBuffer                               commandBuffer,
			Buffer                                      buffer,
			DeviceSize                                  offset,
			IndexType                                   indexType);

		VKAPI_ATTR void VKAPI_CALL vkCmdBindVertexBuffers(
			CommandBuffer                               commandBuffer,
			uint32_t                                    firstBinding,
			uint32_t                                    bindingCount,
			const Buffer*                               pBuffers,
			const DeviceSize*                           pOffsets);

		VKAPI_ATTR void VKAPI_CALL vkCmdDraw(
			CommandBuffer                               commandBuffer,
			uint32_t                                    vertexCount,
			uint32_t                                    instanceCount,
			uint32_t                                    firstVertex,
			uint32_t                                    firstInstance);

		VKAPI_ATTR void VKAPI_CALL vkCmdDrawIndexed(
			CommandBuffer                               commandBuffer,
			uint32_t                                    indexCount,
			uint32_t                                    instanceCount,
			uint32_t                                    firstIndex,
			int32_t                                     vertexOffset,
			uint32_t                                    firstInstance);

		VKAPI_ATTR void VKAPI_CALL vkCmdDrawIndirect(
			CommandBuffer                               commandBuffer,
			Buffer                                      buffer,
			DeviceSize                                  offset,
			uint32_t                                    drawCount,
			uint32_t                                    stride);

		VKAPI_ATTR void VKAPI_CALL vkCmdDrawIndexedIndirect(
			CommandBuffer                               commandBuffer,
			Buffer                                      buffer,
			DeviceSize                                  offset,
			uint32_t                                    drawCount,
			uint32_t                                    stride);

		VKAPI_ATTR void VKAPI_CALL vkCmdDispatch(
			CommandBuffer                               commandBuffer,
			uint32_t                                    x,
			uint32_t                                    y,
			uint32_t                                    z);

		VKAPI_ATTR void VKAPI_CALL vkCmdDispatchIndirect(
			CommandBuffer                               commandBuffer,
			Buffer                                      buffer,
			DeviceSize                                  offset);

		VKAPI_ATTR void VKAPI_CALL vkCmdCopyBuffer(
			CommandBuffer                               commandBuffer,
			Buffer                                      srcBuffer,
			Buffer                                      dstBuffer,
			uint32_t                                    regionCount,
			const BufferCopy*                           pRegions);

		VKAPI_ATTR void VKAPI_CALL vkCmdCopyImage(
			CommandBuffer                               commandBuffer,
			Image                                       srcImage,
			ImageLayout                                 srcImageLayout,
			Image                                       dstImage,
			ImageLayout                                 dstImageLayout,
			uint32_t                                    regionCount,
			const ImageCopy*                            pRegions);

		VKAPI_ATTR void VKAPI_CALL vkCmdBlitImage(
			CommandBuffer                               commandBuffer,
			Image                                       srcImage,
			ImageLayout                                 srcImageLayout,
			Image                                       dstImage,
			ImageLayout                                 dstImageLayout,
			uint32_t                                    regionCount,
			const ImageBlit*                            pRegions,
			Filter                                      filter);

		VKAPI_ATTR void VKAPI_CALL vkCmdCopyBufferToImage(
			CommandBuffer                               commandBuffer,
			Buffer                                      srcBuffer,
			Image                                       dstImage,
			ImageLayout                                 dstImageLayout,
			uint32_t                                    regionCount,
			const BufferImageCopy*                      pRegions);

		VKAPI_ATTR void VKAPI_CALL vkCmdCopyImageToBuffer(
			CommandBuffer                               commandBuffer,
			Image                                       srcImage,
			ImageLayout                                 srcImageLayout,
			Buffer                                      dstBuffer,
			uint32_t                                    regionCount,
			const BufferImageCopy*                      pRegions);

		VKAPI_ATTR void VKAPI_CALL vkCmdUpdateBuffer(
			CommandBuffer                               commandBuffer,
			Buffer                                      dstBuffer,
			DeviceSize                                  dstOffset,
			DeviceSize                                  dataSize,
			const uint32_t*                             pData);

		VKAPI_ATTR void VKAPI_CALL vkCmdFillBuffer(
			CommandBuffer                               commandBuffer,
			Buffer                                      dstBuffer,
			DeviceSize                                  dstOffset,
			DeviceSize                                  size,
			uint32_t                                    data);

		VKAPI_ATTR void VKAPI_CALL vkCmdClearColorImage(
			CommandBuffer                               commandBuffer,
			Image                                       image,
			ImageLayout                                 imageLayout,
			const ClearColorValue*                      pColor,
			uint32_t                                    rangeCount,
			const ImageSubresourceRange*                pRanges);

		VKAPI_ATTR void VKAPI_CALL vkCmdClearDepthStencilImage(
			CommandBuffer                               commandBuffer,
			Image                                       image,
			ImageLayout                                 imageLayout,
			const ClearDepthStencilValue*               pDepthStencil,
			uint32_t                                    rangeCount,
			const ImageSubresourceRange*                pRanges);

		VKAPI_ATTR void VKAPI_CALL vkCmdClearAttachments(
			CommandBuffer                               commandBuffer,
			uint32_t                                    attachmentCount,
			const ClearAttachment*                      pAttachments,
			uint32_t                                    rectCount,
			const ClearRectangle*                       pRects);

		VKAPI_ATTR void VKAPI_CALL vkCmdResolveImage(
			CommandBuffer                               commandBuffer,
			Image                                       srcImage,
			ImageLayout                                 srcImageLayout,
			Image                                       dstImage,
			ImageLayout                                 dstImageLayout,
			uint32_t                                    regionCount,
			const ImageResolve*                         pRegions);

		VKAPI_ATTR void VKAPI_CALL vkCmdSetEvent(
			CommandBuffer                               commandBuffer,
			Event                                       event,
			PipelineStageFlags                          stageMask);

		VKAPI_ATTR void VKAPI_CALL vkCmdResetEvent(
			CommandBuffer                               commandBuffer,
			Event                                       event,
			PipelineStageFlags                          stageMask);

		VKAPI_ATTR void VKAPI_CALL vkCmdWaitEvents(
			CommandBuffer                               commandBuffer,
			uint32_t                                    eventCount,
			const Event*                                pEvents,
			PipelineStageFlags                          srcStageMask,
			PipelineStageFlags                          dstStageMask,
			uint32_t                                    memoryBarrierCount,
			const MemoryBarrier*                        pMemoryBarriers,
			uint32_t                                    bufferMemoryBarrierCount,
			const BufferMemoryBarrier*                  pBufferMemoryBarriers,
			uint32_t                                    imageMemoryBarrierCount,
			const ImageMemoryBarrier*                   pImageMemoryBarriers);

		VKAPI_ATTR void VKAPI_CALL vkCmdPipelineBarrier(
			CommandBuffer                               commandBuffer,
			PipelineStageFlags                          srcStageMask,
			PipelineStageFlags                          dstStageMask,
			DependencyFlags                             dependencyFlags,
			uint32_t                                    memoryBarrierCount,
			const MemoryBarrier*                        pMemoryBarriers,
			uint32_t                                    bufferMemoryBarrierCount,
			const BufferMemoryBarrier*                  pBufferMemoryBarriers,
			uint32_t                                    imageMemoryBarrierCount,
			const ImageMemoryBarrier*                   pImageMemoryBarriers);

		VKAPI_ATTR void VKAPI_CALL vkCmdBeginQuery(
			CommandBuffer                               commandBuffer,
			QueryPool                                   queryPool,
			uint32_t                                    query,
			QueryControlFlags                           flags);

		VKAPI_ATTR void VKAPI_CALL vkCmdEndQuery(
			CommandBuffer                               commandBuffer,
			QueryPool                                   queryPool,
			uint32_t                                    query);

		VKAPI_ATTR void VKAPI_CALL vkCmdResetQueryPool(
			CommandBuffer                               commandBuffer,
			QueryPool                                   queryPool,
			uint32_t                                    firstQuery,
			uint32_t                                    queryCount);

		VKAPI_ATTR void VKAPI_CALL vkCmdWriteTimestamp(
			CommandBuffer                               commandBuffer,
			PipelineStageFlagBits                       pipelineStage,
			QueryPool                                   queryPool,
			uint32_t                                    query);

		VKAPI_ATTR void VKAPI_CALL vkCmdCopyQueryPoolResults(
			CommandBuffer                               commandBuffer,
			QueryPool                                   queryPool,
			uint32_t                                    firstQuery,
			uint32_t                                    queryCount,
			Buffer                                      dstBuffer,
			DeviceSize                                  dstOffset,
			DeviceSize                                  stride,
			QueryResultFlags                            flags);

		VKAPI_ATTR void VKAPI_CALL vkCmdPushConstants(
			CommandBuffer                               commandBuffer,
			PipelineLayout                              layout,
			ShaderStageFlags                            stageFlags,
			uint32_t                                    offset,
			uint32_t                                    size,
			const void*                                 pValues);

		VKAPI_ATTR void VKAPI_CALL vkCmdBeginRenderPass(
			CommandBuffer                               commandBuffer,
			const RenderPassBeginInfo*                  pRenderPassBegin,
			SubpassContents                             contents);

		VKAPI_ATTR void VKAPI_CALL vkCmdNextSubpass(
			CommandBuffer                               commandBuffer,
			SubpassContents                             contents);

		VKAPI_ATTR void VKAPI_CALL vkCmdEndRenderPass(
			CommandBuffer                               commandBuffer);

		VKAPI_ATTR void VKAPI_CALL vkCmdExecuteCommands(
			CommandBuffer                               commandBuffer,
			uint32_t                                    commandBufferCount,
			const CommandBuffer*                        pCommandBuffers);



		VKAPI_ATTR void VKAPI_CALL vkDestroySurfaceKHR(
			Instance                                    instance,
			Surface                                     surface,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkGetPhysicalDeviceSurfaceSupportKHR(
			PhysicalDevice                              physicalDevice,
			uint32_t                                    queueFamilyIndex,
			Surface                                     surface,
			Bool32*                                     pSupported);

		VKAPI_ATTR Result VKAPI_CALL vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
			PhysicalDevice                              physicalDevice,
			Surface                                     surface,
			SurfaceCapabilities*                        pSurfaceCapabilities);

		VKAPI_ATTR Result VKAPI_CALL vkGetPhysicalDeviceSurfaceFormatsKHR(
			PhysicalDevice                              physicalDevice,
			Surface                                     surface,
			uint32_t*                                   pSurfaceFormatCount,
			SurfaceFormat*                              pSurfaceFormats);

		VKAPI_ATTR Result VKAPI_CALL vkGetPhysicalDeviceSurfacePresentModesKHR(
			PhysicalDevice                              physicalDevice,
			Surface                                     surface,
			uint32_t*                                   pPresentModeCount,
			PresentMode*                                pPresentModes);

		VKAPI_ATTR Result VKAPI_CALL vkCreateSwapchainKHR(
			Device                                      device,
			const SwapchainCreateInfo*                  pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Swapchain*                                  pSwapchain);

		VKAPI_ATTR void VKAPI_CALL vkDestroySwapchainKHR(
			Device                                      device,
			Swapchain                                   swapchain,
			const AllocationCallbacks*                  pAllocator);

		VKAPI_ATTR Result VKAPI_CALL vkGetSwapchainImagesKHR(
			Device                                      device,
			Swapchain                                   swapchain,
			uint32_t*                                   pSwapchainImageCount,
			Image*                                      pSwapchainImages);

		VKAPI_ATTR Result VKAPI_CALL vkAcquireNextImageKHR(
			Device                                      device,
			Swapchain                                   swapchain,
			uint64_t                                    timeout,
			Semaphore                                   semaphore,
			Fence                                       fence,
			uint32_t*                                   pImageIndex);

		VKAPI_ATTR Result VKAPI_CALL vkQueuePresentKHR(
			Queue                                       queue,
			const PresentInfo*                          pPresentInfo);

		VKAPI_ATTR Result VKAPI_CALL vkCreateWin32SurfaceKHR(
			Instance                                    instance,
			const Win32SurfaceCreateInfo*               pCreateInfo,
			const AllocationCallbacks*                  pAllocator,
			Surface*                                    pSurface);

		VKAPI_ATTR uint32_t VKAPI_CALL vkGetPhysicalDeviceWin32PresentationSupportKHR(
			PhysicalDevice                              physicalDevice,
			uint32_t                                    queueFamilyIndex);
	}
}
