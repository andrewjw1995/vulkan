#pragma once

#include "DescriptorPoolSize.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class DescriptorPoolCreateInfo : public Structure
	{
	public:
		DescriptorPoolCreateInfo();

		DescriptorPoolCreateInfo& Flags(DescriptorPoolCreateFlags flags);
		DescriptorPoolCreateInfo& MaxSets(uint32_t max);
		DescriptorPoolCreateInfo& PoolSizes(uint32_t count, const DescriptorPoolSize* pSizes);

	private:
		DescriptorPoolCreateFlags    flags;
		uint32_t                     maxSets;
		uint32_t                     poolSizeCount;
		const DescriptorPoolSize*    pPoolSizes;
	};
}