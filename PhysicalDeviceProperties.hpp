#pragma once

#include "Enums.hpp"
#include "PhysicalDeviceLimits.hpp"
#include "PhysicalDeviceSparseProperties.hpp"
#include "UUID.hpp"
#include "Version.hpp"

#include <string>

namespace vulkan
{
	class PhysicalDeviceProperties
	{
	public:
		static const unsigned int MAX_NAME_SIZE = 256;

		Version                             APIVersion() const;
		Version                             DriverVersion() const;
		uint32_t                            VendorID() const;
		uint32_t                            DeviceID() const;
		PhysicalDeviceType                  DeviceType() const;
		std::string                         DeviceName() const;
		UUID                                PipelineCacheUUID() const;
		const PhysicalDeviceLimits&               Limits() const;
		const PhysicalDeviceSparseProperties&     SparseProperties() const;

	private:
		uint32_t                            apiVersion;
		uint32_t                            driverVersion;
		uint32_t                            vendorID;
		uint32_t                            deviceID;
		PhysicalDeviceType                  deviceType;
		char                                deviceName[MAX_NAME_SIZE];
		uint8_t                             pipelineCacheUUID[UUID::SIZE];
		PhysicalDeviceLimits                limits;
		PhysicalDeviceSparseProperties      sparseProperties;
	};
}
