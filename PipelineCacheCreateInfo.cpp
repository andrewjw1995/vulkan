#include "PipelineCacheCreateInfo.hpp"

using namespace vulkan;


PipelineCacheCreateInfo::PipelineCacheCreateInfo() :
	Structure(StructureType::PIPELINE_CACHE_CREATE_INFO),
	flags(),
	initialDataSize(0),
	pInitialData(nullptr)
{ }

PipelineCacheCreateInfo::PipelineCacheCreateInfo(size_t initialDataSize, const void* pInitialData) :
	Structure(StructureType::PIPELINE_CACHE_CREATE_INFO),
	flags(),
	initialDataSize(initialDataSize),
	pInitialData(pInitialData)
{ }

PipelineCacheCreateInfo& PipelineCacheCreateInfo::Flags(PipelineCacheCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineCacheCreateInfo& PipelineCacheCreateInfo::InitialData(size_t size, const void* pData)
{
	this->initialDataSize = size;
	this->pInitialData = pData;
	return *this;
}
