#pragma once

#include "Enums.hpp"

namespace vulkan
{
	template <typename Flag>
	class Flags
	{
	public:
		Flags() : value(0) { }
		Flags(Flag flag) : value(static_cast<int>(flag)) { }
		Flags(const Flags<Flag>& other) : value(other.value) { }
		explicit Flags(uint32_t value) : value(value) { }

		Flags<Flag>& operator=(const Flags<Flag>& other)
		{
			value = other.value;
			return *this;
		}

		Flags<Flag>& operator|=(const Flags<Flag>& other)
		{
			value |= other.value;
			return *this;
		}

		Flags<Flag>& operator&=(const Flags<Flag>& other)
		{
			value &= other.value;
			return *this;
		}

		Flags<Flag>& operator^=(const Flags<Flag>& other)
		{
			value ^= other.value;
			return *this;
		}

		Flags<Flag> operator|(const Flags<Flag>& other) const
		{
			return Flags<Flag>(value | other.value);
		}

		Flags<Flag> operator&(const Flags<Flag>& other) const
		{
			return Flags<Flag>(value & other.value);
		}

		Flags<Flag> operator^(const Flags<Flag>& other) const
		{
			return Flags<Flag>(value ^ other.value);
		}

		bool operator==(const Flags<Flag>& other) const
		{
			return value == other.value;
		}

		bool operator!=(const Flags<Flag>& other) const
		{
			return value != other.value;
		}

		explicit operator bool() const
		{
			return value != 0;
		}

		explicit operator uint32_t() const
		{
			return value;
		}

	private:
		uint32_t value;
	};

	template <typename Flag>
	Flags<Flag> operator|(Flag flag, const Flags<Flag>& flags)
	{
		return flags | flag;
	}

	template <typename Flag>
	Flags<Flag> operator&(Flag flag, const Flags<Flag>& flags)
	{
		return flags & flag;
	}

	template <typename Flag>
	Flags<Flag> operator^(Flag flag, const Flags<Flag>& flags)
	{
		return flags ^ flag;
	}

	typedef Flags<EmptyFlagBits> BufferViewCreateFlags;
	typedef Flags<EmptyFlagBits> DescriptorPoolResetFlags;
	typedef Flags<EmptyFlagBits> DescriptorSetLayoutCreateFlags;
	typedef Flags<EmptyFlagBits> DeviceCreateFlags;
	typedef Flags<EmptyFlagBits> DeviceQueueCreateFlags;
	typedef Flags<EmptyFlagBits> EventCreateFlags;
	typedef Flags<EmptyFlagBits> FramebufferCreateFlags;
	typedef Flags<EmptyFlagBits> ImageViewCreateFlags;
	typedef Flags<EmptyFlagBits> InstanceCreateFlags;
	typedef Flags<EmptyFlagBits> MemoryMapFlags;
	typedef Flags<EmptyFlagBits> PipelineCacheCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineColorBlendStateCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineDepthStencilStateCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineDynamicStateCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineInputAssemblyStateCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineLayoutCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineMultisampleStateCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineRasterizationStateCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineShaderStageCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineTessellationStateCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineVertexInputStateCreateFlags;
	typedef Flags<EmptyFlagBits> PipelineViewportStateCreateFlags;
	typedef Flags<EmptyFlagBits> RenderPassCreateFlags;
	typedef Flags<EmptyFlagBits> SamplerCreateFlags;
	typedef Flags<EmptyFlagBits> SemaphoreCreateFlags;
	typedef Flags<EmptyFlagBits> ShaderModuleCreateFlags;
	typedef Flags<EmptyFlagBits> SubpassDescriptionFlags;
	typedef Flags<EmptyFlagBits> SwapchainCreateFlags;
	typedef Flags<EmptyFlagBits> Win32SurfaceCreateFlags;

	typedef Flags<AccessFlagBits> AccessFlags;
	inline AccessFlags operator|(AccessFlagBits a, AccessFlagBits b) { return AccessFlags(a) | b; }

	typedef Flags<AttachmentDescriptionFlagBits> AttachmentDescriptionFlags;
	inline AttachmentDescriptionFlags operator|(AttachmentDescriptionFlagBits a, AttachmentDescriptionFlagBits b) { return AttachmentDescriptionFlags(a) | b; }

	typedef Flags<BufferCreateFlagBits> BufferCreateFlags;
	inline BufferCreateFlags operator|(BufferCreateFlagBits a, BufferCreateFlagBits b) { return BufferCreateFlags(a) | b; }

	typedef Flags<BufferUsageFlagBits> BufferUsageFlags;
	inline BufferUsageFlags operator|(BufferUsageFlagBits a, BufferUsageFlagBits b) { return BufferUsageFlags(a) | b; }

	typedef Flags<ColorComponentFlagBits> ColorComponentFlags;
	inline ColorComponentFlags operator|(ColorComponentFlagBits a, ColorComponentFlagBits b) { return ColorComponentFlags(a) | b; }

	typedef Flags<CommandBufferResetFlagBits> CommandBufferResetFlags;
	inline CommandBufferResetFlags operator|(CommandBufferResetFlagBits a, CommandBufferResetFlagBits b) { return CommandBufferResetFlags(a) | b; }

	typedef Flags<CommandBufferUsageFlagBits> CommandBufferUsageFlags;
	inline CommandBufferUsageFlags operator|(CommandBufferUsageFlagBits a, CommandBufferUsageFlagBits b) { return CommandBufferUsageFlags(a) | b; }

	typedef Flags<CommandPoolCreateFlagBits> CommandPoolCreateFlags;
	inline CommandPoolCreateFlags operator|(CommandPoolCreateFlagBits a, CommandPoolCreateFlagBits b) { return CommandPoolCreateFlags(a) | b; }

	typedef Flags<CommandPoolResetFlagBits> CommandPoolResetFlags;
	inline CommandPoolResetFlags operator|(CommandPoolResetFlagBits a, CommandPoolResetFlagBits b) { return CommandPoolResetFlags(a) | b; }

	typedef Flags<CompositeAlphaFlagBits> CompositeAlphaFlags;
	inline CompositeAlphaFlags operator|(CompositeAlphaFlagBits a, CompositeAlphaFlagBits b) { return CompositeAlphaFlags(a) | b; }

	typedef Flags<CullModeFlagBits> CullModeFlags;
	inline CullModeFlags operator|(CullModeFlagBits a, CullModeFlagBits b) { return CullModeFlags(a) | b; }

	typedef Flags<DependencyFlagBits> DependencyFlags;
	inline DependencyFlags operator|(DependencyFlagBits a, DependencyFlagBits b) { return DependencyFlags(a) | b; }

	typedef Flags<DescriptorPoolCreateFlagBits> DescriptorPoolCreateFlags;
	inline DescriptorPoolCreateFlags operator|(DescriptorPoolCreateFlagBits a, DescriptorPoolCreateFlagBits b) { return DescriptorPoolCreateFlags(a) | b; }

	typedef Flags<FenceCreateFlagBits> FenceCreateFlags;
	inline FenceCreateFlags operator|(FenceCreateFlagBits a, FenceCreateFlagBits b) { return FenceCreateFlags(a) | b; }

	typedef Flags<FormatFeatureFlagBits> FormatFeatureFlags;
	inline FormatFeatureFlags operator|(FormatFeatureFlagBits a, FormatFeatureFlagBits b) { return FormatFeatureFlags(a) | b; }

	typedef Flags<ImageAspectFlagBits> ImageAspectFlags;
	inline ImageAspectFlags operator|(ImageAspectFlagBits a, ImageAspectFlagBits b) { return ImageAspectFlags(a) | b; }

	typedef Flags<ImageCreateFlagBits> ImageCreateFlags;
	inline ImageCreateFlags operator|(ImageCreateFlagBits a, ImageCreateFlagBits b) { return ImageCreateFlags(a) | b; }

	typedef Flags<ImageUsageFlagBits> ImageUsageFlags;
	inline ImageUsageFlags operator|(ImageUsageFlagBits a, ImageUsageFlagBits b) { return ImageUsageFlags(a) | b; }

	typedef Flags<MemoryHeapFlagBits> MemoryHeapFlags;
	inline MemoryHeapFlags operator|(MemoryHeapFlagBits a, MemoryHeapFlagBits b) { return MemoryHeapFlags(a) | b; }

	typedef Flags<MemoryPropertyFlagBits> MemoryPropertyFlags;
	inline MemoryPropertyFlags operator|(MemoryPropertyFlagBits a, MemoryPropertyFlagBits b) { return MemoryPropertyFlags(a) | b; }

	typedef Flags<PipelineCreateFlagBits> PipelineCreateFlags;
	inline PipelineCreateFlags operator|(PipelineCreateFlagBits a, PipelineCreateFlagBits b) { return PipelineCreateFlags(a) | b; }

	typedef Flags<PipelineStageFlagBits> PipelineStageFlags;
	inline PipelineStageFlags operator|(PipelineStageFlagBits a, PipelineStageFlagBits b) { return PipelineStageFlags(a) | b; }

	typedef Flags<QueueFlagBits> QueueFlags;
	inline QueueFlags operator|(QueueFlagBits a, QueueFlagBits b) { return QueueFlags(a) | b; }

	typedef Flags<QueryControlFlagBits> QueryControlFlags;
	inline QueryControlFlags operator|(QueryControlFlagBits a, QueryControlFlagBits b) { return QueryControlFlags(a) | b; }

	typedef Flags<QueryPipelineStatisticFlagBits> QueryPipelineStatisticFlags;
	inline QueryPipelineStatisticFlags operator|(QueryPipelineStatisticFlagBits a, QueryPipelineStatisticFlagBits b) { return QueryPipelineStatisticFlags(a) | b; }

	typedef Flags<QueryResultFlagBits> QueryResultFlags;
	inline QueryResultFlags operator|(QueryResultFlagBits a, QueryResultFlagBits b) { return QueryResultFlags(a) | b; }

	typedef Flags<SampleCountFlagBits> SampleCountFlags;
	inline SampleCountFlags operator|(SampleCountFlagBits a, SampleCountFlagBits b) { return SampleCountFlags(a) | b; }

	typedef Flags<ShaderStageFlagBits> ShaderStageFlags;
	inline ShaderStageFlags operator|(ShaderStageFlagBits a, ShaderStageFlagBits b) { return ShaderStageFlags(a) | b; }

	typedef Flags<SparseImageFormatFlagBits> SparseImageFormatFlags;
	inline SparseImageFormatFlags operator|(SparseImageFormatFlagBits a, SparseImageFormatFlagBits b) { return SparseImageFormatFlags(a) | b; }

	typedef Flags<SparseMemoryBindFlagBits> SparseMemoryBindFlags;
	inline SparseMemoryBindFlags operator|(SparseMemoryBindFlagBits a, SparseMemoryBindFlagBits b) { return SparseMemoryBindFlags(a) | b; }

	typedef Flags<StencilFaceFlagBits> StencilFaceFlags;
	inline StencilFaceFlags operator|(StencilFaceFlagBits a, StencilFaceFlagBits b) { return StencilFaceFlags(a) | b; }

	typedef Flags<SurfaceTransformFlagBits> SurfaceTransformFlags;
	inline SurfaceTransformFlags operator|(SurfaceTransformFlagBits a, SurfaceTransformFlagBits b) { return SurfaceTransformFlags(a) | b; }
}
