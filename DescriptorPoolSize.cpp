#include "DescriptorPoolSize.hpp"

using namespace vulkan;

DescriptorPoolSize::DescriptorPoolSize(DescriptorType type, uint32_t descriptorCount) :
	type(type),
	descriptorCount(descriptorCount)
{ }
