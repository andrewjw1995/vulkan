#include "CommandBufferBeginInfo.hpp"

using namespace vulkan;

CommandBufferBeginInfo::CommandBufferBeginInfo() :
	Structure(StructureType::COMMAND_BUFFER_BEGIN_INFO),
	flags(),
	pInheritanceInfo(nullptr)
{ }

CommandBufferBeginInfo& CommandBufferBeginInfo::Flags(CommandBufferUsageFlags flags)
{
	this->flags = flags;
	return *this;
}

CommandBufferBeginInfo& CommandBufferBeginInfo::InheritanceInfo(const CommandBufferInheritanceInfo* pInfo)
{
	this->pInheritanceInfo = pInfo;
	return *this;
}
