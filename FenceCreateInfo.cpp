#include "FenceCreateInfo.hpp"

using namespace vulkan;

FenceCreateInfo::FenceCreateInfo() :
	Structure(StructureType::FENCE_CREATE_INFO),
	flags(0)
{ }

FenceCreateInfo::FenceCreateInfo(FenceCreateFlags flags) :
	Structure(StructureType::FENCE_CREATE_INFO),
	flags(flags)
{ }

FenceCreateInfo& FenceCreateInfo::Flags(FenceCreateFlags flags)
{
	this->flags = flags;
	return *this;
}
