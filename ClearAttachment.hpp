#pragma once

#include "ClearValue.hpp"
#include "Flags.hpp"

namespace vulkan
{
	class ClearAttachment
	{
	public:
		ClearAttachment(ImageAspectFlags aspectMask, uint32_t colorAttachment, ClearValue clearValue);

	private:
		ImageAspectFlags aspectMask;
		uint32_t         colorAttachment;
		ClearValue       clearValue;
	};
}