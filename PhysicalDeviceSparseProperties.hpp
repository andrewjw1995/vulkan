#pragma once

#include "Bool32.hpp"

namespace vulkan
{
	class PhysicalDeviceSparseProperties
	{
	public:
		Bool32 Standard2DBlockShape() const;
		Bool32 Standard2DMultisampleBlockShape() const;
		Bool32 Standard3DBlockShape() const;
		Bool32 AlignedMipSize() const;
		Bool32 NonResidentStrict() const;

	private:
		Bool32 residencyStandard2DBlockShape;
		Bool32 residencyStandard2DMultisampleBlockShape;
		Bool32 residencyStandard3DBlockShape;
		Bool32 residencyAlignedMipSize;
		Bool32 residencyNonResidentStrict;
	};
}
