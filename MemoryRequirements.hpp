#pragma once

#include "DeviceSize.hpp"

namespace vulkan
{
	class MemoryRequirements
	{
	public:
		DeviceSize Size() const;
		DeviceSize Alignment() const;
		uint32_t MemoryTypeBits() const;

	private:
		DeviceSize size;
		DeviceSize alignment;
		uint32_t memoryTypeBits;
	};
}
