#pragma once

#include "Bool32.hpp"
#include "Flags.hpp"
#include "StencilOpState.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineDepthStencilStateCreateInfo : public Structure
	{
	public:
		PipelineDepthStencilStateCreateInfo();

		PipelineDepthStencilStateCreateInfo& Flags(PipelineDepthStencilStateCreateFlags flags);
		PipelineDepthStencilStateCreateInfo& EnableDepthTest(CompareOp op);
		PipelineDepthStencilStateCreateInfo& DisableDepthTest();
		PipelineDepthStencilStateCreateInfo& EnableDepthWrite();
		PipelineDepthStencilStateCreateInfo& DisableDepthWrite();
		PipelineDepthStencilStateCreateInfo& EnableDepthBounds(float min, float max);
		PipelineDepthStencilStateCreateInfo& DisableDepthBounds();
		PipelineDepthStencilStateCreateInfo& EnableStencilTest(StencilOpState front, StencilOpState back);
		PipelineDepthStencilStateCreateInfo& DisableStencilTest();

	private:
		PipelineDepthStencilStateCreateFlags flags;
		Bool32                               depthTestEnable;
		Bool32                               depthWriteEnable;
		CompareOp                            depthCompareOp;
		Bool32                               depthBoundsTestEnable;
		Bool32                               stencilTestEnable;
		StencilOpState                       front;
		StencilOpState                       back;
		float                                minDepthBounds;
		float                                maxDepthBounds;
	};
}