#include "ComputePipelineCreateInfo.hpp"

using namespace vulkan;

ComputePipelineCreateInfo::ComputePipelineCreateInfo() :
	Structure(StructureType::COMPUTE_PIPELINE_CREATE_INFO),
	flags(),
	stage(),
	layout(),
	basePipelineHandle(),
	basePipelineIndex(-1)
{ }

ComputePipelineCreateInfo& ComputePipelineCreateInfo::Flags(PipelineCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

ComputePipelineCreateInfo& ComputePipelineCreateInfo::Stage(PipelineShaderStageCreateInfo stage)
{
	this->stage = stage;
	return *this;
}

ComputePipelineCreateInfo& ComputePipelineCreateInfo::Layout(PipelineLayout layout)
{
	this->layout = layout;
	return *this;
}

ComputePipelineCreateInfo& ComputePipelineCreateInfo::BasePipeline(Pipeline handle)
{
	this->basePipelineHandle = handle;
	this->basePipelineIndex = -1;
	return *this;
}

ComputePipelineCreateInfo& ComputePipelineCreateInfo::BasePipeline(int32_t index)
{
	this->basePipelineIndex = index;
	this->basePipelineHandle = Pipeline();
	return *this;
}
