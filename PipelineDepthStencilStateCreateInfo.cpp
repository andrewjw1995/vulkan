#include "PipelineDepthStencilStateCreateInfo.hpp"

using namespace vulkan;

PipelineDepthStencilStateCreateInfo::PipelineDepthStencilStateCreateInfo() :
	Structure(StructureType::PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO),
	flags(),
	depthTestEnable(false),
	depthWriteEnable(false),
	depthCompareOp(CompareOp::NEVER), // TODO: NEVER or ALWAYS?
	depthBoundsTestEnable(false),
	stencilTestEnable(false),
	front(),
	back(),
	minDepthBounds(0),
	maxDepthBounds(0)
{ }

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::Flags(PipelineDepthStencilStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::EnableDepthTest(CompareOp op)
{
	this->depthTestEnable = true;
	this->depthCompareOp = op;
	return *this;
}

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::DisableDepthTest()
{
	this->depthTestEnable = false;
	return *this;
}

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::EnableDepthWrite()
{
	this->depthWriteEnable = true;
	return *this;
}

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::DisableDepthWrite()
{
	this->depthWriteEnable = false;
	return *this;
}

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::EnableDepthBounds(float min, float max)
{
	this->depthBoundsTestEnable = true;
	this->minDepthBounds = min;
	this->maxDepthBounds = max;
	return *this;
}

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::DisableDepthBounds()
{
	this->depthBoundsTestEnable = false;
	return *this;
}

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::EnableStencilTest(StencilOpState front, StencilOpState back)
{
	this->stencilTestEnable = true;
	this->front = front;
	this->back = back;
	return *this;
}

PipelineDepthStencilStateCreateInfo& PipelineDepthStencilStateCreateInfo::DisableStencilTest()
{
	this->stencilTestEnable = false;
	return *this;
}
