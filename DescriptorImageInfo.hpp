#pragma once

#include "Enums.hpp"
#include "NonDispatchableHandle.hpp"

namespace vulkan
{
	class DescriptorImageInfo
	{
	public:
		DescriptorImageInfo(Sampler sampler, ImageView imageView, ImageLayout imageLayout);

	private:
		Sampler        sampler;
		ImageView      imageView;
		ImageLayout    imageLayout;
	};
}