#include "MemoryType.hpp"

using namespace vulkan;

MemoryPropertyFlags MemoryType::Flags() const
{
	return propertyFlags;
}

uint32_t MemoryType::HeapIndex() const
{
	return heapIndex;
}
