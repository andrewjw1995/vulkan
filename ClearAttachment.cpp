#include "ClearAttachment.hpp"

using namespace vulkan;

ClearAttachment::ClearAttachment(ImageAspectFlags aspectMask, uint32_t colorAttachment, ClearValue clearValue) :
	aspectMask(aspectMask),
	colorAttachment(colorAttachment),
	clearValue(clearValue)
{ }
