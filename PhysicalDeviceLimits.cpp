#include "PhysicalDeviceLimits.hpp"

using namespace std;
using namespace vulkan;

uint32_t PhysicalDeviceLimits::MaxImageDimension1D() const
{
	return maxImageDimension1D;
}

uint32_t PhysicalDeviceLimits::MaxImageDimension2D() const
{
	return maxImageDimension2D;
}

uint32_t PhysicalDeviceLimits::MaxImageDimension3D() const
{
	return maxImageDimension3D;
}

uint32_t PhysicalDeviceLimits::MaxImageDimensionCube() const
{
	return maxImageDimensionCube;
}

uint32_t PhysicalDeviceLimits::MaxImageArrayLayers() const
{
	return maxImageArrayLayers;
}

uint32_t PhysicalDeviceLimits::MaxTexelBufferElements() const
{
	return maxTexelBufferElements;
}

uint32_t PhysicalDeviceLimits::MaxUniformBufferRange() const
{
	return maxUniformBufferRange;
}

uint32_t PhysicalDeviceLimits::MaxStorageBufferRange() const
{
	return maxStorageBufferRange;
}

uint32_t PhysicalDeviceLimits::MaxPushConstantsSize() const
{
	return maxPushConstantsSize;
}

uint32_t PhysicalDeviceLimits::MaxMemoryAllocationCount() const
{
	return maxMemoryAllocationCount;
}

uint32_t PhysicalDeviceLimits::MaxSamplerAllocationCount() const
{
	return maxSamplerAllocationCount;
}

DeviceSize PhysicalDeviceLimits::BufferImageGranularity() const
{
	return bufferImageGranularity;
}

DeviceSize PhysicalDeviceLimits::SparseAddressSpaceSize() const
{
	return sparseAddressSpaceSize;
}

uint32_t PhysicalDeviceLimits::MaxBoundDescriptorSets() const
{
	return maxBoundDescriptorSets;
}

uint32_t PhysicalDeviceLimits::MaxPerStageDescriptorSamplers() const
{
	return maxPerStageDescriptorSamplers;
}

uint32_t PhysicalDeviceLimits::MaxPerStageDescriptorUniformBuffers() const
{
	return maxPerStageDescriptorUniformBuffers;
}

uint32_t PhysicalDeviceLimits::MaxPerStageDescriptorStorageBuffers() const
{
	return maxPerStageDescriptorStorageBuffers;
}

uint32_t PhysicalDeviceLimits::MaxPerStageDescriptorSampledImages() const
{
	return maxPerStageDescriptorSampledImages;
}

uint32_t PhysicalDeviceLimits::MaxPerStageDescriptorStorageImages() const
{
	return maxPerStageDescriptorStorageImages;
}

uint32_t PhysicalDeviceLimits::MaxPerStageDescriptorInputAttachments() const
{
	return maxPerStageDescriptorInputAttachments;
}

uint32_t PhysicalDeviceLimits::MaxPerStageResources() const
{
	return maxPerStageResources;
}

uint32_t PhysicalDeviceLimits::MaxDescriptorSetSamplers() const
{
	return maxDescriptorSetSamplers;
}

uint32_t PhysicalDeviceLimits::MaxDescriptorSetUniformBuffers() const
{
	return maxDescriptorSetUniformBuffers;
}

uint32_t PhysicalDeviceLimits::MaxDescriptorSetUniformBuffersDynamic() const
{
	return maxDescriptorSetUniformBuffersDynamic;
}

uint32_t PhysicalDeviceLimits::MaxDescriptorSetStorageBuffers() const
{
	return maxDescriptorSetStorageBuffers;
}

uint32_t PhysicalDeviceLimits::MaxDescriptorSetStorageBuffersDynamic() const
{
	return maxDescriptorSetStorageBuffersDynamic;
}

uint32_t PhysicalDeviceLimits::MaxDescriptorSetSampledImages() const
{
	return maxDescriptorSetSampledImages;
}

uint32_t PhysicalDeviceLimits::MaxDescriptorSetStorageImages() const
{
	return maxDescriptorSetStorageImages;
}

uint32_t PhysicalDeviceLimits::MaxDescriptorSetInputAttachments() const
{
	return maxDescriptorSetInputAttachments;
}

uint32_t PhysicalDeviceLimits::MaxVertexInputAttributes() const
{
	return maxVertexInputAttributes;
}

uint32_t PhysicalDeviceLimits::MaxVertexInputBindings() const
{
	return maxVertexInputBindings;
}

uint32_t PhysicalDeviceLimits::MaxVertexInputAttributeOffset() const
{
	return maxVertexInputAttributeOffset;
}

uint32_t PhysicalDeviceLimits::MaxVertexInputBindingStride() const
{
	return maxVertexInputBindingStride;
}

uint32_t PhysicalDeviceLimits::MaxVertexOutputComponents() const
{
	return maxVertexOutputComponents;
}

uint32_t PhysicalDeviceLimits::MaxTessellationGenerationLevel() const
{
	return maxTessellationGenerationLevel;
}

uint32_t PhysicalDeviceLimits::MaxTessellationPatchSize() const
{
	return maxTessellationPatchSize;
}

uint32_t PhysicalDeviceLimits::MaxTessellationControlPerVertexInputComponents() const
{
	return maxTessellationControlPerVertexInputComponents;
}

uint32_t PhysicalDeviceLimits::MaxTessellationControlPerVertexOutputComponents() const
{
	return maxTessellationControlPerVertexOutputComponents;
}

uint32_t PhysicalDeviceLimits::MaxTessellationControlPerPatchOutputComponents() const
{
	return maxTessellationControlPerPatchOutputComponents;
}

uint32_t PhysicalDeviceLimits::MaxTessellationControlTotalOutputComponents() const
{
	return maxTessellationControlTotalOutputComponents;
}

uint32_t PhysicalDeviceLimits::MaxTessellationEvaluationInputComponents() const
{
	return maxTessellationEvaluationInputComponents;
}

uint32_t PhysicalDeviceLimits::MaxTessellationEvaluationOutputComponents() const
{
	return maxTessellationEvaluationOutputComponents;
}

uint32_t PhysicalDeviceLimits::MaxGeometryShaderInvocations() const
{
	return maxGeometryShaderInvocations;
}

uint32_t PhysicalDeviceLimits::MaxGeometryInputComponents() const
{
	return maxGeometryInputComponents;
}

uint32_t PhysicalDeviceLimits::MaxGeometryOutputComponents() const
{
	return maxGeometryOutputComponents;
}

uint32_t PhysicalDeviceLimits::MaxGeometryOutputVertices() const
{
	return maxGeometryOutputVertices;
}

uint32_t PhysicalDeviceLimits::MaxGeometryTotalOutputComponents() const
{
	return maxGeometryTotalOutputComponents;
}

uint32_t PhysicalDeviceLimits::MaxFragmentInputComponents() const
{
	return maxFragmentInputComponents;
}

uint32_t PhysicalDeviceLimits::MaxFragmentOutputAttachments() const
{
	return maxFragmentOutputAttachments;
}

uint32_t PhysicalDeviceLimits::MaxFragmentDualSrcAttachments() const
{
	return maxFragmentDualSrcAttachments;
}

uint32_t PhysicalDeviceLimits::MaxFragmentCombinedOutputResources() const
{
	return maxFragmentCombinedOutputResources;
}

uint32_t PhysicalDeviceLimits::MaxComputeSharedMemorySize() const
{
	return maxComputeSharedMemorySize;
}

uint32_t PhysicalDeviceLimits::MaxComputeWorkGroupCount(unsigned int index) const
{
	if (index > 2)
		throw out_of_range("index");
	return maxComputeWorkGroupCount[index];
}

uint32_t PhysicalDeviceLimits::MaxComputeWorkGroupInvocations() const
{
	return maxComputeWorkGroupInvocations;
}

uint32_t PhysicalDeviceLimits::MaxComputeWorkGroupSize(unsigned int index) const
{
	if (index > 2)
		throw out_of_range("index");
	return maxComputeWorkGroupSize[index];
}

uint32_t PhysicalDeviceLimits::SubPixelPrecisionBits() const
{
	return subPixelPrecisionBits;
}

uint32_t PhysicalDeviceLimits::SubTexelPrecisionBits() const
{
	return subTexelPrecisionBits;
}

uint32_t PhysicalDeviceLimits::MipmapPrecisionBits() const
{
	return mipmapPrecisionBits;
}

uint32_t PhysicalDeviceLimits::MaxDrawIndexedIndexValue() const
{
	return maxDrawIndexedIndexValue;
}

uint32_t PhysicalDeviceLimits::MaxDrawIndirectCount() const
{
	return maxDrawIndirectCount;
}

float PhysicalDeviceLimits::MaxSamplerLodBias() const
{
	return maxSamplerLodBias;
}

float PhysicalDeviceLimits::MaxSamplerAnisotropy() const
{
	return maxSamplerAnisotropy;
}

uint32_t PhysicalDeviceLimits::MaxViewports() const
{
	return maxViewports;
}

uint32_t PhysicalDeviceLimits::MaxViewportDimensions(unsigned int index) const
{
	if (index > 1)
		throw out_of_range("index");
	return maxViewportDimensions[index];
}

float PhysicalDeviceLimits::ViewportBoundsRange(unsigned int index) const
{
	if (index > 1)
		throw out_of_range("index");
	return viewportBoundsRange[index];
}

uint32_t PhysicalDeviceLimits::ViewportSubPixelBits() const
{
	return viewportSubPixelBits;
}

size_t PhysicalDeviceLimits::MinMemoryMapAlignment() const
{
	return minMemoryMapAlignment;
}

DeviceSize PhysicalDeviceLimits::MinTexelBufferOffsetAlignment() const
{
	return minTexelBufferOffsetAlignment;
}

DeviceSize PhysicalDeviceLimits::MinUniformBufferOffsetAlignment() const
{
	return minUniformBufferOffsetAlignment;
}

DeviceSize PhysicalDeviceLimits::MinStorageBufferOffsetAlignment() const
{
	return minStorageBufferOffsetAlignment;
}

int32_t PhysicalDeviceLimits::MinTexelOffset() const
{
	return minTexelOffset;
}

uint32_t PhysicalDeviceLimits::MaxTexelOffset() const
{
	return maxTexelOffset;
}

int32_t PhysicalDeviceLimits::MinTexelGatherOffset() const
{
	return minTexelGatherOffset;
}

uint32_t PhysicalDeviceLimits::MaxTexelGatherOffset() const
{
	return maxTexelGatherOffset;
}

float PhysicalDeviceLimits::MinInterpolationOffset() const
{
	return minInterpolationOffset;
}

float PhysicalDeviceLimits::MaxInterpolationOffset() const
{
	return maxInterpolationOffset;
}

uint32_t PhysicalDeviceLimits::SubPixelInterpolationOffsetBits() const
{
	return subPixelInterpolationOffsetBits;
}

uint32_t PhysicalDeviceLimits::MaxFramebufferWidth() const
{
	return maxFramebufferWidth;
}

uint32_t PhysicalDeviceLimits::MaxFramebufferHeight() const
{
	return maxFramebufferHeight;
}

uint32_t PhysicalDeviceLimits::MaxFramebufferLayers() const
{
	return maxFramebufferLayers;
}

SampleCountFlags PhysicalDeviceLimits::FramebufferColorSampleCounts() const
{
	return framebufferColorSampleCounts;
}

SampleCountFlags PhysicalDeviceLimits::FramebufferDepthSampleCounts() const
{
	return framebufferDepthSampleCounts;
}

SampleCountFlags PhysicalDeviceLimits::FramebufferStencilSampleCounts() const
{
	return framebufferStencilSampleCounts;
}

SampleCountFlags PhysicalDeviceLimits::FramebufferNoAttachmentsSampleCounts() const
{
	return framebufferNoAttachmentsSampleCounts;
}

uint32_t PhysicalDeviceLimits::MaxColorAttachments() const
{
	return maxColorAttachments;
}

SampleCountFlags PhysicalDeviceLimits::SampledImageColorSampleCounts() const
{
	return sampledImageColorSampleCounts;
}

SampleCountFlags PhysicalDeviceLimits::SampledImageIntegerSampleCounts() const
{
	return sampledImageIntegerSampleCounts;
}

SampleCountFlags PhysicalDeviceLimits::SampledImageDepthSampleCounts() const
{
	return sampledImageDepthSampleCounts;
}

SampleCountFlags PhysicalDeviceLimits::SampledImageStencilSampleCounts() const
{
	return sampledImageStencilSampleCounts;
}

SampleCountFlags PhysicalDeviceLimits::StorageImageSampleCounts() const
{
	return storageImageSampleCounts;
}

uint32_t PhysicalDeviceLimits::MaxSampleMaskWords() const
{
	return maxSampleMaskWords;
}

Bool32 PhysicalDeviceLimits::TimestampComputeAndGraphics() const
{
	return timestampComputeAndGraphics;
}

float PhysicalDeviceLimits::TimestampPeriod() const
{
	return timestampPeriod;
}

uint32_t PhysicalDeviceLimits::MaxClipDistances() const
{
	return maxClipDistances;
}

uint32_t PhysicalDeviceLimits::MaxCullDistances() const
{
	return maxCullDistances;
}

uint32_t PhysicalDeviceLimits::MaxCombinedClipAndCullDistances() const
{
	return maxCombinedClipAndCullDistances;
}

uint32_t PhysicalDeviceLimits::DiscreteQueuePriorities() const
{
	return discreteQueuePriorities;
}

float PhysicalDeviceLimits::PointSizeRange(unsigned int index) const
{
	if (index > 1)
		throw out_of_range("index");
	return pointSizeRange[index];
}

float PhysicalDeviceLimits::LineWidthRange(unsigned int index) const
{
	if (index > 1)
		throw out_of_range("index");
	return lineWidthRange[index];
}

float PhysicalDeviceLimits::PointSizeGranularity() const
{
	return pointSizeGranularity;
}

float PhysicalDeviceLimits::LineWidthGranularity() const
{
	return lineWidthGranularity;
}

Bool32 PhysicalDeviceLimits::StrictLines() const
{
	return strictLines;
}

Bool32 PhysicalDeviceLimits::StandardSampleLocations() const
{
	return standardSampleLocations;
}

DeviceSize PhysicalDeviceLimits::OptimalBufferCopyOffsetAlignment() const
{
	return optimalBufferCopyOffsetAlignment;
}

DeviceSize PhysicalDeviceLimits::OptimalBufferCopyRowPitchAlignment() const
{
	return optimalBufferCopyRowPitchAlignment;
}

DeviceSize PhysicalDeviceLimits::NonCoherentAtomSize() const
{
	return nonCoherentAtomSize;
}
