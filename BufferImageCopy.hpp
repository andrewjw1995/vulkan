#pragma once

#include "DeviceSize.hpp"
#include "Extent3D.hpp"
#include "ImageSubresourceLayers.hpp"
#include "Offset3D.hpp"

namespace vulkan
{
	class BufferImageCopy
	{
	public:
		BufferImageCopy();

		BufferImageCopy& Buffer(DeviceSize offset, uint32_t rowLength, uint32_t height);
		BufferImageCopy& Image(ImageSubresourceLayers subresource, Offset3D offset, Extent3D extent);

	private:
		DeviceSize                bufferOffset;
		uint32_t                  bufferRowLength;
		uint32_t                  bufferImageHeight;
		ImageSubresourceLayers    imageSubresource;
		Offset3D                  imageOffset;
		Extent3D                  imageExtent;
	};
}