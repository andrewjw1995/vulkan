#include "Functions.hpp"
#include "PhysicalDevice.hpp"
#include "VulkanError.hpp"

using namespace std;
using namespace vulkan;

vector<Layer> PhysicalDevice::EnumerateLayers() const
{
	uint32_t count = 0;
	Result result = vkEnumerateDeviceLayerProperties(*this, &count, nullptr);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to count device-level layers");
	vector<Layer> buffer(count);
	result = vkEnumerateDeviceLayerProperties(*this, &count, buffer.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to enumerate device-level layers");
	return buffer;
}

vector<Extension> PhysicalDevice::EnumerateExtensions(const char* pLayer) const
{
	uint32_t count = 0;
	Result result = vkEnumerateDeviceExtensionProperties(*this, pLayer, &count, nullptr);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to count device-level extensions");
	vector<Extension> buffer(count);
	result = vkEnumerateDeviceExtensionProperties(*this, pLayer, &count, buffer.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to enumerate device-level extensions");
	return buffer;
}

void PhysicalDevice::GetFeatures(PhysicalDeviceFeatures& features) const
{
	vkGetPhysicalDeviceFeatures(*this, &features);
}

PhysicalDeviceFeatures PhysicalDevice::GetFeatures() const
{
	PhysicalDeviceFeatures features;
	vkGetPhysicalDeviceFeatures(*this, &features);
	return features;
}

FormatProperties PhysicalDevice::GetFormatProperties(Format format)
{
	FormatProperties format_properties;
	vkGetPhysicalDeviceFormatProperties(*this, format, &format_properties);
	return format_properties;
}

ImageFormatProperties PhysicalDevice::GetImageFormatProperties(
	Format format,
	ImageType type,
	ImageTiling tiling,
	ImageUsageFlags usage,
	ImageCreateFlags flags
	) const
{
	ImageFormatProperties image_format_properties;
	Result result = vkGetPhysicalDeviceImageFormatProperties(*this, format, type, tiling, usage,
		flags, &image_format_properties);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to get physical device's image format properties");
	return image_format_properties;
}

void PhysicalDevice::GetProperties(PhysicalDeviceProperties& properties) const
{
	vkGetPhysicalDeviceProperties(*this, &properties);
}

PhysicalDeviceProperties PhysicalDevice::GetProperties() const
{
	PhysicalDeviceProperties properties;
	vkGetPhysicalDeviceProperties(*this, &properties);
	return properties;
}

vector<QueueFamily> PhysicalDevice::GetQueueFamilies() const
{
	uint32_t count = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(*this, &count, nullptr);
	vector<QueueFamily> buffer(count);
	vkGetPhysicalDeviceQueueFamilyProperties(*this, &count, buffer.data());
	return buffer;
}

void PhysicalDevice::GetMemoryProperties(PhysicalDeviceMemoryProperties& memoryProperties) const
{
	vkGetPhysicalDeviceMemoryProperties(*this, &memoryProperties);
}

PhysicalDeviceMemoryProperties PhysicalDevice::GetMemoryProperties() const
{
	PhysicalDeviceMemoryProperties memory_properties;
	vkGetPhysicalDeviceMemoryProperties(*this, &memory_properties);
	return memory_properties;
}

vector<SparseImageFormatProperties> PhysicalDevice::GetSparseImageFormatProperties(
	Format                                      format,
	ImageType                                   type,
	SampleCountFlagBits                         samples,
	ImageUsageFlags                             usage,
	ImageTiling                                 tiling
	) const
{
	uint32_t count = 0;
	vkGetPhysicalDeviceSparseImageFormatProperties(*this, format, type, samples, usage, tiling,
		&count, nullptr);
	vector<SparseImageFormatProperties> buffer(count);
	vkGetPhysicalDeviceSparseImageFormatProperties(*this, format, type, samples, usage, tiling,
		&count, buffer.data());
	return buffer;
}

Device PhysicalDevice::CreateDevice(const DeviceCreateInfo& info) const
{
	Device device;
	Result result = vkCreateDevice(*this, &info, nullptr, &device);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create device");
	return device;
}

bool PhysicalDevice::GetSurfaceSupport(uint32_t queueFamilyIndex, Surface surface) const
{
	Bool32 supported;
	Result result = vkGetPhysicalDeviceSurfaceSupportKHR(*this, queueFamilyIndex, surface, &supported);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to check surface support");
	return supported;
}

SurfaceCapabilities PhysicalDevice::GetSurfaceCapabilities(Surface surface) const
{
	SurfaceCapabilities surface_capabilities;
	Result result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(*this, surface, &surface_capabilities);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to get surface capabilities");
	return surface_capabilities;
}

std::vector<SurfaceFormat> PhysicalDevice::GetSurfaceFormats(Surface surface) const
{
	uint32_t count = 0;
	Result result = vkGetPhysicalDeviceSurfaceFormatsKHR(*this, surface, &count, nullptr);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to count surface formats");
	vector<SurfaceFormat> buffer(count);
	result = vkGetPhysicalDeviceSurfaceFormatsKHR(*this, surface, &count, buffer.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to enumerate surface formats");
	return buffer;
}

std::vector<PresentMode> PhysicalDevice::GetSurfacePresentModes(Surface surface) const
{
	uint32_t count = 0;
	Result result = vkGetPhysicalDeviceSurfacePresentModesKHR(*this, surface, &count, nullptr);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to count surface present modes");
	vector<PresentMode> buffer(count);
	result = vkGetPhysicalDeviceSurfacePresentModesKHR(*this, surface, &count, buffer.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to enumerate surface present modes");
	return buffer;
}

bool PhysicalDevice::GetWin32PresentationSupport(uint32_t queueFamilyIndex) const
{
	return Bool32(vkGetPhysicalDeviceWin32PresentationSupportKHR(*this, queueFamilyIndex));
}
