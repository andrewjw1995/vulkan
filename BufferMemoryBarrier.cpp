#include "BufferMemoryBarrier.hpp"

using namespace vulkan;

BufferMemoryBarrier::BufferMemoryBarrier() :
	Structure(StructureType::BUFFER_MEMORY_BARRIER),
	srcAccessMask(),
	dstAccessMask(),
	srcQueueFamilyIndex(0),
	dstQueueFamilyIndex(0),
	buffer(),
	offset(0),
	size(0)
{ }

BufferMemoryBarrier& BufferMemoryBarrier::Source(AccessFlags accessMask, uint32_t queueFamily)
{
	this->srcAccessMask = accessMask;
	this->srcQueueFamilyIndex = queueFamily;
	return *this;
}

BufferMemoryBarrier& BufferMemoryBarrier::Destination(AccessFlags accessMask, uint32_t queueFamily)
{
	this->dstAccessMask = accessMask;
	this->dstQueueFamilyIndex = queueFamily;
	return *this;
}

BufferMemoryBarrier& BufferMemoryBarrier::Buffer(vulkan::Buffer buffer, DeviceSize offset, DeviceSize size)
{
	this->buffer = buffer;
	this->offset = offset;
	this->size = size;
	return *this;
}
