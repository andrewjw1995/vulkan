#pragma once

#include "Enums.hpp"

#include <cstdint>

namespace vulkan
{
	class Structure
	{
	public:
		Structure(StructureType type);
		Structure(StructureType type, Structure* next);

		void SetNext(Structure* next);

		StructureType Type() const;
		Structure* Next() const;

	private:
		StructureType sType;
		Structure* pNext;
	};
}
