#pragma once

#include "Enums.hpp"

namespace vulkan
{
	class ComponentMapping
	{
	public:
		ComponentMapping();
		ComponentMapping(ComponentSwizzle r, ComponentSwizzle g, ComponentSwizzle b, ComponentSwizzle a);

	private:
		ComponentSwizzle    r;
		ComponentSwizzle    g;
		ComponentSwizzle    b;
		ComponentSwizzle    a;
	};
}