#pragma once

#include "Enums.hpp"

namespace vulkan
{
	class SurfaceFormat
	{
	public:
		SurfaceFormat();
		SurfaceFormat(Format format, ColorSpace colorSpace);

		Format        Format() const;
		ColorSpace    ColorSpace() const;

	private:
		vulkan::Format        format;
		vulkan::ColorSpace    colorSpace;
	};
}