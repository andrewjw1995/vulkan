#include "ImageViewCreateInfo.hpp"

using namespace vulkan;

ImageViewCreateInfo::ImageViewCreateInfo() :
	Structure(StructureType::IMAGE_VIEW_CREATE_INFO),
	flags(),
	image(),
	viewType(),
	format(Format::UNDEFINED),
	components(),
	subresourceRange()
{ }

ImageViewCreateInfo& ImageViewCreateInfo::Flags(ImageViewCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

ImageViewCreateInfo& ImageViewCreateInfo::Image(vulkan::Image image)
{
	this->image = image;
	return *this;
}

ImageViewCreateInfo& ImageViewCreateInfo::ViewType(ImageViewType type)
{
	this->viewType = type;
	return *this;
}

ImageViewCreateInfo& ImageViewCreateInfo::Format(vulkan::Format format)
{
	this->format = format;
	return *this;
}

ImageViewCreateInfo& ImageViewCreateInfo::Components(ComponentMapping mapping)
{
	this->components = mapping;
	return *this;
}

ImageViewCreateInfo& ImageViewCreateInfo::SubresourceRange(ImageSubresourceRange range)
{
	this->subresourceRange = range;
	return *this;
}
