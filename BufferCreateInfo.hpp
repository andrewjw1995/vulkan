#pragma once

#include "DeviceSize.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class BufferCreateInfo : public Structure
	{
	public:
		BufferCreateInfo();

		BufferCreateInfo& Flags(BufferCreateFlags flags);
		BufferCreateInfo& Size(DeviceSize size);
		BufferCreateInfo& Usage(BufferUsageFlags usage);
		BufferCreateInfo& Concurrent(uint32_t count, const uint32_t* pQueueFamilyIndices);
		BufferCreateInfo& Exclusive();

	private:
		BufferCreateFlags      flags;
		DeviceSize             size;
		BufferUsageFlags       usage;
		SharingMode            sharingMode;
		uint32_t               queueFamilyIndexCount;
		const uint32_t*        pQueueFamilyIndices;
	};
}