#include "SparseImageFormatProperties.hpp"

using namespace vulkan;

ImageAspectFlags          SparseImageFormatProperties::AspectMask() const
{
	return aspectMask;
}

Extent3D                  SparseImageFormatProperties::ImageGranularity() const
{
	return imageGranularity;
}

SparseImageFormatFlags    SparseImageFormatProperties::Flags() const
{
	return flags;
}
