#include "PipelineColorBlendAttachmentState.hpp"

using namespace vulkan;

PipelineColorBlendAttachmentState::PipelineColorBlendAttachmentState() :
	blendEnable(false),
	srcColorBlendFactor(BlendFactor::ZERO), // TODO: ZERO or ONE?
	dstColorBlendFactor(BlendFactor::ZERO),
	colorBlendOp(BlendOp::ADD),
	srcAlphaBlendFactor(BlendFactor::ZERO), // TODO: ZERO or ONE?
	dstAlphaBlendFactor(BlendFactor::ZERO),
	alphaBlendOp(BlendOp::ADD),
	colorWriteMask(ColorComponentFlagBits::RGBA)
{ }

PipelineColorBlendAttachmentState& PipelineColorBlendAttachmentState::EnableBlend()
{
	this->blendEnable = true;
	return *this;
}

PipelineColorBlendAttachmentState& PipelineColorBlendAttachmentState::DisableBlend()
{
	this->blendEnable = false;
	return *this;
}

PipelineColorBlendAttachmentState& PipelineColorBlendAttachmentState::Color(BlendFactor source, BlendFactor destination, BlendOp op)
{
	this->srcColorBlendFactor = source;
	this->dstColorBlendFactor = destination;
	this->colorBlendOp = op;
	return *this;
}

PipelineColorBlendAttachmentState& PipelineColorBlendAttachmentState::Alpha(BlendFactor source, BlendFactor destination, BlendOp op)
{
	this->srcAlphaBlendFactor = source;
	this->dstAlphaBlendFactor = destination;
	this->alphaBlendOp = op;
	return *this;
}

PipelineColorBlendAttachmentState& PipelineColorBlendAttachmentState::ColorWriteMask(ColorComponentFlags mask)
{
	this->colorWriteMask = mask;
	return *this;
}
