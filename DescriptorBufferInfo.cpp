#include "DescriptorBufferInfo.hpp"

using namespace vulkan;

DescriptorBufferInfo::DescriptorBufferInfo(Buffer buffer, DeviceSize offset, DeviceSize range) :
	buffer(buffer),
	offset(offset),
	range(range)
{ }
