#include "PushConstantRange.hpp"

using namespace vulkan;

PushConstantRange::PushConstantRange(ShaderStageFlags stageFlags, uint32_t offset, uint32_t size) :
	stageFlags(stageFlags),
	offset(offset),
	size(size)
{ }
