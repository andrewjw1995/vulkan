#pragma once

#include "CommandBufferInheritanceInfo.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class CommandBufferBeginInfo : public Structure
	{
	public:
		CommandBufferBeginInfo();

		CommandBufferBeginInfo& Flags(CommandBufferUsageFlags flags);
		CommandBufferBeginInfo& InheritanceInfo(const CommandBufferInheritanceInfo* pInfo);

	private:
		CommandBufferUsageFlags                flags;
		const CommandBufferInheritanceInfo*    pInheritanceInfo;
	};
}