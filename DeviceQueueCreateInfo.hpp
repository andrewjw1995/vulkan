#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class DeviceQueueCreateInfo : public Structure
	{
	public:
		DeviceQueueCreateInfo();

		DeviceQueueCreateInfo& Flags(DeviceQueueCreateFlags flags);
		DeviceQueueCreateInfo& QueueFamilyIndex(uint32_t index);
		DeviceQueueCreateInfo& Queues(uint32_t count, const float* pPriorities);

	private:
		DeviceQueueCreateFlags      flags;
		uint32_t                    queueFamilyIndex;
		uint32_t                    queueCount;
		const float*                pQueuePriorities;
	};
}
