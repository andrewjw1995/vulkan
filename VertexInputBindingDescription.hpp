#pragma once

#include "Enums.hpp"

namespace vulkan
{
	class VertexInputBindingDescription
	{
	public:
		VertexInputBindingDescription(uint32_t binding, uint32_t stride, VertexInputRate inputRate);

	private:
		uint32_t           binding;
		uint32_t           stride;
		VertexInputRate    inputRate;
	};
}