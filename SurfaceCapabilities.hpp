#pragma once

#include "Extent2D.hpp"
#include "Flags.hpp"

namespace vulkan
{
	class SurfaceCapabilities
	{
	public:
		uint32_t                    MinImageCount() const;
		uint32_t                    MaxImageCount() const;
		Extent2D                    CurrentExtent() const;
		Extent2D                    MinImageExtent() const;
		Extent2D                    MaxImageExtent() const;
		uint32_t                    MaxImageArrayLayers() const;
		SurfaceTransformFlags       SupportedTransforms() const;
		SurfaceTransformFlagBits    CurrentTransform() const;
		CompositeAlphaFlags         SupportedCompositeAlpha() const;
		ImageUsageFlags             SupportedUsageFlags() const;

	private:
		uint32_t                    minImageCount;
		uint32_t                    maxImageCount;
		Extent2D                    currentExtent;
		Extent2D                    minImageExtent;
		Extent2D                    maxImageExtent;
		uint32_t                    maxImageArrayLayers;
		SurfaceTransformFlags       supportedTransforms;
		SurfaceTransformFlagBits    currentTransform;
		CompositeAlphaFlags         supportedCompositeAlpha;
		ImageUsageFlags             supportedUsageFlags;
	};
}