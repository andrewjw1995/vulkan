#include "AttachmentReference.hpp"

using namespace vulkan;

AttachmentReference::AttachmentReference(uint32_t attachment, ImageLayout layout) :
	attachment(attachment),
	layout(layout)
{ }
