#pragma once

#include <cstdint>
#include <string>

namespace vulkan
{
	enum class AccessFlagBits
	{
		INDIRECT_COMMAND_READ = 0x00000001,
		INDEX_READ = 0x00000002,
		VERTEX_ATTRIBUTE_READ = 0x00000004,
		UNIFORM_READ = 0x00000008,
		INPUT_ATTACHMENT_READ = 0x00000010,
		SHADER_READ = 0x00000020,
		SHADER_WRITE = 0x00000040,
		COLOR_ATTACHMENT_READ = 0x00000080,
		COLOR_ATTACHMENT_WRITE = 0x00000100,
		DEPTH_STENCIL_ATTACHMENT_READ = 0x00000200,
		DEPTH_STENCIL_ATTACHMENT_WRITE = 0x00000400,
		TRANSFER_READ = 0x00000800,
		TRANSFER_WRITE = 0x00001000,
		HOST_READ = 0x00002000,
		HOST_WRITE = 0x00004000,
		MEMORY_READ = 0x00008000,
		MEMORY_WRITE = 0x00010000,
	};

	enum class AttachmentDescriptionFlagBits
	{
		MAY_ALIAS = 0x00000001,
	};

	enum class AttachmentLoadOp
	{
		LOAD = 0,
		CLEAR = 1,
		DONT_CARE = 2,
	};

	enum class AttachmentStoreOp
	{
		STORE = 0,
		DONT_CARE = 1,
	};

	enum class BlendFactor
	{
		ZERO = 0,
		ONE = 1,
		SRC_COLOR = 2,
		ONE_MINUS_SRC_COLOR = 3,
		DST_COLOR = 4,
		ONE_MINUS_DST_COLOR = 5,
		SRC_ALPHA = 6,
		ONE_MINUS_SRC_ALPHA = 7,
		DST_ALPHA = 8,
		ONE_MINUS_DST_ALPHA = 9,
		CONSTANT_COLOR = 10,
		ONE_MINUS_CONSTANT_COLOR = 11,
		CONSTANT_ALPHA = 12,
		ONE_MINUS_CONSTANT_ALPHA = 13,
		SRC_ALPHA_SATURATE = 14,
		SRC1_COLOR = 15,
		ONE_MINUS_SRC1_COLOR = 16,
		SRC1_ALPHA = 17,
		ONE_MINUS_SRC1_ALPHA = 18,
	};

	enum class BlendOp
	{
		ADD = 0,
		SUBTRACT = 1,
		REVERSE_SUBTRACT = 2,
		MIN = 3,
		MAX = 4,
	};

	enum class BorderColor
	{
		FLOAT_TRANSPARENT_BLACK = 0,
		INT_TRANSPARENT_BLACK = 1,
		FLOAT_OPAQUE_BLACK = 2,
		INT_OPAQUE_BLACK = 3,
		FLOAT_OPAQUE_WHITE = 4,
		INT_OPAQUE_WHITE = 5,
	};

	enum class BufferCreateFlagBits
	{
		SPARSE_BINDING = 0x00000001,
		SPARSE_RESIDENCY = 0x00000002,
		SPARSE_ALIASED = 0x00000004,
	};

	enum class BufferUsageFlagBits
	{
		TRANSFER_SRC = 0x00000001,
		TRANSFER_DST = 0x00000002,
		UNIFORM_TEXEL_BUFFER = 0x00000004,
		STORAGE_TEXEL_BUFFER = 0x00000008,
		UNIFORM_BUFFER = 0x00000010,
		STORAGE_BUFFER = 0x00000020,
		INDEX_BUFFER = 0x00000040,
		VERTEX_BUFFER = 0x00000080,
		INDIRECT_BUFFER = 0x00000100,
	};

	enum class ColorComponentFlagBits
	{
		R = 0x00000001,
		G = 0x00000002,
		B = 0x00000004,
		A = 0x00000008,
		// Extra
		RGB = 0x00000007,
		RGBA = 0x0000000F,
	};

	enum class ColorSpace
	{
		SRGB_NONLINEAR = 0,
	};

	enum class CommandBufferLevel
	{
		PRIMARY = 0,
		SECONDARY = 1,
	};

	enum class CommandBufferResetFlagBits
	{
		RELEASE_RESOURCES = 0x00000001,
	};

	enum class CommandBufferUsageFlagBits
	{
		ONE_TIME_SUBMIT = 0x00000001,
		RENDER_PASS_CONTINUE = 0x00000002,
		SIMULTANEOUS_USE = 0x00000004,
	};

	enum class CommandPoolCreateFlagBits
	{
		TRANSIENT = 0x00000001,
		RESET_COMMAND_BUFFER = 0x00000002,
	};

	enum class CommandPoolResetFlagBits
	{
		RELEASE_RESOURCES = 0x00000001,
	};

	enum class CompareOp
	{
		NEVER = 0,
		LESS = 1,
		EQUAL = 2,
		LESS_OR_EQUAL = 3,
		GREATER = 4,
		NOT_EQUAL = 5,
		GREATER_OR_EQUAL = 6,
		ALWAYS = 7,
	};

	enum class ComponentSwizzle
	{
		IDENTITY = 0,
		ZERO = 1,
		ONE = 2,
		R = 3,
		G = 4,
		B = 5,
		A = 6,
	};

	enum class CompositeAlphaFlagBits
	{
		OPAQUE = 0x00000001,
		PRE_MULTIPLIED = 0x00000002,
		POST_MULTIPLIED = 0x00000004,
		INHERIT = 0x00000008,
	};

	enum class CullModeFlagBits
	{
		NONE = 0,
		FRONT = 0x00000001,
		BACK = 0x00000002,
	};

	enum class DependencyFlagBits
	{
		BY_REGION = 0x00000001,
	};

	enum class DescriptorPoolCreateFlagBits
	{
		FREE_DESCRIPTOR_SET = 0x00000001,
	};

	enum class DescriptorType
	{
		SAMPLER = 0,
		COMBINED_IMAGE_SAMPLER = 1,
		SAMPLED_IMAGE = 2,
		STORAGE_IMAGE = 3,
		UNIFORM_TEXEL_BUFFER = 4,
		STORAGE_TEXEL_BUFFER = 5,
		UNIFORM_BUFFER = 6,
		STORAGE_BUFFER = 7,
		UNIFORM_BUFFER_DYNAMIC = 8,
		STORAGE_BUFFER_DYNAMIC = 9,
		INPUT_ATTACHMENT = 10,
	};

	enum class DynamicState
	{
		VIEWPORT = 0,
		SCISSOR = 1,
		LINE_WIDTH = 2,
		DEPTH_BIAS = 3,
		BLEND_CONSTANTS = 4,
		DEPTH_BOUNDS = 5,
		STENCIL_COMPARE_MASK = 6,
		STENCIL_WRITE_MASK = 7,
		STENCIL_REFERENCE = 8,
	};

	enum class EmptyFlagBits { };

	enum class FenceCreateFlagBits
	{
		SIGNALED = 0x00000001,
	};

	enum class Filter
	{
		NEAREST = 0,
		LINEAR = 1,
		CUBIC = 1000015000,
	};

	enum class Format
	{
		UNDEFINED = 0,
		R4G4_UNORM_PACK8 = 1,
		R4G4B4A4_UNORM_PACK16 = 2,
		B4G4R4A4_UNORM_PACK16 = 3,
		R5G6B5_UNORM_PACK16 = 4,
		B5G6R5_UNORM_PACK16 = 5,
		R5G5B5A1_UNORM_PACK16 = 6,
		B5G5R5A1_UNORM_PACK16 = 7,
		A1R5G5B5_UNORM_PACK16 = 8,
		R8_UNORM = 9,
		R8_SNORM = 10,
		R8_USCALED = 11,
		R8_SSCALED = 12,
		R8_UINT = 13,
		R8_SINT = 14,
		R8_SRGB = 15,
		R8G8_UNORM = 16,
		R8G8_SNORM = 17,
		R8G8_USCALED = 18,
		R8G8_SSCALED = 19,
		R8G8_UINT = 20,
		R8G8_SINT = 21,
		R8G8_SRGB = 22,
		R8G8B8_UNORM = 23,
		R8G8B8_SNORM = 24,
		R8G8B8_USCALED = 25,
		R8G8B8_SSCALED = 26,
		R8G8B8_UINT = 27,
		R8G8B8_SINT = 28,
		R8G8B8_SRGB = 29,
		B8G8R8_UNORM = 30,
		B8G8R8_SNORM = 31,
		B8G8R8_USCALED = 32,
		B8G8R8_SSCALED = 33,
		B8G8R8_UINT = 34,
		B8G8R8_SINT = 35,
		B8G8R8_SRGB = 36,
		R8G8B8A8_UNORM = 37,
		R8G8B8A8_SNORM = 38,
		R8G8B8A8_USCALED = 39,
		R8G8B8A8_SSCALED = 40,
		R8G8B8A8_UINT = 41,
		R8G8B8A8_SINT = 42,
		R8G8B8A8_SRGB = 43,
		B8G8R8A8_UNORM = 44,
		B8G8R8A8_SNORM = 45,
		B8G8R8A8_USCALED = 46,
		B8G8R8A8_SSCALED = 47,
		B8G8R8A8_UINT = 48,
		B8G8R8A8_SINT = 49,
		B8G8R8A8_SRGB = 50,
		A8B8G8R8_UNORM_PACK32 = 51,
		A8B8G8R8_SNORM_PACK32 = 52,
		A8B8G8R8_USCALED_PACK32 = 53,
		A8B8G8R8_SSCALED_PACK32 = 54,
		A8B8G8R8_UINT_PACK32 = 55,
		A8B8G8R8_SINT_PACK32 = 56,
		A8B8G8R8_SRGB_PACK32 = 57,
		A2R10G10B10_UNORM_PACK32 = 58,
		A2R10G10B10_SNORM_PACK32 = 59,
		A2R10G10B10_USCALED_PACK32 = 60,
		A2R10G10B10_SSCALED_PACK32 = 61,
		A2R10G10B10_UINT_PACK32 = 62,
		A2R10G10B10_SINT_PACK32 = 63,
		A2B10G10R10_UNORM_PACK32 = 64,
		A2B10G10R10_SNORM_PACK32 = 65,
		A2B10G10R10_USCALED_PACK32 = 66,
		A2B10G10R10_SSCALED_PACK32 = 67,
		A2B10G10R10_UINT_PACK32 = 68,
		A2B10G10R10_SINT_PACK32 = 69,
		R16_UNORM = 70,
		R16_SNORM = 71,
		R16_USCALED = 72,
		R16_SSCALED = 73,
		R16_UINT = 74,
		R16_SINT = 75,
		R16_SFLOAT = 76,
		R16G16_UNORM = 77,
		R16G16_SNORM = 78,
		R16G16_USCALED = 79,
		R16G16_SSCALED = 80,
		R16G16_UINT = 81,
		R16G16_SINT = 82,
		R16G16_SFLOAT = 83,
		R16G16B16_UNORM = 84,
		R16G16B16_SNORM = 85,
		R16G16B16_USCALED = 86,
		R16G16B16_SSCALED = 87,
		R16G16B16_UINT = 88,
		R16G16B16_SINT = 89,
		R16G16B16_SFLOAT = 90,
		R16G16B16A16_UNORM = 91,
		R16G16B16A16_SNORM = 92,
		R16G16B16A16_USCALED = 93,
		R16G16B16A16_SSCALED = 94,
		R16G16B16A16_UINT = 95,
		R16G16B16A16_SINT = 96,
		R16G16B16A16_SFLOAT = 97,
		R32_UINT = 98,
		R32_SINT = 99,
		R32_SFLOAT = 100,
		R32G32_UINT = 101,
		R32G32_SINT = 102,
		R32G32_SFLOAT = 103,
		R32G32B32_UINT = 104,
		R32G32B32_SINT = 105,
		R32G32B32_SFLOAT = 106,
		R32G32B32A32_UINT = 107,
		R32G32B32A32_SINT = 108,
		R32G32B32A32_SFLOAT = 109,
		R64_UINT = 110,
		R64_SINT = 111,
		R64_SFLOAT = 112,
		R64G64_UINT = 113,
		R64G64_SINT = 114,
		R64G64_SFLOAT = 115,
		R64G64B64_UINT = 116,
		R64G64B64_SINT = 117,
		R64G64B64_SFLOAT = 118,
		R64G64B64A64_UINT = 119,
		R64G64B64A64_SINT = 120,
		R64G64B64A64_SFLOAT = 121,
		B10G11R11_UFLOAT_PACK32 = 122,
		E5B9G9R9_UFLOAT_PACK32 = 123,
		D16_UNORM = 124,
		X8_D24_UNORM_PACK32 = 125,
		D32_SFLOAT = 126,
		S8_UINT = 127,
		D16_UNORM_S8_UINT = 128,
		D24_UNORM_S8_UINT = 129,
		D32_SFLOAT_S8_UINT = 130,
		BC1_RGB_UNORM_BLOCK = 131,
		BC1_RGB_SRGB_BLOCK = 132,
		BC1_RGBA_UNORM_BLOCK = 133,
		BC1_RGBA_SRGB_BLOCK = 134,
		BC2_UNORM_BLOCK = 135,
		BC2_SRGB_BLOCK = 136,
		BC3_UNORM_BLOCK = 137,
		BC3_SRGB_BLOCK = 138,
		BC4_UNORM_BLOCK = 139,
		BC4_SNORM_BLOCK = 140,
		BC5_UNORM_BLOCK = 141,
		BC5_SNORM_BLOCK = 142,
		BC6H_UFLOAT_BLOCK = 143,
		BC6H_SFLOAT_BLOCK = 144,
		BC7_UNORM_BLOCK = 145,
		BC7_SRGB_BLOCK = 146,
		ETC2_R8G8B8_UNORM_BLOCK = 147,
		ETC2_R8G8B8_SRGB_BLOCK = 148,
		ETC2_R8G8B8A1_UNORM_BLOCK = 149,
		ETC2_R8G8B8A1_SRGB_BLOCK = 150,
		ETC2_R8G8B8A8_UNORM_BLOCK = 151,
		ETC2_R8G8B8A8_SRGB_BLOCK = 152,
		EAC_R11_UNORM_BLOCK = 153,
		EAC_R11_SNORM_BLOCK = 154,
		EAC_R11G11_UNORM_BLOCK = 155,
		EAC_R11G11_SNORM_BLOCK = 156,
		ASTC_4x4_UNORM_BLOCK = 157,
		ASTC_4x4_SRGB_BLOCK = 158,
		ASTC_5x4_UNORM_BLOCK = 159,
		ASTC_5x4_SRGB_BLOCK = 160,
		ASTC_5x5_UNORM_BLOCK = 161,
		ASTC_5x5_SRGB_BLOCK = 162,
		ASTC_6x5_UNORM_BLOCK = 163,
		ASTC_6x5_SRGB_BLOCK = 164,
		ASTC_6x6_UNORM_BLOCK = 165,
		ASTC_6x6_SRGB_BLOCK = 166,
		ASTC_8x5_UNORM_BLOCK = 167,
		ASTC_8x5_SRGB_BLOCK = 168,
		ASTC_8x6_UNORM_BLOCK = 169,
		ASTC_8x6_SRGB_BLOCK = 170,
		ASTC_8x8_UNORM_BLOCK = 171,
		ASTC_8x8_SRGB_BLOCK = 172,
		ASTC_10x5_UNORM_BLOCK = 173,
		ASTC_10x5_SRGB_BLOCK = 174,
		ASTC_10x6_UNORM_BLOCK = 175,
		ASTC_10x6_SRGB_BLOCK = 176,
		ASTC_10x8_UNORM_BLOCK = 177,
		ASTC_10x8_SRGB_BLOCK = 178,
		ASTC_10x10_UNORM_BLOCK = 179,
		ASTC_10x10_SRGB_BLOCK = 180,
		ASTC_12x10_UNORM_BLOCK = 181,
		ASTC_12x10_SRGB_BLOCK = 182,
		ASTC_12x12_UNORM_BLOCK = 183,
		ASTC_12x12_SRGB_BLOCK = 184,
	};

	enum class FormatFeatureFlagBits
	{
		SAMPLED_IMAGE = 0x00000001,
		STORAGE_IMAGE = 0x00000002,
		STORAGE_IMAGE_ATOMIC = 0x00000004,
		UNIFORM_TEXEL_BUFFER = 0x00000008,
		STORAGE_TEXEL_BUFFER = 0x00000010,
		STORAGE_TEXEL_BUFFER_ATOMIC = 0x00000020,
		VERTEX_BUFFER = 0x00000040,
		COLOR_ATTACHMENT = 0x00000080,
		COLOR_ATTACHMENT_BLEND = 0x00000100,
		DEPTH_STENCIL_ATTACHMENT = 0x00000200,
		BLIT_SRC = 0x00000400,
		BLIT_DST = 0x00000800,
		SAMPLED_IMAGE_FILTER_LINEAR = 0x00001000,
		SAMPLED_IMAGE_FILTER_CUBIC = 0x00002000,
	};

	enum class FrontFace
	{
		COUNTER_CLOCKWISE = 0,
		CLOCKWISE = 1,
	};

	enum class ImageAspectFlagBits
	{
		COLOR = 0x00000001,
		DEPTH = 0x00000002,
		STENCIL = 0x00000004,
		METADATA = 0x00000008,
	};

	enum class ImageCreateFlagBits
	{
		SPARSE_BINDING = 0x00000001,
		SPARSE_RESIDENCY = 0x00000002,
		SPARSE_ALIASED = 0x00000004,
		MUTABLE_FORMAT = 0x00000008,
		CUBE_COMPATIBLE = 0x00000010,
	};

	enum class ImageLayout
	{
		UNDEFINED = 0,
		GENERAL = 1,
		COLOR_ATTACHMENT = 2,
		DEPTH_STENCIL_ATTACHMENT = 3,
		DEPTH_STENCIL_READ_ONLY = 4,
		SHADER_READ_ONLY = 5,
		TRANSFER_SRC = 6,
		TRANSFER_DST = 7,
		PREINITIALIZED = 8,
		PRESENT_SRC = 1000001002,
	};

	enum class ImageTiling
	{
		OPTIMAL = 0,
		LINEAR = 1,
	};

	enum class ImageType
	{
		X1D = 0,
		X2D = 1,
		X3D = 2,
	};

	enum class ImageUsageFlagBits
	{
		TRANSFER_SRC = 0x00000001,
		TRANSFER_DST = 0x00000002,
		SAMPLED = 0x00000004,
		STORAGE = 0x00000008,
		COLOR_ATTACHMENT = 0x00000010,
		DEPTH_STENCIL_ATTACHMENT = 0x00000020,
		TRANSIENT_ATTACHMENT = 0x00000040,
		INPUT_ATTACHMENT = 0x00000080,
	};

	enum class ImageViewType
	{
		X1D = 0,
		X2D = 1,
		X3D = 2,
		CUBE = 3,
		X1D_ARRAY = 4,
		X2D_ARRAY = 5,
		CUBE_ARRAY = 6,
	};

	enum class IndexType
	{
		UINT16 = 0,
		UINT32 = 1,
	};

	enum class InternalAllocationType
	{
		/// <summary>
		/// The allocation is intended for execution by the host.
		/// </summary>
		EXECUTABLE = 0
	};

	enum class LogicOp
	{
		CLEAR = 0,
		AND = 1,
		AND_REVERSE = 2,
		COPY = 3,
		AND_INVERTED = 4,
		NO_OP = 5,
		XOR = 6,
		OR = 7,
		NOR = 8,
		EQUIVALENT = 9,
		INVERT = 10,
		OR_REVERSE = 11,
		COPY_INVERTED = 12,
		OR_INVERTED = 13,
		NAND = 14,
		SET = 15,
	};

	enum class MemoryHeapFlagBits
	{
		DEVICE_LOCAL = 0x00000001,
	};

	enum class MemoryPropertyFlagBits
	{
		DEVICE_LOCAL = 0x00000001,
		HOST_VISIBLE = 0x00000002,
		HOST_COHERENT = 0x00000004,
		HOST_CACHED = 0x00000008,
		LAZILY_ALLOCATED = 0x00000010,
	};

	enum class MipmapMode
	{
		NEAREST = 0,
		LINEAR = 1,
	};

	enum class PhysicalDeviceType
	{
		OTHER = 0,
		INTEGRATED_GPU = 1,
		DISCRETE_GPU = 2,
		VIRTUAL_GPU = 3,
		CPU = 4,
	};

	enum class PipelineBindPoint
	{
		GRAPHICS = 0,
		COMPUTE = 1,
	};

	enum class PipelineCacheHeaderVersion
	{
		ONE = 1,
	};

	enum class PipelineCreateFlagBits
	{
		DISABLE_OPTIMIZATION = 0x00000001,
		ALLOW_DERIVATIVES = 0x00000002,
		DERIVATIVE = 0x00000004,
	};

	enum class PipelineStageFlagBits
	{
		TOP_OF_PIPE = 0x00000001,
		DRAW_INDIRECT = 0x00000002,
		VERTEX_INPUT = 0x00000004,
		VERTEX_SHADER = 0x00000008,
		TESSELLATION_CONTROL_SHADER = 0x00000010,
		TESSELLATION_EVALUATION_SHADER = 0x00000020,
		GEOMETRY_SHADER = 0x00000040,
		FRAGMENT_SHADER = 0x00000080,
		EARLY_FRAGMENT_TESTS = 0x00000100,
		LATE_FRAGMENT_TESTS = 0x00000200,
		COLOR_ATTACHMENT_OUTPUT = 0x00000400,
		COMPUTE_SHADER = 0x00000800,
		TRANSFER = 0x00001000,
		BOTTOM_OF_PIPE = 0x00002000,
		HOST = 0x00004000,
		ALL_GRAPHICS = 0x00008000,
		ALL_COMMANDS = 0x00010000,
	};

	enum class PolygonMode
	{
		FILL = 0,
		LINE = 1,
		POINT = 2,
	};

	enum class PresentMode
	{
		IMMEDIATE = 0,
		MAILBOX = 1,
		FIFO = 2,
		FIFO_RELAXED = 3,
	};

	enum class PrimitiveTopology
	{
		POINT_LIST = 0,
		LINE_LIST = 1,
		LINE_STRIP = 2,
		TRIANGLE_LIST = 3,
		TRIANGLE_STRIP = 4,
		TRIANGLE_FAN = 5,
		LINE_LIST_WITH_ADJACENCY = 6,
		LINE_STRIP_WITH_ADJACENCY = 7,
		TRIANGLE_LIST_WITH_ADJACENCY = 8,
		TRIANGLE_STRIP_WITH_ADJACENCY = 9,
		PATCH_LIST = 10,
	};

	enum class QueryControlFlagBits
	{
		VK_QUERY_CONTROL_PRECISE = 0x00000001,
	};

	enum class QueryPipelineStatisticFlagBits
	{
		INPUT_ASSEMBLY_VERTICES = 0x00000001,
		INPUT_ASSEMBLY_PRIMITIVES = 0x00000002,
		VERTEX_SHADER_INVOCATIONS = 0x00000004,
		GEOMETRY_SHADER_INVOCATIONS = 0x00000008,
		GEOMETRY_SHADER_PRIMITIVES = 0x00000010,
		CLIPPING_INVOCATIONS = 0x00000020,
		CLIPPING_PRIMITIVES = 0x00000040,
		FRAGMENT_SHADER_INVOCATIONS = 0x00000080,
		TESSELLATION_CONTROL_SHADER_PATCHES = 0x00000100,
		TESSELLATION_EVALUATION_SHADER_INVOCATIONS = 0x00000200,
		COMPUTE_SHADER_INVOCATIONS = 0x00000400,
	};

	enum class QueryResultFlagBits
	{
		X64 = 0x00000001,
		WAIT = 0x00000002,
		WITH_AVAILABILITY = 0x00000004,
		PARTIAL = 0x00000008,
	};

	enum class QueryType
	{
		OCCLUSION = 0,
		PIPELINE_STATISTICS = 1,
		TIMESTAMP = 2,
	};

	enum class QueueFlagBits
	{
		GRAPHICS = 0x00000001,
		COMPUTE = 0x00000002,
		TRANSFER = 0x00000004,
		SPARSE_BINDING = 0x00000008,
	};

	/// <summary>
	/// All return codes in Vulkan are reported via Result return values.
	/// </summary>
	/// <remarks>
	/// Successful completion codes are returned when a command needs to communicate success or
	/// status information. All successful completion codes are non - negative values.
	/// Runtime error codes are returned when a command needs to communicate a failure that could
	/// only be detected at runtime. All run time error codes are negative values.
	/// 
	/// If a command returns a run time error, it will leave any result pointers unmodified.
	/// Out of memory errors do not damage any currently existing Vulkan objects. Objects that have
	/// already been successfully created can still be used by the application.
	/// 
	/// Performance - critical commands generally do not have return codes. If a run time error
	/// occurs in such commands, the implementation will defer reporting the error until a
	/// specified point. For commands that record into command buffers (vkCmd*) run time errors are	/// reported by vkEndCommandBuffer.
	/// </remarks>
	enum class Result
	{
		/// <summary>
		/// Command successfully completed.
		/// </summary>
		SUCCESS = 0,
		/// <summary>
		/// A fence or query has not yet completed.
		/// </summary>
		NOT_READY = 1,
		/// <summary>
		/// A wait operation has not completed in the specified time.
		/// </summary>
		TIMEOUT = 2,
		/// <summary>
		/// An event is signaled.
		/// </summary>
		EVENT_SET = 3,
		/// <summary>
		/// An event is unsignaled.
		/// </summary>
		EVENT_RESET = 4,
		/// <summary>
		/// A return array was too small for the result.
		/// </summary>
		INCOMPLETE = 5,
		/// <summary>
		/// A host memory allocation has failed.
		/// </summary>
		OUT_OF_HOST_MEMORY = -1,
		/// <summary>
		/// A device memory allocation has failed.
		/// </summary>
		OUT_OF_DEVICE_MEMORY = -2,
		/// <summary>
		/// Initialization of an object could not be completed for implementation - specific
		/// reasons.
		/// </summary>
		INITIALIZATION_FAILED = -3,
		/// <summary>
		/// The logical or physical device has been lost.
		/// </summary>
		DEVICE_LOST = -4,
		/// <summary>
		/// Mapping of a memory object has failed.
		/// </summary>
		MEMORY_MAP_FAILED = -5,
		/// <summary>
		/// A requested layer is not present or could not be loaded.
		/// </summary>
		LAYER_NOT_PRESENT = -6,
		/// <summary>
		/// A requested extension is not supported.
		/// </summary>
		EXTENSION_NOT_PRESENT = -7,
		/// <summary>
		/// A requested feature is not supported.
		/// </summary>
		FEATURE_NOT_PRESENT = -8,
		/// <summary>
		/// The requested version of Vulkan is not supported by the driver or is otherwise
		/// incompatible for implementation - specific reasons.
		/// </summary>
		INCOMPATIBLE_DRIVER = -9,
		/// <summary>
		/// Too many objects of the type have already been created.
		/// </summary>
		TOO_MANY_OBJECTS = -10,
		/// <summary>
		/// A requested format is not supported on this device.
		/// </summary>
		FORMAT_NOT_SUPPORTED = -11,
		/// <summary>
		/// A surface is no longer available.
		/// </summary>
		SURFACE_LOST = -1000000000,
		/// <summary>
		/// The requested window is already connected to a VkSurfaceKHR, or to some other non -
		/// Vulkan API.
		/// </summary>
		NATIVE_WINDOW_IN_USE = -1000000001,
		/// <summary>
		/// A swapchain no longer matches the surface properties exactly, but can still be used to
		/// present to the surface successfully.
		/// </summary>
		SUBOPTIMAL = 1000001003,
		/// <summary>
		/// A surface has changed in such a way that it is no longer compatible with the swapchain,
		/// and further presentation requests using the swapchain will fail. Applications must
		/// query the new surface properties and recreate their swapchain if they wish to continue
		/// presenting to the surface.
		/// </summary>
		OUT_OF_DATE = -1000001004,
		/// <summary>
		/// The display used by a swapchain does not use the same presentable image layout, or is
		/// incompatible in a way that prevents sharing an image.
		/// </summary>
		INCOMPATIBLE_DISPLAY = -1000003001,
		// TODO: Add summary
		VALIDATION_FAILED = -1000011001,
		// TODO: Add summary
		INVALID_SHADER = -1000012000,
	};

	enum class SampleCountFlagBits
	{
		X1 = 0x00000001,
		X2 = 0x00000002,
		X4 = 0x00000004,
		X8 = 0x00000008,
		X16 = 0x00000010,
		X32 = 0x00000020,
		X64 = 0x00000040,
	};

	enum class SamplerAddressMode
	{
		REPEAT = 0,
		MIRRORED_REPEAT = 1,
		CLAMP_TO_EDGE = 2,
		CLAMP_TO_BORDER = 3,
		MIRROR_CLAMP_TO_EDGE = 4,
	};

	enum class SamplerMipmapMode
	{
		NEAREST = 0,
		LINEAR = 1,
	};

	enum class ShaderStageFlagBits
	{
		VERTEX = 0x00000001,
		TESSELLATION_CONTROL = 0x00000002,
		TESSELLATION_EVALUATION = 0x00000004,
		GEOMETRY = 0x00000008,
		FRAGMENT = 0x00000010,
		COMPUTE = 0x00000020,
		ALL_GRAPHICS = 0x0000001F,
	};

	enum class SharingMode
	{
		EXCLUSIVE = 0,
		CONCURRENT = 1,
	};

	enum class SparseImageFormatFlagBits
	{
		SINGLE_MIPTAIL = 0x00000001,
		ALIGNED_MIP_SIZE = 0x00000002,
		NONSTANDARD_BLOCK_SIZE = 0x00000004,
	};

	enum class SparseMemoryBindFlagBits
	{
		METADATA = 0x00000001,
	};

	enum class StencilFaceFlagBits
	{
		FRONT = 0x00000001,
		BACK = 0x00000002,
		FRONT_AND_BACK = 0x00000003,
	};

	enum class StencilOp
	{
		KEEP = 0,
		ZERO = 1,
		REPLACE = 2,
		INCREMENT_AND_CLAMP = 3,
		DECREMENT_AND_CLAMP = 4,
		INVERT = 5,
		INCREMENT_AND_WRAP = 6,
		DECREMENT_AND_WRAP = 7,
	};

	/// <summary>
	/// Any class which inherits from Structure inherently has a StructureType sType member, which
	/// allows a Structure to be treated as a tagged union. There will only be one appropriate
	/// value for the sType member, based on the name of the class, so classes should automatically
	/// fill in this value.
	/// </summary>
	enum class StructureType
	{
		APPLICATION_INFO = 0,
		INSTANCE_CREATE_INFO = 1,
		DEVICE_QUEUE_CREATE_INFO = 2,
		DEVICE_CREATE_INFO = 3,
		SUBMIT_INFO = 4,
		MEMORY_ALLOCATE_INFO = 5,
		MAPPED_MEMORY_RANGE = 6,
		BIND_SPARSE_INFO = 7,
		FENCE_CREATE_INFO = 8,
		SEMAPHORE_CREATE_INFO = 9,
		EVENT_CREATE_INFO = 10,
		QUERY_POOL_CREATE_INFO = 11,
		BUFFER_CREATE_INFO = 12,
		BUFFER_VIEW_CREATE_INFO = 13,
		IMAGE_CREATE_INFO = 14,
		IMAGE_VIEW_CREATE_INFO = 15,
		SHADER_MODULE_CREATE_INFO = 16,
		PIPELINE_CACHE_CREATE_INFO = 17,
		PIPELINE_SHADER_STAGE_CREATE_INFO = 18,
		PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO = 19,
		PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO = 20,
		PIPELINE_TESSELLATION_STATE_CREATE_INFO = 21,
		PIPELINE_VIEWPORT_STATE_CREATE_INFO = 22,
		PIPELINE_RASTERIZATION_STATE_CREATE_INFO = 23,
		PIPELINE_MULTISAMPLE_STATE_CREATE_INFO = 24,
		PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO = 25,
		PIPELINE_COLOR_BLEND_STATE_CREATE_INFO = 26,
		PIPELINE_DYNAMIC_STATE_CREATE_INFO = 27,
		GRAPHICS_PIPELINE_CREATE_INFO = 28,
		COMPUTE_PIPELINE_CREATE_INFO = 29,
		PIPELINE_LAYOUT_CREATE_INFO = 30,
		SAMPLER_CREATE_INFO = 31,
		DESCRIPTOR_SET_LAYOUT_CREATE_INFO = 32,
		DESCRIPTOR_POOL_CREATE_INFO = 33,
		DESCRIPTOR_SET_ALLOCATE_INFO = 34,
		WRITE_DESCRIPTOR_SET = 35,
		COPY_DESCRIPTOR_SET = 36,
		FRAMEBUFFER_CREATE_INFO = 37,
		RENDER_PASS_CREATE_INFO = 38,
		COMMAND_POOL_CREATE_INFO = 39,
		COMMAND_BUFFER_ALLOCATE_INFO = 40,
		COMMAND_BUFFER_INHERITANCE_INFO = 41,
		COMMAND_BUFFER_BEGIN_INFO = 42,
		RENDER_PASS_BEGIN_INFO = 43,
		BUFFER_MEMORY_BARRIER = 44,
		IMAGE_MEMORY_BARRIER = 45,
		MEMORY_BARRIER = 46,
		LOADER_INSTANCE_CREATE_INFO = 47,
		LOADER_DEVICE_CREATE_INFO = 48,
		SWAPCHAIN_CREATE_INFO_KHR = 1000001000,
		PRESENT_INFO_KHR = 1000001001,
		DISPLAY_MODE_CREATE_INFO_KHR = 1000002000,
		DISPLAY_SURFACE_CREATE_INFO_KHR = 1000002001,
		DISPLAY_PRESENT_INFO_KHR = 1000003000,
		XLIB_SURFACE_CREATE_INFO_KHR = 1000004000,
		XCB_SURFACE_CREATE_INFO_KHR = 1000005000,
		WAYLAND_SURFACE_CREATE_INFO_KHR = 1000006000,
		MIR_SURFACE_CREATE_INFO_KHR = 1000007000,
		ANDROID_SURFACE_CREATE_INFO_KHR = 1000008000,
		WIN32_SURFACE_CREATE_INFO_KHR = 1000009000,
		DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT = 1000011000,
	};

	enum class SubpassContents
	{
		INLINE = 0,
		SECONDARY_COMMAND_BUFFERS = 1,
	};

	enum class SurfaceTransformFlagBits
	{
		IDENTITY = 0x00000001,
		ROTATE_90 = 0x00000002,
		ROTATE_180 = 0x00000004,
		ROTATE_270 = 0x00000008,
		HORIZONTAL_MIRROR = 0x00000010,
		HORIZONTAL_MIRROR_ROTATE_90 = 0x00000020,
		HORIZONTAL_MIRROR_ROTATE_180 = 0x00000040,
		HORIZONTAL_MIRROR_ROTATE_270 = 0x00000080,
		INHERIT = 0x00000100,
	};

	/// <summary>
	/// Each allocation has a scope which defines its lifetime and which object it is associated with.
	/// </summary>
	enum class SystemAllocationScope
	{
		/// <summary>
		/// The allocation is scoped to the lifetime of a vulkan command.
		/// </summary>
		COMMAND = 0,
		/// <summary>
		/// The allocation is scoped to the lifetime of the Vulkan object that is being created or used.
		/// </summary>
		OBJECT = 1,
		/// <summary>
		/// The allocation is scoped to the lifetime of a pipeline cache object.
		/// </summary>
		CACHE = 2,
		/// <summary>
		/// The allocation is scoped to the lifetime of the Vulkan device.
		/// </summary>
		DEVICE = 3,
		/// <summary>
		/// The allocation is scoped to the lifetime of the Vulkan instance.
		/// </summary>
		INSTANCE = 4,
	};

	enum class VertexInputRate
	{
		VERTEX = 0,
		INSTANCE = 1,
	};

	/// <summary>
	/// Returns the enum's name as a string, so it can be used at runtime.
	/// </summary>
	std::string GetName(Result result);

	/// <summary>
	/// Returns a general description of the code, and the situations in which it is returned.
	/// </summary>
	std::string GetDetails(Result result);

	/// <summary>
	/// Throws a <see cref="VulkanError" /> if the result has a negative integer value.
	/// </summary>
	void ThrowIfError(Result result, const char* message);

	/// <summary>
	/// Throws a <see cref="VulkanError" /> if the result has a negative integer value.
	/// </summary>
	void ThrowIfError(Result result, std::string message);
}
