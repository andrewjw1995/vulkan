#include "Offset3D.hpp"

using namespace vulkan;

Offset3D::Offset3D() :
	x(0),
	y(0),
	z(0)
{ }

Offset3D::Offset3D(uint32_t x, uint32_t y, uint32_t z) :
	x(x),
	y(y),
	z(z)
{ }
