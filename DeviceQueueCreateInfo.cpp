#include "DeviceQueueCreateInfo.hpp"

using namespace vulkan;

DeviceQueueCreateInfo::DeviceQueueCreateInfo() :
	Structure(StructureType::DEVICE_QUEUE_CREATE_INFO),
	flags(0),
	queueFamilyIndex(0),
	queueCount(0),
	pQueuePriorities(nullptr)
{ }

DeviceQueueCreateInfo& DeviceQueueCreateInfo::Flags(DeviceQueueCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

DeviceQueueCreateInfo& DeviceQueueCreateInfo::QueueFamilyIndex(uint32_t index)
{
	queueFamilyIndex = index;
	return *this;
}

DeviceQueueCreateInfo& DeviceQueueCreateInfo::Queues(uint32_t count, const float* pPriorities)
{
	queueCount = count;
	pQueuePriorities = pPriorities;
	return *this;
}
