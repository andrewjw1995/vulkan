#pragma once

#include "SpecializationMapEntry.hpp"

namespace vulkan
{
	class SpecializationInfo
	{
	public:
		SpecializationInfo();

		SpecializationInfo& MapEntries(uint32_t count, const SpecializationMapEntry* pEntries);
		SpecializationInfo& Data(size_t size, const void* pData);

	private:
		uint32_t                           mapEntryCount;
		const SpecializationMapEntry*      pMapEntries;
		size_t                             dataSize;
		const void*                        pData;
	};
}