#include "ImageSubresourceLayers.hpp"

using namespace vulkan;

ImageSubresourceLayers::ImageSubresourceLayers() :
	aspectMask(),
	mipLevel(0),
	baseArrayLayer(0),
	layerCount(0)
{ }

ImageSubresourceLayers& ImageSubresourceLayers::AspectMask(ImageAspectFlags mask)
{
	this->aspectMask = mask;
	return *this;
}

ImageSubresourceLayers& ImageSubresourceLayers::MipLevel(uint32_t level)
{
	this->mipLevel = level;
	return *this;
}

ImageSubresourceLayers& ImageSubresourceLayers::ArrayLayers(uint32_t base, uint32_t count)
{
	this->baseArrayLayer = base;
	this->layerCount = count;
	return *this;
}
