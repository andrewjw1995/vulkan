#pragma once

#include "Extent2D.hpp"
#include "Offset2D.hpp"

namespace vulkan
{
	class Window
	{
	public:
		virtual ~Window() { }
		virtual Extent2D Size() const = 0;
		virtual Offset2D Position() const = 0;
		virtual bool IsVisible() const = 0;
		virtual bool IsMaximised() const = 0;
		virtual bool IsResizeable() const = 0;
		virtual bool IsPositionable() const = 0;
		virtual void Hide() = 0;
		virtual void Show() = 0;
		virtual void Minimize() = 0;
		virtual void Maximize() = 0;
		virtual void Size(Extent2D size) = 0;
		virtual void Position(Offset2D position) = 0;
		virtual void ProcessEvents() = 0;
	};
}