#pragma once

#include "Enums.hpp"
#include "Platforms.hpp"

namespace vulkan
{
	typedef void* (VKAPI_PTR *PFN_vkAllocationFunction)(
		void*                                       pUserData,
		size_t                                      size,
		size_t                                      alignment,
		SystemAllocationScope                       allocationScope
	);

	typedef void* (VKAPI_PTR *PFN_vkReallocationFunction)(
		void*                                       pUserData,
		void*                                       pOriginal,
		size_t                                      size,
		size_t                                      alignment,
		SystemAllocationScope                       allocationScope
	);

	typedef void (VKAPI_PTR *PFN_vkFreeFunction)(
		void*                                       pUserData,
		void*                                       pMemory
	);

	typedef void (VKAPI_PTR *PFN_vkInternalAllocationNotification)(
		void*                                       pUserData,
		size_t                                      size,
		InternalAllocationType                      allocationType,
		SystemAllocationScope                       allocationScope
	);

	typedef void (VKAPI_PTR *PFN_vkInternalFreeNotification)(
		void*                                       pUserData,
		size_t                                      size,
		InternalAllocationType                      allocationType,
		SystemAllocationScope                       allocationScope
	);

	struct AllocationCallbacks
	{
		void*                                   pUserData;
		PFN_vkAllocationFunction                pfnAllocation;
		PFN_vkReallocationFunction              pfnReallocation;
		PFN_vkFreeFunction                      pfnFree;
		PFN_vkInternalAllocationNotification    pfnInternalAllocation;
		PFN_vkInternalFreeNotification          pfnInternalFree;
	};
}