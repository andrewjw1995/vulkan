#pragma once

#include "Extent3D.hpp"
#include "Flags.hpp"

namespace vulkan
{
	class QueueFamily
	{
	public:
		QueueFlags      Flags() const;
		uint32_t        QueueCount() const;
		uint32_t        TimestampValidBits() const;
		Extent3D        MinImageTransferGranularity() const;

	private:
		QueueFlags      queueFlags;
		uint32_t        queueCount;
		uint32_t        timestampValidBits;
		Extent3D        minImageTransferGranularity;
	};
}