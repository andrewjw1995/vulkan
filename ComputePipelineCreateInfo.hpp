#pragma once

#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "PipelineShaderStageCreateInfo.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class ComputePipelineCreateInfo : public Structure
	{
	public:
		ComputePipelineCreateInfo();

		ComputePipelineCreateInfo& Flags(PipelineCreateFlags flags);
		ComputePipelineCreateInfo& Stage(PipelineShaderStageCreateInfo stage);
		ComputePipelineCreateInfo& Layout(PipelineLayout layout);
		ComputePipelineCreateInfo& BasePipeline(Pipeline handle);
		ComputePipelineCreateInfo& BasePipeline(int32_t index);

	private:
		PipelineCreateFlags              flags;
		PipelineShaderStageCreateInfo    stage;
		PipelineLayout                   layout;
		Pipeline                         basePipelineHandle;
		int32_t                          basePipelineIndex;
	};
}