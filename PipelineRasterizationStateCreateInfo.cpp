#include "PipelineRasterizationStateCreateInfo.hpp"

using namespace vulkan;

PipelineRasterizationStateCreateInfo::PipelineRasterizationStateCreateInfo() :
	Structure(StructureType::PIPELINE_RASTERIZATION_STATE_CREATE_INFO),
	flags(),
	depthClampEnable(false),
	polygonMode(PolygonMode::FILL),
	cullMode(),
	frontFace(FrontFace::COUNTER_CLOCKWISE),
	depthBiasEnable(false),
	depthBiasConstantFactor(0),
	depthBiasClamp(0),
	depthBiasSlopeFactor(0),
	lineWidth(0)
{ }

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::Flags(PipelineRasterizationStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::EnableDepthClamp()
{
	this->depthClampEnable = true;
	return *this;
}

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::DisableDepthClamp()
{
	this->depthClampEnable = false;
	return *this;
}

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::PolygonMode(vulkan::PolygonMode mode)
{
	this->polygonMode = mode;
	return *this;
}

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::CullMode(CullModeFlags mode)
{
	this->cullMode = mode;
	return *this;
}

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::FrontFace(vulkan::FrontFace face)
{
	this->frontFace = face;
	return *this;
}

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::EnableDepthBias(float constant, float clamp, float slope)
{
	this->depthBiasEnable = true;
	this->depthBiasConstantFactor = constant;
	this->depthBiasClamp = clamp;
	this->depthBiasSlopeFactor = slope;
	return *this;
}

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::DisableDepthBias()
{
	this->depthBiasEnable = false;
	return *this;
}

PipelineRasterizationStateCreateInfo& PipelineRasterizationStateCreateInfo::LineWidth(float width)
{
	this->lineWidth = width;
	return *this;
}
