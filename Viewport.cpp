#include "Viewport.hpp"

using namespace vulkan;

Viewport::Viewport() :
	x(0),
	y(0),
	width(0),
	height(0),
	minDepth(0),
	maxDepth(0)
{ }

Viewport::Viewport(Offset2D offset, Extent2D extent, float minDepth, float maxDepth) :
	x((float)offset.x),
	y((float)offset.y),
	width((float)extent.width),
	height((float)extent.height),
	minDepth(minDepth),
	maxDepth(maxDepth)
{ }

Viewport::Viewport(float x, float y, float width, float height, float minDepth, float maxDepth) :
	x(x),
	y(y),
	width(width),
	height(height),
	minDepth(minDepth),
	maxDepth(maxDepth)
{ }

Viewport& Viewport::Offset(float x, float y)
{
	this->x = x;
	this->y = y;
	return *this;
}

Viewport& Viewport::Offset(Offset2D offset)
{
	this->x = (float)offset.x;
	this->y = (float)offset.y;
	return *this;
}

Viewport& Viewport::Extent(float width, float height)
{
	this->width = width;
	this->height = height;
	return *this;
}

Viewport& Viewport::Extent(Extent2D extent)
{
	this->width = (float)extent.width;
	this->height = (float)extent.height;
	return *this;
}

Viewport& Viewport::Depth(float min, float max)
{
	this->minDepth = min;
	this->maxDepth = max;
	return *this;
}
