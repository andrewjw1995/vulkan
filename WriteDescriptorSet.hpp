#pragma once

#include "DescriptorBufferInfo.hpp"
#include "DescriptorImageInfo.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class WriteDescriptorSet : public Structure
	{
	public:
		WriteDescriptorSet();

		WriteDescriptorSet& Destination(DescriptorSet set, uint32_t binding, uint32_t arrayElement);
		WriteDescriptorSet& Descriptor(DescriptorType type, uint32_t count);
		WriteDescriptorSet& Descriptor(DescriptorType type, uint32_t count, const DescriptorImageInfo* pInfo);
		WriteDescriptorSet& Descriptor(DescriptorType type, uint32_t count, const DescriptorBufferInfo* pInfo);
		WriteDescriptorSet& Descriptor(DescriptorType type, uint32_t count, const BufferView* pInfo);

	private:
		DescriptorSet                  dstSet;
		uint32_t                       dstBinding;
		uint32_t                       dstArrayElement;
		uint32_t                       descriptorCount;
		DescriptorType                 descriptorType;
		const DescriptorImageInfo*     pImageInfo;
		const DescriptorBufferInfo*    pBufferInfo;
		const BufferView*              pTexelBufferView;
	};
}