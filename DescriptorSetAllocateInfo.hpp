#pragma once

#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class DescriptorSetAllocateInfo : public Structure
	{
	public:
		DescriptorSetAllocateInfo();

		DescriptorSetAllocateInfo& DescriptorPool(DescriptorPool pool);
		DescriptorSetAllocateInfo& DescriptorSets(uint32_t count, const DescriptorSetLayout* pSetLayouts);
		uint32_t DescriptorSetCount() const;

	private:
		vulkan::DescriptorPool        descriptorPool;
		uint32_t                      descriptorSetCount;
		const DescriptorSetLayout*    pSetLayouts;
	};
}