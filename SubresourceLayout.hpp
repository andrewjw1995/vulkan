#pragma once

#include "DeviceSize.hpp"

namespace vulkan
{
	class SubresourceLayout
	{
	public:
		SubresourceLayout();

		SubresourceLayout& Offset(DeviceSize offset);
		SubresourceLayout& Size(DeviceSize size);
		SubresourceLayout& RowPitch(DeviceSize pitch);
		SubresourceLayout& ArrayPitch(DeviceSize pitch);
		SubresourceLayout& DepthPitch(DeviceSize pitch);
		DeviceSize Offset() const;
		DeviceSize Size() const;
		DeviceSize RowPitch() const;
		DeviceSize ArrayPitch() const;
		DeviceSize DepthPitch() const;

	private:
		DeviceSize offset;
		DeviceSize size;
		DeviceSize rowPitch;
		DeviceSize arrayPitch;
		DeviceSize depthPitch;
	};
}