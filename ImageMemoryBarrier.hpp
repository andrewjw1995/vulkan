#pragma once

#include "Flags.hpp"
#include "ImageSubresourceRange.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class ImageMemoryBarrier : public Structure
	{
	public:
		ImageMemoryBarrier();

		ImageMemoryBarrier& Source(AccessFlags accessMask, uint32_t queueFamily);
		ImageMemoryBarrier& Destination(AccessFlags accessMask, uint32_t queueFamily);
		ImageMemoryBarrier& Image(Image image, ImageSubresourceRange subresourceRange, ImageLayout oldLayout, ImageLayout newLayout);

	private:
		AccessFlags              srcAccessMask;
		AccessFlags              dstAccessMask;
		ImageLayout              oldLayout;
		ImageLayout              newLayout;
		uint32_t                 srcQueueFamilyIndex;
		uint32_t                 dstQueueFamilyIndex;
		vulkan::Image            image;
		ImageSubresourceRange    subresourceRange;
	};
}