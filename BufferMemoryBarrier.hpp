#pragma once

#include "DeviceSize.hpp"
#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class BufferMemoryBarrier : public Structure
	{
	public:
		BufferMemoryBarrier();

		BufferMemoryBarrier& Source(AccessFlags accessMask, uint32_t queueFamily);
		BufferMemoryBarrier& Destination(AccessFlags accessMask, uint32_t queueFamily);
		BufferMemoryBarrier& Buffer(Buffer buffer, DeviceSize offset, DeviceSize size);

	private:
		AccessFlags      srcAccessMask;
		AccessFlags      dstAccessMask;
		uint32_t         srcQueueFamilyIndex;
		uint32_t         dstQueueFamilyIndex;
		vulkan::Buffer   buffer;
		DeviceSize       offset;
		DeviceSize       size;
	};
}