#pragma once

#include "Enums.hpp"

namespace vulkan
{
	class VertexInputAttributeDescription
	{
	public:
		VertexInputAttributeDescription(uint32_t location, uint32_t binding, Format format, uint32_t offset);

	private:
		uint32_t    location;
		uint32_t    binding;
		Format      format;
		uint32_t    offset;
	};
}