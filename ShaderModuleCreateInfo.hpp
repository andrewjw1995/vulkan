#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class ShaderModuleCreateInfo : public Structure
	{
	public:
		ShaderModuleCreateInfo();
		ShaderModuleCreateInfo(size_t codeSize, const uint32_t* pCode);

		ShaderModuleCreateInfo& Flags(ShaderModuleCreateFlags flags);
		ShaderModuleCreateInfo& Code(size_t size, const uint32_t* pCode);

	private:
		ShaderModuleCreateFlags      flags;
		size_t                       codeSize;
		const uint32_t*              pCode;
	};
}