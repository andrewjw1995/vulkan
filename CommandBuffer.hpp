#pragma once

#include "BufferCopy.hpp"
#include "BufferImageCopy.hpp"
#include "BufferMemoryBarrier.hpp"
#include "ClearAttachment.hpp"
#include "ClearRectangle.hpp"
#include "ClearValue.hpp"
#include "CommandBufferBeginInfo.hpp"
#include "DeviceSize.hpp"
#include "DispatchableHandle.hpp"
#include "Flags.hpp"
#include "ImageBlit.hpp"
#include "ImageCopy.hpp"
#include "ImageMemoryBarrier.hpp"
#include "ImageSubresourceRange.hpp"
#include "ImageResolve.hpp"
#include "MemoryBarrier.hpp"
#include "Rectangle.hpp"
#include "RenderPassBeginInfo.hpp"
#include "Viewport.hpp"

namespace vulkan
{
	class CommandBuffer : public DispatchableHandle
	{
	public:
		void Begin(const CommandBufferBeginInfo& info) const;
		void End() const;
		void Reset(CommandBufferResetFlags flags) const;
		void BindPipeline(PipelineBindPoint bindPoint, Pipeline pipeline) const;
		void SetViewport(uint32_t first, uint32_t count, const Viewport* pViewports) const;
		void SetScissor(uint32_t first, uint32_t count, const Rectangle* pScissors) const;
		void SetLineWidth(float width) const;
		void SetDepthBias(float constant, float clamp, float slope) const;
		void SetBlendConstants(float r, float g, float b, float a) const;
		void SetDepthBounds(float min, float max) const;
		void SetStencilCompareMask(StencilFaceFlags faceMask, uint32_t compareMask) const;
		void SetStencilWriteMask(StencilFaceFlags faceMask, uint32_t writeMask) const;
		void SetStencilReference(StencilFaceFlags faceMask, uint32_t reference) const;
		void BindDescriptorSets(
			PipelineBindPoint                           bindPoint,
			PipelineLayout                              layout,
			uint32_t                                    firstSet,
			uint32_t                                    descriptorSetCount,
			const DescriptorSet*                        pSets,
			uint32_t                                    dynamicOffsetCount,
			const uint32_t*                             pDynamicOffsets) const;
		void BindIndexBuffer(Buffer buffer, DeviceSize offset, IndexType type) const;
		void BindVertexBuffers(uint32_t first, uint32_t count, const Buffer* pBuffers, const DeviceSize* pOffsets) const;
		void Draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t firstVertex, uint32_t firstInstance) const;
		void DrawIndexed(
			uint32_t                                    indexCount,
			uint32_t                                    instanceCount,
			uint32_t                                    firstIndex,
			int32_t                                     vertexOffset,
			uint32_t                                    firstInstance) const;
		void DrawIndirect(Buffer buffer, DeviceSize offset, uint32_t drawCount, uint32_t stride) const;
		void DrawIndexedIndirect(Buffer buffer, DeviceSize offset, uint32_t drawCount, uint32_t stride) const;
		void Dispatch(uint32_t x, uint32_t y, uint32_t z) const;
		void DispatchIndirect(Buffer buffer, DeviceSize offset) const;
		void CopyBuffer(Buffer srcBuffer, Buffer dstBuffer, uint32_t regionCount, const BufferCopy* pRegions) const;
		void CopyImage(
			Image                                       srcImage,
			ImageLayout                                 srcImageLayout,
			Image                                       dstImage,
			ImageLayout                                 dstImageLayout,
			uint32_t                                    regionCount,
			const ImageCopy*                            pRegions) const;
		void BlitImage(
			Image                                       srcImage,
			ImageLayout                                 srcImageLayout,
			Image                                       dstImage,
			ImageLayout                                 dstImageLayout,
			uint32_t                                    regionCount,
			const ImageBlit*                            pRegions,
			Filter                                      filter) const;
		void CopyBufferToImage(
			Buffer                                      srcBuffer,
			Image                                       dstImage,
			ImageLayout                                 dstImageLayout,
			uint32_t                                    regionCount,
			const BufferImageCopy*                      pRegions) const;
		void CopyImageToBuffer(
			Image                                       srcImage,
			ImageLayout                                 srcImageLayout,
			Buffer                                      dstBuffer,
			uint32_t                                    regionCount,
			const BufferImageCopy*                      pRegions) const;
		void UpdateBuffer(
			Buffer                                      dstBuffer,
			DeviceSize                                  dstOffset,
			DeviceSize                                  dataSize,
			const uint32_t*                             pData) const;
		void FillBuffer(
			Buffer                                      dstBuffer,
			DeviceSize                                  dstOffset,
			DeviceSize                                  size,
			uint32_t                                    data) const;
		void ClearColorImage(
			Image                                       image,
			ImageLayout                                 imageLayout,
			const ClearColorValue*                      pColor,
			uint32_t                                    rangeCount,
			const ImageSubresourceRange*                pRanges) const;
		void ClearDepthStencilImage(
			Image                                       image,
			ImageLayout                                 imageLayout,
			const ClearDepthStencilValue*               pDepthStencil,
			uint32_t                                    rangeCount,
			const ImageSubresourceRange*                pRanges) const;
		void ClearAttachments(
			uint32_t                                    attachmentCount,
			const ClearAttachment*                      pAttachments,
			uint32_t                                    rectCount,
			const ClearRectangle*                       pRects) const;
		void ResolveImage(
			Image                                       srcImage,
			ImageLayout                                 srcImageLayout,
			Image                                       dstImage,
			ImageLayout                                 dstImageLayout,
			uint32_t                                    regionCount,
			const ImageResolve*                         pRegions) const;
		void SetEvent(Event event, PipelineStageFlags stageMask) const;
		void ResetEvent(Event event, PipelineStageFlags stageMask) const;
		void WaitEvents(
			uint32_t                                    eventCount,
			const Event*                                pEvents,
			PipelineStageFlags                          srcStageMask,
			PipelineStageFlags                          dstStageMask,
			uint32_t                                    memoryBarrierCount,
			const MemoryBarrier*                        pMemoryBarriers,
			uint32_t                                    bufferMemoryBarrierCount,
			const BufferMemoryBarrier*                  pBufferMemoryBarriers,
			uint32_t                                    imageMemoryBarrierCount,
			const ImageMemoryBarrier*                   pImageMemoryBarriers) const;
		void PipelineBarrier(
			PipelineStageFlags                          srcStageMask,
			PipelineStageFlags                          dstStageMask,
			DependencyFlags                             dependencyFlags,
			uint32_t                                    memoryBarrierCount,
			const MemoryBarrier*                        pMemoryBarriers,
			uint32_t                                    bufferMemoryBarrierCount,
			const BufferMemoryBarrier*                  pBufferMemoryBarriers,
			uint32_t                                    imageMemoryBarrierCount,
			const ImageMemoryBarrier*                   pImageMemoryBarriers) const;
		void BeginQuery(QueryPool queryPool, uint32_t query, QueryControlFlags flags) const;
		void EndQuery(QueryPool queryPool, uint32_t query) const;
		void ResetQueryPool(QueryPool queryPool, uint32_t firstQuery, uint32_t queryCount) const;
		void WriteTimestamp(PipelineStageFlagBits pipelineStage, QueryPool queryPool, uint32_t query) const;
		void CopyQueryPoolResults(
			QueryPool                                   queryPool,
			uint32_t                                    firstQuery,
			uint32_t                                    queryCount,
			Buffer                                      dstBuffer,
			DeviceSize                                  dstOffset,
			DeviceSize                                  stride,
			QueryResultFlags                            flags) const;
		void PushConstants(
			PipelineLayout                              layout,
			ShaderStageFlags                            stageFlags,
			uint32_t                                    offset,
			uint32_t                                    size,
			const void*                                 pValues) const;
		void BeginRenderPass(const RenderPassBeginInfo& pRenderPassBegin, SubpassContents contents) const;
		void NextSubpass(SubpassContents contents) const;
		void EndRenderPass() const;
		void ExecuteCommands(uint32_t commandBufferCount, const CommandBuffer* pCommandBuffers) const;
	};
}