#include "MemoryBarrier.hpp"

using namespace vulkan;

MemoryBarrier::MemoryBarrier() :
	Structure(StructureType::MEMORY_BARRIER),
	srcAccessMask(),
	dstAccessMask()
{ }

MemoryBarrier& MemoryBarrier::Source(AccessFlags accessMask)
{
	this->srcAccessMask = accessMask;
	return *this;
}

MemoryBarrier& MemoryBarrier::Destination(AccessFlags accessMask)
{
	this->dstAccessMask = accessMask;
	return *this;
}
