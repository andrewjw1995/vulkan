#include "CommandBufferInheritanceInfo.hpp"

using namespace vulkan;

CommandBufferInheritanceInfo::CommandBufferInheritanceInfo() :
	Structure(StructureType::COMMAND_BUFFER_INHERITANCE_INFO),
	renderPass(),
	subpass(0),
	framebuffer(),
	occlusionQueryEnable(false),
	queryFlags(),
	pipelineStatistics()
{ }

CommandBufferInheritanceInfo& CommandBufferInheritanceInfo::RenderPass(vulkan::RenderPass renderPass, uint32_t subpass)
{
	this->renderPass = renderPass;
	this->subpass = subpass;
	return *this;
}

CommandBufferInheritanceInfo& CommandBufferInheritanceInfo::Framebuffer(vulkan::Framebuffer framebuffer)
{
	this->framebuffer = framebuffer;
	return *this;
}

CommandBufferInheritanceInfo& CommandBufferInheritanceInfo::EnableOcclusionQuery()
{
	this->occlusionQueryEnable = true;
	return *this;
}

CommandBufferInheritanceInfo& CommandBufferInheritanceInfo::DisableOcclusionQuery()
{
	this->occlusionQueryEnable = false;
	return *this;
}

CommandBufferInheritanceInfo& CommandBufferInheritanceInfo::QueryFlags(QueryControlFlags flags)
{
	this->queryFlags = flags;
	return *this;
}

CommandBufferInheritanceInfo& CommandBufferInheritanceInfo::PipelineStatistics(QueryPipelineStatisticFlags flags)
{
	this->pipelineStatistics = flags;
	return *this;
}
