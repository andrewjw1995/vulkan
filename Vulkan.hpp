#pragma once

#include "Extension.hpp"
#include "Instance.hpp"
#include "InstanceCreateInfo.hpp"
#include "Layer.hpp"

#include <vector>

namespace vulkan
{
	std::vector<Extension> EnumerateExtensions(const char* pLayer);
	std::vector<Layer> EnumerateLayers();
	Instance CreateInstance(const InstanceCreateInfo& info);
}
