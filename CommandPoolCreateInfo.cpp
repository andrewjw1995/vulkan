#include "CommandPoolCreateInfo.hpp"

using namespace vulkan;

CommandPoolCreateInfo::CommandPoolCreateInfo() :
	Structure(StructureType::COMMAND_POOL_CREATE_INFO),
	flags(),
	queueFamilyIndex(0)
{ }

CommandPoolCreateInfo& CommandPoolCreateInfo::Flags(CommandPoolCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

CommandPoolCreateInfo& CommandPoolCreateInfo::QueueFamily(uint32_t index)
{
	this->queueFamilyIndex = index;
	return *this;
}
