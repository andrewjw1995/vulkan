#include "SubresourceLayout.hpp"

using namespace vulkan;

SubresourceLayout::SubresourceLayout() :
	offset(0),
	size(0),
	rowPitch(0),
	arrayPitch(0),
	depthPitch(0)
{ }

SubresourceLayout& SubresourceLayout::Offset(DeviceSize offset)
{
	this->offset = offset;
	return *this;
}

SubresourceLayout& SubresourceLayout::Size(DeviceSize size)
{
	this->size = size;
	return *this;
}

SubresourceLayout& SubresourceLayout::RowPitch(DeviceSize pitch)
{
	this->rowPitch = pitch;
	return *this;
}

SubresourceLayout& SubresourceLayout::ArrayPitch(DeviceSize pitch)
{
	this->arrayPitch = pitch;
	return *this;
}

SubresourceLayout& SubresourceLayout::DepthPitch(DeviceSize pitch)
{
	this->depthPitch = pitch;
	return *this;
}

DeviceSize SubresourceLayout::Offset() const
{
	return offset;
}

DeviceSize SubresourceLayout::Size() const
{
	return size;
}

DeviceSize SubresourceLayout::RowPitch() const
{
	return rowPitch;
}

DeviceSize SubresourceLayout::ArrayPitch() const
{
	return arrayPitch;
}

DeviceSize SubresourceLayout::DepthPitch() const
{
	return depthPitch;
}
