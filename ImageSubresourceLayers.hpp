#pragma once

#include "Flags.hpp"

namespace vulkan
{
	class ImageSubresourceLayers
	{
	public:
		ImageSubresourceLayers();

		ImageSubresourceLayers& AspectMask(ImageAspectFlags mask);
		ImageSubresourceLayers& MipLevel(uint32_t level);
		ImageSubresourceLayers& ArrayLayers(uint32_t base, uint32_t count);

	private:
		ImageAspectFlags      aspectMask;
		uint32_t              mipLevel;
		uint32_t              baseArrayLayer;
		uint32_t              layerCount;
	};
}