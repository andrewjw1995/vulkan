#pragma once

#include "DeviceSize.hpp"
#include "Extent3D.hpp"
#include "Flags.hpp"

namespace vulkan
{
	class ImageFormatProperties
	{
	public:
		Extent3D              MaxExtent() const;
		uint32_t              MaxMipLevels() const;
		uint32_t              MaxArrayLayers() const;
		SampleCountFlags      SampleCounts() const;
		DeviceSize            MaxResourceSize() const;

	private:
		Extent3D              maxExtent;
		uint32_t              maxMipLevels;
		uint32_t              maxArrayLayers;
		SampleCountFlags      sampleCounts;
		DeviceSize            maxResourceSize;
	};
}