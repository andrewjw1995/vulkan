#include "PhysicalDeviceSparseProperties.hpp"

using namespace vulkan;

Bool32 PhysicalDeviceSparseProperties::Standard2DBlockShape() const
{
	return residencyStandard2DBlockShape;
}

Bool32 PhysicalDeviceSparseProperties::Standard2DMultisampleBlockShape() const
{
	return residencyStandard2DMultisampleBlockShape;
}

Bool32 PhysicalDeviceSparseProperties::Standard3DBlockShape() const
{
	return residencyStandard3DBlockShape;
}

Bool32 PhysicalDeviceSparseProperties::AlignedMipSize() const
{
	return residencyAlignedMipSize;
}

Bool32 PhysicalDeviceSparseProperties::NonResidentStrict() const
{
	return residencyNonResidentStrict;
}
