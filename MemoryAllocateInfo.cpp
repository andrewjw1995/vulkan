#include "MemoryAllocateInfo.hpp"

using namespace vulkan;

MemoryAllocateInfo::MemoryAllocateInfo() :
	Structure(StructureType::MEMORY_ALLOCATE_INFO),
	allocationSize(0),
	memoryTypeIndex(0)
{ }

MemoryAllocateInfo& MemoryAllocateInfo::AllocationSize(DeviceSize size)
{
	allocationSize = size;
	return *this;
}

MemoryAllocateInfo& MemoryAllocateInfo::MemoryTypeIndex(uint32_t index)
{
	memoryTypeIndex = index;
	return *this;
}

DeviceSize MemoryAllocateInfo::AllocationSize() const
{
	return allocationSize;
}

uint32_t MemoryAllocateInfo::MemoryTypeIndex() const
{
	return memoryTypeIndex;
}
