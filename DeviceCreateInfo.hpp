#pragma once

#include "DeviceQueueCreateInfo.hpp"
#include "Flags.hpp"
#include "PhysicalDeviceFeatures.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class DeviceCreateInfo : public Structure
	{
	public:
		DeviceCreateInfo();

		DeviceCreateInfo& Flags(DeviceCreateFlags value);
		DeviceCreateInfo& QueueCreateInfos(uint32_t count, const DeviceQueueCreateInfo* pInfos);
		DeviceCreateInfo& EnabledLayers(uint32_t count, const char* const* ppNames);
		DeviceCreateInfo& EnabledExtensions(uint32_t count, const char* const* ppNames);
		DeviceCreateInfo& EnabledFeatures(const PhysicalDeviceFeatures* pFeatures);

	private:
		DeviceCreateFlags                  flags;
		uint32_t                           queueCreateInfoCount;
		const DeviceQueueCreateInfo*       pQueueCreateInfos;
		uint32_t                           enabledLayerCount;
		const char* const*                 ppEnabledLayerNames;
		uint32_t                           enabledExtensionCount;
		const char* const*                 ppEnabledExtensionNames;
		const PhysicalDeviceFeatures*      pEnabledFeatures;
	};
}