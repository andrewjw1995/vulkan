#pragma once

#include <cstdint>

namespace vulkan
{
	union ClearColorValue
	{
		float       float32[4];
		int32_t     int32[4];
		uint32_t    uint32[4];
	};

	struct ClearDepthStencilValue
	{
		float       depth;
		uint32_t    stencil;
	};

	union ClearValue
	{
	public:
		static ClearValue Float32(float r, float g, float b, float a);
		static ClearValue Int32(int32_t r, int32_t g, int32_t b, int32_t a);
		static ClearValue UInt32(uint32_t r, uint32_t g, uint32_t b, uint32_t a);
		static ClearValue DepthStencil(float depth, uint32_t stencil);
		
		ClearColorValue color;
		ClearDepthStencilValue depthStencil;
	};
}