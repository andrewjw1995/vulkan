#include "ClearRectangle.hpp"

using namespace vulkan;

ClearRectangle::ClearRectangle() :
	rect(),
	baseArrayLayer(0),
	layerCount(1)
{ }

ClearRectangle::ClearRectangle(vulkan::Rectangle rectangle, uint32_t baseArrayLayer, uint32_t layerCount) :
	rect(rectangle),
	baseArrayLayer(baseArrayLayer),
	layerCount(layerCount)
{ }

ClearRectangle& ClearRectangle::Rectangle(vulkan::Rectangle rectangle)
{
	this->rect = rectangle;
	return *this;
}

ClearRectangle& ClearRectangle::ArrayLayers(uint32_t base, uint32_t count)
{
	this->baseArrayLayer = base;
	this->layerCount = count;
	return *this;
}
