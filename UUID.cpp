#include "UUID.hpp"

using namespace std;
using namespace vulkan;

UUID::UUID()
{
	for (uint32_t i = 0; i < UUID::SIZE; i++)
		this->value[i] = 0;
}

UUID::UUID(const uint8_t value[UUID::SIZE])
{
	for (uint32_t i = 0; i < UUID::SIZE; i++)
		this->value[i] = value[i];
}

ostream& vulkan::operator<<(ostream& out, const UUID& value)
{
	const char* const HEX = "0123456789ABCDEF";
	for (uint32_t i = 0; i < UUID::SIZE; i++)
	{
		out << HEX[value.value[i] >> 4];
		out << HEX[value.value[i] & 0xF];
	}
	return out;
}