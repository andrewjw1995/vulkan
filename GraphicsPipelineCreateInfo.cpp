#include "GraphicsPipelineCreateInfo.hpp"

using namespace vulkan;

GraphicsPipelineCreateInfo::GraphicsPipelineCreateInfo() :
	Structure(StructureType::GRAPHICS_PIPELINE_CREATE_INFO),
	flags(),
	stageCount(0),
	pStages(nullptr),
	pVertexInputState(nullptr),
	pInputAssemblyState(nullptr),
	pTessellationState(nullptr),
	pViewportState(nullptr),
	pRasterizationState(nullptr),
	pMultisampleState(nullptr),
	pDepthStencilState(nullptr),
	pColorBlendState(nullptr),
	pDynamicState(nullptr),
	layout(),
	renderPass(),
	subpass(0),
	basePipelineHandle(),
	basePipelineIndex(-1)
{ }

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::Flags(PipelineCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::Stages(uint32_t count, const PipelineShaderStageCreateInfo* pStages)
{
	this->stageCount = count;
	this->pStages = pStages;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::VertexInputState(const PipelineVertexInputStateCreateInfo* pInfo)
{
	this->pVertexInputState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::InputAssemblyState(const PipelineInputAssemblyStateCreateInfo* pInfo)
{
	this->pInputAssemblyState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::TessellationState(const PipelineTessellationStateCreateInfo* pInfo)
{
	this->pTessellationState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::ViewportState(const PipelineViewportStateCreateInfo* pInfo)
{
	this->pViewportState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::RasterizationState(const PipelineRasterizationStateCreateInfo* pInfo)
{
	this->pRasterizationState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::MultisampleState(const PipelineMultisampleStateCreateInfo* pInfo)
{
	this->pMultisampleState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::DepthStencilState(const PipelineDepthStencilStateCreateInfo* pInfo)
{
	this->pDepthStencilState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::ColorBlendState(const PipelineColorBlendStateCreateInfo* pInfo)
{
	this->pColorBlendState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::DynamicState(const PipelineDynamicStateCreateInfo* pInfo)
{
	this->pDynamicState = pInfo;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::Layout(PipelineLayout layout)
{
	this->layout = layout;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::RenderPass(vulkan::RenderPass handle)
{
	this->renderPass = handle;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::Subpass(uint32_t index)
{
	this->subpass = index;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::BasePipeline(Pipeline handle)
{
	this->basePipelineHandle = handle;
	this->basePipelineIndex = -1;
	return *this;
}

GraphicsPipelineCreateInfo& GraphicsPipelineCreateInfo::BasePipeline(int32_t index)
{
	this->basePipelineIndex = index;
	this->basePipelineHandle = Pipeline();
	return *this;
}
