#include "SparseImageMemoryBind.hpp"

using namespace vulkan;

SparseImageMemoryBind::SparseImageMemoryBind() = default;

SparseImageMemoryBind& SparseImageMemoryBind::Subresource(ImageSubresource subresource)
{
	this->subresource = subresource;
	return *this;
}

SparseImageMemoryBind& SparseImageMemoryBind::Offset(Offset3D offset)
{
	this->subresource = subresource;
	return *this;
}

SparseImageMemoryBind& SparseImageMemoryBind::Extent(Extent3D extent)
{
	this->extent = extent;
	return *this;
}

SparseImageMemoryBind& SparseImageMemoryBind::Memory(DeviceMemory memory)
{
	this->memory = memory;
	return *this;
}

SparseImageMemoryBind& SparseImageMemoryBind::MemoryOffset(DeviceSize offset)
{
	this->memoryOffset = offset;
	return *this;
}

SparseImageMemoryBind& SparseImageMemoryBind::Flags(SparseMemoryBindFlags flags)
{
	this->flags = flags;
	return *this;
}
