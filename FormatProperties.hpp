#pragma once

#include "Flags.hpp"

namespace vulkan
{
	class FormatProperties
	{
	public:
		FormatFeatureFlags LinearTilingFeatures() const;
		FormatFeatureFlags OptimalTilingFeatures() const;
		FormatFeatureFlags BufferFeatures() const;

	private:
		FormatFeatureFlags linearTilingFeatures;
		FormatFeatureFlags optimalTilingFeatures;
		FormatFeatureFlags bufferFeatures;
	};
}