#include "ClearValue.hpp"

using namespace vulkan;

ClearValue ClearValue::Float32(float r, float g, float b, float a)
{
	ClearValue value;
	value.color.float32[0] = r;
	value.color.float32[1] = g;
	value.color.float32[2] = b;
	value.color.float32[3] = a;
	return value;
}

ClearValue ClearValue::Int32(int32_t r, int32_t g, int32_t b, int32_t a)
{
	ClearValue value;
	value.color.int32[0] = r;
	value.color.int32[1] = g;
	value.color.int32[2] = b;
	value.color.int32[3] = a;
	return value;
}

ClearValue ClearValue::UInt32(uint32_t r, uint32_t g, uint32_t b, uint32_t a)
{
	ClearValue value;
	value.color.uint32[0] = r;
	value.color.uint32[1] = g;
	value.color.uint32[2] = b;
	value.color.uint32[3] = a;
	return value;
}

ClearValue ClearValue::DepthStencil(float depth, uint32_t stencil)
{
	ClearValue value;
	value.depthStencil.depth = depth;
	value.depthStencil.stencil = stencil;
	return value;
}
