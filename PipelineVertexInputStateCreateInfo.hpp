#pragma once

#include "Flags.hpp"
#include "Structure.hpp"
#include "VertexInputAttributeDescription.hpp"
#include "VertexInputBindingDescription.hpp"

namespace vulkan
{
	class PipelineVertexInputStateCreateInfo : public Structure
	{
	public:
		PipelineVertexInputStateCreateInfo();

		PipelineVertexInputStateCreateInfo& Flags(PipelineVertexInputStateCreateFlags flags);
		PipelineVertexInputStateCreateInfo& Bindings(uint32_t count, const VertexInputBindingDescription* pDescriptions);
		PipelineVertexInputStateCreateInfo& Attributes(uint32_t count, const VertexInputAttributeDescription* pDescriptions);

	private:
		PipelineVertexInputStateCreateFlags       flags;
		uint32_t                                  vertexBindingDescriptionCount;
		const VertexInputBindingDescription*      pVertexBindingDescriptions;
		uint32_t                                  vertexAttributeDescriptionCount;
		const VertexInputAttributeDescription*    pVertexAttributeDescriptions;
	};
}