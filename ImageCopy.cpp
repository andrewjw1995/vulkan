#include "ImageCopy.hpp"

using namespace vulkan;

ImageCopy::ImageCopy() :
	srcSubresource(),
	srcOffset(),
	dstSubresource(),
	dstOffset(),
	extent()
{ }

ImageCopy& ImageCopy::Source(ImageSubresourceLayers subresource, Offset3D offset)
{
	this->srcSubresource = subresource;
	this->srcOffset = offset;
	return *this;
}

ImageCopy& ImageCopy::Destination(ImageSubresourceLayers subresource, Offset3D offset)
{
	this->dstSubresource = subresource;
	this->dstOffset = offset;
	return *this;
}

ImageCopy& ImageCopy::Extent(Extent3D extent)
{
	this->extent = extent;
	return *this;
}
