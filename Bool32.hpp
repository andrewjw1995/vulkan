#pragma once

#include <cstdint>
#include <iostream>

namespace vulkan
{
	class Bool32
	{
	public:
		Bool32();
		Bool32(bool value);
		Bool32(uint32_t value);

		Bool32 operator&(Bool32 other) const;
		Bool32 operator|(Bool32 other) const;
		Bool32 operator^(Bool32 other) const;
		Bool32& operator&=(Bool32 other);
		Bool32& operator|=(Bool32 other);
		Bool32& operator^=(Bool32 other);
		operator bool() const;
		friend std::ostream& operator <<(std::ostream& out, const Bool32& value);

	private:
		uint32_t value;
	};
}