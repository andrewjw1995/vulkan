#include "DescriptorImageInfo.hpp"

using namespace vulkan;

DescriptorImageInfo::DescriptorImageInfo(Sampler sampler, ImageView imageView, ImageLayout imageLayout) :
	sampler(sampler),
	imageView(imageView),
	imageLayout(imageLayout)
{ }
