#include "Offset2D.hpp"

using namespace vulkan;

Offset2D::Offset2D() :
	x(0),
	y(0)
{ }

Offset2D::Offset2D(uint32_t x, uint32_t y) :
	x(x),
	y(y)
{ }
