#pragma once

#include <cstdint>

namespace vulkan
{
	class NonDispatchableHandle
	{
	public:
		NonDispatchableHandle();

		bool IsNull() const;
		void MakeNull();

	private:
		uint64_t value;
	};

	class Semaphore           : public NonDispatchableHandle {};
	class Fence               : public NonDispatchableHandle {};
	class DeviceMemory        : public NonDispatchableHandle {};
	class Buffer              : public NonDispatchableHandle {};
	class Image               : public NonDispatchableHandle {};
	class Event               : public NonDispatchableHandle {};
	class QueryPool           : public NonDispatchableHandle {};
	class BufferView          : public NonDispatchableHandle {};
	class ImageView           : public NonDispatchableHandle {};
	class ShaderModule        : public NonDispatchableHandle {};
	class PipelineCache       : public NonDispatchableHandle {};
	class PipelineLayout      : public NonDispatchableHandle {};
	class RenderPass          : public NonDispatchableHandle {};
	class Pipeline            : public NonDispatchableHandle {};
	class DescriptorSetLayout : public NonDispatchableHandle {};
	class Sampler             : public NonDispatchableHandle {};
	class DescriptorPool      : public NonDispatchableHandle {};
	class DescriptorSet       : public NonDispatchableHandle {};
	class Framebuffer         : public NonDispatchableHandle {};
	class CommandPool         : public NonDispatchableHandle {};
	class Surface             : public NonDispatchableHandle {};
	class Swapchain           : public NonDispatchableHandle {};
}
