#include "Enums.hpp"
#include "VulkanError.hpp"

using namespace vulkan;

std::string vulkan::GetName(Result result)
{
	switch (result)
	{
	case Result::SUCCESS:					return "SUCCESS";
	case Result::NOT_READY:					return "NOT_READY";
	case Result::TIMEOUT:					return "TIMEOUT";
	case Result::EVENT_SET:					return "EVENT_SET";
	case Result::EVENT_RESET:				return "EVENT_RESET";
	case Result::INCOMPLETE:				return "INCOMPLETE";
	case Result::OUT_OF_HOST_MEMORY:		return "OUT_OF_HOST_MEMORY";
	case Result::OUT_OF_DEVICE_MEMORY:		return "OUT_OF_DEVICE_MEMORY";
	case Result::INITIALIZATION_FAILED:		return "INITIALIZATION_FAILED";
	case Result::DEVICE_LOST:				return "DEVICE_LOST";
	case Result::MEMORY_MAP_FAILED:			return "MEMORY_MAP_FAILED";
	case Result::LAYER_NOT_PRESENT:			return "LAYER_NOT_PRESENT";
	case Result::EXTENSION_NOT_PRESENT:		return "EXTENSION_NOT_PRESENT";
	case Result::FEATURE_NOT_PRESENT:		return "FEATURE_NOT_PRESENT";
	case Result::INCOMPATIBLE_DRIVER:		return "INCOMPATIBLE_DRIVER";
	case Result::TOO_MANY_OBJECTS:			return "TOO_MANY_OBJECTS";
	case Result::FORMAT_NOT_SUPPORTED:		return "FORMAT_NOT_SUPPORTED";
	case Result::SURFACE_LOST:				return "SURFACE_LOST";
	case Result::NATIVE_WINDOW_IN_USE:		return "NATIVE_WINDOW_IN_USE";
	case Result::SUBOPTIMAL:				return "SUBOPTIMAL";
	case Result::OUT_OF_DATE:				return "OUT_OF_DATE";
	case Result::INCOMPATIBLE_DISPLAY:		return "INCOMPATIBLE_DISPLAY";
	case Result::VALIDATION_FAILED:			return "VALIDATION_FAILED";
	case Result::INVALID_SHADER:			return "INVALID_SHADER";
	default:
		if (static_cast<int>(result) < 0)
			return "UNKNOWN_ERROR";
		else
			return "UNKNOWN_RESULT";
	}
}

std::string vulkan::GetDetails(Result result)
{
	switch (result)
	{
	case Result::SUCCESS:					return "Command successfully completed";
	case Result::NOT_READY:					return "Fence or query has not yet completed";
	case Result::TIMEOUT:					return "A wait operation has not completed in the specified time";
	case Result::EVENT_SET:					return "An event is signaled";
	case Result::EVENT_RESET:				return "An event is unsignaled";
	case Result::INCOMPLETE:				return "A return array was too small for the result";
	case Result::OUT_OF_HOST_MEMORY:		return "A host memory allocation has failed";
	case Result::OUT_OF_DEVICE_MEMORY:		return "A device memory allocation has failed";
	case Result::INITIALIZATION_FAILED:		return "Initialization of an object could not be completed for implementation-specific reasons";
	case Result::DEVICE_LOST:				return "The logical or physical device has been lost";
	case Result::MEMORY_MAP_FAILED:			return "Mapping of a memory object has failed";
	case Result::LAYER_NOT_PRESENT:			return "A requested layer is not present or could not be loaded";
	case Result::EXTENSION_NOT_PRESENT:		return "A requested extension is not supported";
	case Result::FEATURE_NOT_PRESENT:		return "A requested feature is not supported";
	case Result::INCOMPATIBLE_DRIVER:		return "The requested version of Vulkan is not supported by the driver, or is otherwise incompatible for implementation-specific reasons";
	case Result::TOO_MANY_OBJECTS:			return "Too many objects of the type have already been created";
	case Result::FORMAT_NOT_SUPPORTED:		return "A requested format is not supported on this device";
	case Result::SURFACE_LOST:				return "A surface is no longer available";
	case Result::NATIVE_WINDOW_IN_USE:		return "The requested window is already connected to a Surface, or to some other non-Vulkan API";
	case Result::SUBOPTIMAL:				return "A swapchain no longer matches the surface properties exactly, but can still be used to present to the surface successfully";
	case Result::OUT_OF_DATE:				return "A surface has changed in such a way that it is no longer compatible with the swapchain, and further presentation requests using the swapchain will fail";
	case Result::INCOMPATIBLE_DISPLAY:		return "The display used by a swapchain does not use the same presentable image layout, or is incompatible in a way that prevents sharing an image";
	case Result::VALIDATION_FAILED:			return "Validation of the input parameters failed";
	case Result::INVALID_SHADER:			return "The shader provided is invalid";
	default:
		if (static_cast<int>(result) < 0)
			return "An unknown error code has been returned";
		else
			return "An unknown status code has been returned";
	}
}

void vulkan::ThrowIfError(Result result, const char* message)
{
	if (static_cast<int>(result) < 0)
		throw VulkanError(result, message);
}

void vulkan::ThrowIfError(Result result, std::string message)
{
	if (static_cast<int>(result) < 0)
		throw VulkanError(result, message);
}
