#pragma once

#include "Flags.hpp"

namespace vulkan
{
	class PushConstantRange
	{
	public:
		PushConstantRange(ShaderStageFlags stageFlags, uint32_t offset, uint32_t size);

	private:
		ShaderStageFlags    stageFlags;
		uint32_t            offset;
		uint32_t            size;
	};
}