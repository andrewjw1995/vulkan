#pragma once

#include <cstdint>

namespace vulkan
{
	class Offset3D
	{
	public:
		Offset3D();
		Offset3D(uint32_t x, uint32_t y, uint32_t z);

		int32_t x;
		int32_t y;
		int32_t z;
	};
}