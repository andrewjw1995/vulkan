#include "PipelineVertexInputStateCreateInfo.hpp"

using namespace vulkan;

PipelineVertexInputStateCreateInfo::PipelineVertexInputStateCreateInfo() :
	Structure(StructureType::PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO),
	flags(),
	vertexBindingDescriptionCount(0),
	pVertexBindingDescriptions(nullptr),
	vertexAttributeDescriptionCount(0),
	pVertexAttributeDescriptions(nullptr)
{ }

PipelineVertexInputStateCreateInfo& PipelineVertexInputStateCreateInfo::Flags(PipelineVertexInputStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineVertexInputStateCreateInfo& PipelineVertexInputStateCreateInfo::Bindings(uint32_t count, const VertexInputBindingDescription* pDescriptions)
{
	this->vertexBindingDescriptionCount = count;
	this->pVertexBindingDescriptions = pDescriptions;
	return *this;
}

PipelineVertexInputStateCreateInfo& PipelineVertexInputStateCreateInfo::Attributes(uint32_t count, const VertexInputAttributeDescription* pDescriptions)
{
	this->vertexAttributeDescriptionCount = count;
	this->pVertexAttributeDescriptions = pDescriptions;
	return *this;
}
