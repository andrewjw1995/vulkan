#pragma once

#include "Enums.hpp"

#include <stdexcept>

namespace vulkan
{
	class VulkanError : public std::runtime_error
	{
	public:
		VulkanError(Result result, const char* message);
		VulkanError(Result result, const std::string& message);

		Result GetResult() const;

		Result result;
	};
}
