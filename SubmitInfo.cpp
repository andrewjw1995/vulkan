#include "SubmitInfo.hpp"

using namespace vulkan;

SubmitInfo::SubmitInfo() :
	Structure(StructureType::SUBMIT_INFO),
	waitSemaphoreCount(0),
	pWaitSemaphores(nullptr),
	pWaitDstStageMask(0),
	commandBufferCount(0),
	pCommandBuffers(nullptr),
	signalSemaphoreCount(0),
	pSignalSemaphores(nullptr)
{ }

SubmitInfo& SubmitInfo::WaitSemaphores(uint32_t count, const Semaphore* pSemaphores, const PipelineStageFlags* pDestinations)
{
	waitSemaphoreCount = count;
	pWaitSemaphores = pSemaphores;
	pWaitDstStageMask = pDestinations;
	return *this;
}

SubmitInfo& SubmitInfo::CommandBuffers(uint32_t count, const CommandBuffer* pBuffers)
{
	commandBufferCount = count;
	pCommandBuffers = pBuffers;
	return *this;
}

SubmitInfo& SubmitInfo::SignalSemaphores(uint32_t count, const Semaphore* pSemaphores)
{
	signalSemaphoreCount = count;
	pSignalSemaphores = pSemaphores;
	return *this;
}
