#include "Functions.hpp"
#include "Queue.hpp"
#include "VulkanError.hpp"

using namespace vulkan;

void Queue::Submit(uint32_t count, const SubmitInfo* pInfos, Fence fence) const
{
	Result result = vkQueueSubmit(*this, count, pInfos, fence);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to submit to queue");
}

void Queue::BindSparse(uint32_t count, const BindSparseInfo* pBinds, Fence fence) const
{
	Result result = vkQueueBindSparse(*this, count, pBinds, fence);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to bind sparse memory");
}

void Queue::WaitIdle() const
{
	Result result = vkQueueWaitIdle(*this);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to wait for queue to become idle");
}

void Queue::Present(const PresentInfo& info) const
{
	Result result = vkQueuePresentKHR(*this, &info);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to present");
}
