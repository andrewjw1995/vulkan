#include "SparseImageMemoryRequirements.hpp"

using namespace vulkan;

SparseImageFormatProperties SparseImageMemoryRequirements::FormatProperties() const
{
	return formatProperties;
}

uint32_t SparseImageMemoryRequirements::MipTailFirstLod() const
{
	return imageMipTailFirstLod;
}

DeviceSize SparseImageMemoryRequirements::MipTailSize() const
{
	return imageMipTailSize;
}

DeviceSize SparseImageMemoryRequirements::MipTailOffset() const
{
	return imageMipTailOffset;
}

DeviceSize SparseImageMemoryRequirements::MipTailStride() const
{
	return imageMipTailStride;
}
