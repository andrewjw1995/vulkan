#include "PipelineShaderStageCreateInfo.hpp"

using namespace vulkan;

PipelineShaderStageCreateInfo::PipelineShaderStageCreateInfo() :
	Structure(StructureType::PIPELINE_SHADER_STAGE_CREATE_INFO),
	flags(),
	stage(),
	module(),
	pName(nullptr),
	pSpecializationInfo(nullptr)
{ }

PipelineShaderStageCreateInfo::PipelineShaderStageCreateInfo(ShaderStageFlagBits stage, ShaderModule module, const char* pName) :
	Structure(StructureType::PIPELINE_SHADER_STAGE_CREATE_INFO),
	flags(),
	stage(stage),
	module(module),
	pName(pName),
	pSpecializationInfo(nullptr)
{ }

PipelineShaderStageCreateInfo& PipelineShaderStageCreateInfo::Flags(PipelineShaderStageCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineShaderStageCreateInfo& PipelineShaderStageCreateInfo::Stage(ShaderStageFlagBits stage)
{
	this->stage = stage;
	return *this;
}

PipelineShaderStageCreateInfo& PipelineShaderStageCreateInfo::Module(ShaderModule module)
{
	this->module = module;
	return *this;
}

PipelineShaderStageCreateInfo& PipelineShaderStageCreateInfo::Name(const char* pName)
{
	this->pName = pName;
	return *this;
}

PipelineShaderStageCreateInfo& PipelineShaderStageCreateInfo::SpecializationInfo(const vulkan::SpecializationInfo* pInfo)
{
	this->pSpecializationInfo = pInfo;
	return *this;
}
