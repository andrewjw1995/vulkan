#include "SparseImageOpaqueMemoryBindInfo.hpp"

using namespace vulkan;

SparseImageOpaqueMemoryBindInfo::SparseImageOpaqueMemoryBindInfo() :
	image(vulkan::Image()),
	bindCount(0),
	pBinds(nullptr)
{ }

SparseImageOpaqueMemoryBindInfo& SparseImageOpaqueMemoryBindInfo::Image(vulkan::Image image)
{
	this->image = image;
	return *this;
}

SparseImageOpaqueMemoryBindInfo& SparseImageOpaqueMemoryBindInfo::Binds(uint32_t count, const SparseMemoryBind* pBinds)
{
	this->bindCount = count;
	this->pBinds = pBinds;
	return *this;
}
