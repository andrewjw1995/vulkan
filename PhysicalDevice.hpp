#pragma once

#include "Device.hpp"
#include "DeviceCreateInfo.hpp"
#include "DispatchableHandle.hpp"
#include "Extension.hpp"
#include "FormatProperties.hpp"
#include "ImageFormatProperties.hpp"
#include "Layer.hpp"
#include "PhysicalDeviceFeatures.hpp"
#include "PhysicalDeviceMemoryProperties.hpp"
#include "PhysicalDeviceProperties.hpp"
#include "QueueFamily.hpp"
#include "SparseImageFormatProperties.hpp"
#include "SurfaceCapabilities.hpp"
#include "SurfaceFormat.hpp"

#include <vector>

namespace vulkan
{
	class PhysicalDevice : public DispatchableHandle
	{
	public:
		std::vector<Layer> EnumerateLayers() const;
		std::vector<Extension> EnumerateExtensions(const char* pLayer) const;
		void GetFeatures(PhysicalDeviceFeatures& features) const;
		PhysicalDeviceFeatures GetFeatures() const;
		FormatProperties GetFormatProperties(Format format);

		ImageFormatProperties GetImageFormatProperties(
			Format format,
			ImageType type,
			ImageTiling tiling,
			ImageUsageFlags usage,
			ImageCreateFlags flags
		) const;

		void GetProperties(PhysicalDeviceProperties& properties) const;
		PhysicalDeviceProperties GetProperties() const;
		std::vector<QueueFamily> GetQueueFamilies() const;
		void GetMemoryProperties(PhysicalDeviceMemoryProperties& memoryProperties) const;
		PhysicalDeviceMemoryProperties GetMemoryProperties() const;

		std::vector<SparseImageFormatProperties> GetSparseImageFormatProperties(
			Format                                      format,
			ImageType                                   type,
			SampleCountFlagBits                         samples,
			ImageUsageFlags                             usage,
			ImageTiling                                 tiling
		) const;

		Device CreateDevice(const DeviceCreateInfo& info) const;
		bool GetSurfaceSupport(uint32_t queueFamilyIndex, Surface surface) const;
		SurfaceCapabilities GetSurfaceCapabilities(Surface surface) const;
		std::vector<SurfaceFormat> GetSurfaceFormats(Surface surface) const;
		std::vector<PresentMode> GetSurfacePresentModes(Surface surface) const;
		bool GetWin32PresentationSupport(uint32_t queueFamilyIndex) const;
	};
}
