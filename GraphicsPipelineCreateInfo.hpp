#pragma once

#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "PipelineColorBlendStateCreateInfo.hpp"
#include "PipelineDepthStencilStateCreateInfo.hpp"
#include "PipelineDynamicStateCreateInfo.hpp"
#include "PipelineInputAssemblyStateCreateInfo.hpp"
#include "PipelineMultisampleStateCreateInfo.hpp"
#include "PipelineRasterizationStateCreateInfo.hpp"
#include "PipelineShaderStageCreateInfo.hpp"
#include "PipelineTessellationStateCreateInfo.hpp"
#include "PipelineVertexInputStateCreateInfo.hpp"
#include "PipelineViewportStateCreateInfo.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class GraphicsPipelineCreateInfo : public Structure
	{
	public:
		GraphicsPipelineCreateInfo();

		GraphicsPipelineCreateInfo& Flags(PipelineCreateFlags flags);
		GraphicsPipelineCreateInfo& Stages(uint32_t count, const PipelineShaderStageCreateInfo* pStages);
		GraphicsPipelineCreateInfo& VertexInputState(const PipelineVertexInputStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& InputAssemblyState(const PipelineInputAssemblyStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& TessellationState(const PipelineTessellationStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& ViewportState(const PipelineViewportStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& RasterizationState(const PipelineRasterizationStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& MultisampleState(const PipelineMultisampleStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& DepthStencilState(const PipelineDepthStencilStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& ColorBlendState(const PipelineColorBlendStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& DynamicState(const PipelineDynamicStateCreateInfo* pInfo);
		GraphicsPipelineCreateInfo& Layout(PipelineLayout layout);
		GraphicsPipelineCreateInfo& RenderPass(RenderPass handle);
		GraphicsPipelineCreateInfo& Subpass(uint32_t index);
		GraphicsPipelineCreateInfo& BasePipeline(Pipeline handle);
		GraphicsPipelineCreateInfo& BasePipeline(int32_t index);

	private:
		PipelineCreateFlags                            flags;
		uint32_t                                       stageCount;
		const PipelineShaderStageCreateInfo*           pStages;
		const PipelineVertexInputStateCreateInfo*      pVertexInputState;
		const PipelineInputAssemblyStateCreateInfo*    pInputAssemblyState;
		const PipelineTessellationStateCreateInfo*     pTessellationState;
		const PipelineViewportStateCreateInfo*         pViewportState;
		const PipelineRasterizationStateCreateInfo*    pRasterizationState;
		const PipelineMultisampleStateCreateInfo*      pMultisampleState;
		const PipelineDepthStencilStateCreateInfo*     pDepthStencilState;
		const PipelineColorBlendStateCreateInfo*       pColorBlendState;
		const PipelineDynamicStateCreateInfo*          pDynamicState;
		PipelineLayout                                 layout;
		vulkan::RenderPass                             renderPass;
		uint32_t                                       subpass;
		Pipeline                                       basePipelineHandle;
		int32_t                                        basePipelineIndex;
	};
}