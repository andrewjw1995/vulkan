#pragma once

#include "Enums.hpp"

namespace vulkan
{
	class DescriptorPoolSize
	{
	public:
		DescriptorPoolSize(DescriptorType type, uint32_t descriptorCount);

	private:
		DescriptorType type;
		uint32_t       descriptorCount;
	};
}