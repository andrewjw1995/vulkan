#pragma once

#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "PushConstantRange.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineLayoutCreateInfo : public Structure
	{
	public:
		PipelineLayoutCreateInfo();

		PipelineLayoutCreateInfo& Flags(PipelineLayoutCreateFlags flags);
		PipelineLayoutCreateInfo& SetLayouts(uint32_t count, const DescriptorSetLayout* pSetLayouts);
		PipelineLayoutCreateInfo& PushConstantRanges(uint32_t count, const PushConstantRange* pRanges);

	private:
		PipelineLayoutCreateFlags     flags;
		uint32_t                      setLayoutCount;
		const DescriptorSetLayout*    pSetLayouts;
		uint32_t                      pushConstantRangeCount;
		const PushConstantRange*      pPushConstantRanges;
	};
}