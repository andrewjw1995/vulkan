#include "Extension.hpp"

using namespace std;
using namespace vulkan;

Extension::Extension() :
	specVersion(0)
{
	for (size_t i = 0; i < MAX_NAME_SIZE; i++)
		extensionName[i] = '\0';
}

const char* Extension::Name() const
{
	return extensionName;
}

uint32_t Extension::SpecificationVersion() const
{
	return specVersion;
}
