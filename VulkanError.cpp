#include "VulkanError.hpp"

using namespace std;
using namespace vulkan;

VulkanError::VulkanError(Result result, const char* message) :
	runtime_error(message),
	result(result)
{ }

VulkanError::VulkanError(Result result, const string& message) :
	runtime_error(message),
	result(result)
{ }

Result VulkanError::GetResult() const
{
	return result;
}
