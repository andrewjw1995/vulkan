#pragma once

#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class CommandBufferAllocateInfo : public Structure
	{
	public:
		CommandBufferAllocateInfo(CommandPool commandPool, CommandBufferLevel level, uint32_t commandBufferCount);

		uint32_t CommandBufferCount() const;

	private:
		CommandPool        commandPool;
		CommandBufferLevel level;
		uint32_t           commandBufferCount;
	};
}