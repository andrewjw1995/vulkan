#include "Structure.hpp"

using namespace vulkan;

Structure::Structure(StructureType type) :
	sType(type),
	pNext(nullptr)
{ }

Structure::Structure(StructureType type, Structure* pNext) :
	sType(type),
	pNext(pNext)
{ }

void Structure::SetNext(Structure* pNext)
{
	this->pNext = pNext;
}

StructureType Structure::Type() const
{
	return sType;
}

Structure* Structure::Next() const
{
	return pNext;
}