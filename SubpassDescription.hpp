#pragma once

#include "AttachmentReference.hpp"
#include "Enums.hpp"
#include "Flags.hpp"

namespace vulkan
{
	class SubpassDescription
	{
	public:
		SubpassDescription();

		SubpassDescription& Flags(SubpassDescriptionFlags flags);
		SubpassDescription& BindPoint(PipelineBindPoint bindPoint);
		SubpassDescription& InputAttachments(uint32_t count, const AttachmentReference* pAttachments);
		SubpassDescription& ColorAttachments(uint32_t count, const AttachmentReference* pAttachments);
		SubpassDescription& ColorAttachments(uint32_t count, const AttachmentReference* pAttachments, const AttachmentReference* pResolveAttachments);
		SubpassDescription& DepthStencilAttachment(const AttachmentReference* pAttachment);
		SubpassDescription& PreserveAttachments(uint32_t count, const uint32_t* pAttachments);

	private:
		SubpassDescriptionFlags    flags;
		PipelineBindPoint          pipelineBindPoint;
		uint32_t                   inputAttachmentCount;
		const AttachmentReference* pInputAttachments;
		uint32_t                   colorAttachmentCount;
		const AttachmentReference* pColorAttachments;
		const AttachmentReference* pResolveAttachments;
		const AttachmentReference* pDepthStencilAttachment;
		uint32_t                   preserveAttachmentCount;
		const uint32_t*            pPreserveAttachments;
	};
}