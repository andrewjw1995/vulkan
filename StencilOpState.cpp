#include "StencilOpState.hpp"

using namespace vulkan;

StencilOpState::StencilOpState() :
	failOp(StencilOp::KEEP),
	passOp(StencilOp::KEEP),
	depthFailOp(StencilOp::KEEP),
	compareOp(CompareOp::NEVER), // TODO: NEVER or ALWAYS?
	compareMask(0),
	writeMask(0),
	reference(0)
{ }

StencilOpState& StencilOpState::FailOp(StencilOp op)
{
	this->failOp = op;
	return *this;
}

StencilOpState& StencilOpState::PassOp(StencilOp op)
{
	this->passOp = op;
	return *this;
}

StencilOpState& StencilOpState::DepthFailOp(StencilOp op)
{
	this->depthFailOp = op;
	return *this;
}

StencilOpState& StencilOpState::CompareOp(vulkan::CompareOp op)
{
	this->compareOp = op;
	return *this;
}

StencilOpState& StencilOpState::CompareMask(uint32_t mask)
{
	this->compareMask = mask;
	return *this;
}

StencilOpState& StencilOpState::WriteMask(uint32_t mask)
{
	this->writeMask = mask;
	return *this;
}

StencilOpState& StencilOpState::Reference(uint32_t reference)
{
	this->reference = reference;
	return *this;
}
