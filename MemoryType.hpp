#pragma once

#include "Flags.hpp"

namespace vulkan
{
	class MemoryType
	{
	public:
		MemoryPropertyFlags Flags() const;
		uint32_t            HeapIndex() const;

	private:
		MemoryPropertyFlags propertyFlags;
		uint32_t            heapIndex;
	};
}
