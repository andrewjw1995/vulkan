#include "ShaderModuleCreateInfo.hpp"

using namespace vulkan;

ShaderModuleCreateInfo::ShaderModuleCreateInfo() :
	Structure(StructureType::SHADER_MODULE_CREATE_INFO),
	flags(),
	codeSize(0),
	pCode(nullptr)
{ }

ShaderModuleCreateInfo::ShaderModuleCreateInfo(size_t codeSize, const uint32_t* pCode) :
	Structure(StructureType::SHADER_MODULE_CREATE_INFO),
	flags(),
	codeSize(codeSize),
	pCode(pCode)
{ }

ShaderModuleCreateInfo& ShaderModuleCreateInfo::Flags(ShaderModuleCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

ShaderModuleCreateInfo& ShaderModuleCreateInfo::Code(size_t size, const uint32_t* pCode)
{
	this->codeSize = size;
	this->pCode = pCode;
	return *this;
}
