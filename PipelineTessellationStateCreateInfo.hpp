#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineTessellationStateCreateInfo : public Structure
	{
	public:
		PipelineTessellationStateCreateInfo();
		PipelineTessellationStateCreateInfo(uint32_t patchControlPoints);

		PipelineTessellationStateCreateInfo& Flags(PipelineTessellationStateCreateFlags flags);
		PipelineTessellationStateCreateInfo& PatchControlPoints(uint32_t count);

	private:
		PipelineTessellationStateCreateFlags flags;
		uint32_t                             patchControlPoints;
	};
}