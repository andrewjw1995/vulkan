#pragma once

#include <cstdint>

namespace vulkan
{
	class Extent3D
	{
	public:
		Extent3D();
		Extent3D(uint32_t width, uint32_t height, uint32_t depth);

		uint32_t width;
		uint32_t height;
		uint32_t depth;
	};
}