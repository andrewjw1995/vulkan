#pragma once

#include "Bool32.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class SamplerCreateInfo : public Structure
	{
	public:
		SamplerCreateInfo();

		SamplerCreateInfo& Flags(SamplerCreateFlags flags);
		SamplerCreateInfo& Filters(Filter min, Filter mag);
		SamplerCreateInfo& MipmapMode(SamplerMipmapMode mode);
		SamplerCreateInfo& AddressModes(SamplerAddressMode u, SamplerAddressMode v, SamplerAddressMode w);
		SamplerCreateInfo& MipLodBias(float bias);
		SamplerCreateInfo& EnableAnisotropy(float max);
		SamplerCreateInfo& DisableAnisotropy();
		SamplerCreateInfo& EnableCompare(CompareOp op);
		SamplerCreateInfo& DisableCompare();
		SamplerCreateInfo& Lod(float min, float max);
		SamplerCreateInfo& BorderColor(BorderColor color);
		SamplerCreateInfo& UnnormalizedCoordinates();
		SamplerCreateInfo& NormalizedCoordinates();

	private:
		SamplerCreateFlags    flags;
		Filter                magFilter;
		Filter                minFilter;
		SamplerMipmapMode     mipmapMode;
		SamplerAddressMode    addressModeU;
		SamplerAddressMode    addressModeV;
		SamplerAddressMode    addressModeW;
		float                 mipLodBias;
		Bool32                anisotropyEnable;
		float                 maxAnisotropy;
		Bool32                compareEnable;
		CompareOp             compareOp;
		float                 minLod;
		float                 maxLod;
		vulkan::BorderColor   borderColor;
		Bool32                unnormalizedCoordinates;
	};
}