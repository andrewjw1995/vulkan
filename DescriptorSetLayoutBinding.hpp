#pragma once

#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"

namespace vulkan
{
	class DescriptorSetLayoutBinding
	{
	public:
		DescriptorSetLayoutBinding();

		DescriptorSetLayoutBinding& Binding(uint32_t index);
		DescriptorSetLayoutBinding& Descriptors(DescriptorType type, uint32_t count);
		DescriptorSetLayoutBinding& Descriptors(DescriptorType type, uint32_t count, const Sampler* pImmutableSamplers);
		DescriptorSetLayoutBinding& StageFlags(ShaderStageFlags flags);

	private:
		uint32_t         binding;
		DescriptorType   descriptorType;
		uint32_t         descriptorCount;
		ShaderStageFlags stageFlags;
		const Sampler*   pImmutableSamplers;
	};
}