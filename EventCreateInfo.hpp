#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class EventCreateInfo : public Structure
	{
	public:
		EventCreateInfo();
		EventCreateInfo(EventCreateFlags flags);

	private:
		EventCreateFlags flags;
	};
}