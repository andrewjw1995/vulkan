#pragma once

#include "Window.hpp"

namespace vulkan
{
	class WindowListener
	{
	public:
		virtual ~WindowListener() { }
		virtual void Maximized(Window& sender) = 0;
		virtual void Minimized(Window& sender) = 0;
		virtual void Resized(Window& sender) = 0;
		virtual void Moved(Window& sender) = 0;
		virtual void FocusGained(Window& sender) = 0;
		virtual void FocusLost(Window& sender) = 0;
		virtual void CloseRequested(Window& sender) = 0;
		virtual void RedrawRequested(Window& sender) = 0;
	};
}