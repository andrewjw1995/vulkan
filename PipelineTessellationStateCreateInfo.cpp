#include "PipelineTessellationStateCreateInfo.hpp"

using namespace vulkan;

PipelineTessellationStateCreateInfo::PipelineTessellationStateCreateInfo() :
	Structure(StructureType::PIPELINE_TESSELLATION_STATE_CREATE_INFO),
	flags(),
	patchControlPoints(0)
{ }

PipelineTessellationStateCreateInfo::PipelineTessellationStateCreateInfo(uint32_t patchControlPoints) :
	Structure(StructureType::PIPELINE_TESSELLATION_STATE_CREATE_INFO),
	flags(),
	patchControlPoints(patchControlPoints)
{ }

PipelineTessellationStateCreateInfo& PipelineTessellationStateCreateInfo::Flags(PipelineTessellationStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineTessellationStateCreateInfo& PipelineTessellationStateCreateInfo::PatchControlPoints(uint32_t count)
{
	this->patchControlPoints = patchControlPoints;
	return *this;
}
