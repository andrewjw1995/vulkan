#include "MemoryHeap.hpp"

using namespace vulkan;

DeviceSize MemoryHeap::Size() const
{
	return size;
}

MemoryHeapFlags MemoryHeap::Flags() const
{
	return flags;
}
