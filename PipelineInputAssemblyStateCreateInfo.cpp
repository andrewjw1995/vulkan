#include "PipelineInputAssemblyStateCreateInfo.hpp"

using namespace vulkan;

PipelineInputAssemblyStateCreateInfo::PipelineInputAssemblyStateCreateInfo() :
	Structure(StructureType::PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO),
	flags(),
	topology(PrimitiveTopology::POINT_LIST),
	primitiveRestartEnable(false)
{ }

PipelineInputAssemblyStateCreateInfo::PipelineInputAssemblyStateCreateInfo(PrimitiveTopology topology, Bool32 primitiveRestartEnable) :
	Structure(StructureType::PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO),
	flags(),
	topology(topology),
	primitiveRestartEnable(primitiveRestartEnable)
{ }

PipelineInputAssemblyStateCreateInfo& PipelineInputAssemblyStateCreateInfo::Flags(PipelineInputAssemblyStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineInputAssemblyStateCreateInfo& PipelineInputAssemblyStateCreateInfo::Topology(PrimitiveTopology topology)
{
	this->topology = topology;
	return *this;
}

PipelineInputAssemblyStateCreateInfo& PipelineInputAssemblyStateCreateInfo::EnablePrimitiveRestart()
{
	this->primitiveRestartEnable = true;
	return *this;
}

PipelineInputAssemblyStateCreateInfo& PipelineInputAssemblyStateCreateInfo::DisablePrimitiveRestart()
{
	this->primitiveRestartEnable = false;
	return *this;
}
