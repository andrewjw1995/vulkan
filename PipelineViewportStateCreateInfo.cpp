#include "PipelineViewportStateCreateInfo.hpp"

using namespace vulkan;

PipelineViewportStateCreateInfo::PipelineViewportStateCreateInfo() :
	Structure(StructureType::PIPELINE_VIEWPORT_STATE_CREATE_INFO),
	flags(),
	viewportCount(0),
	pViewports(nullptr),
	scissorCount(0),
	pScissors(nullptr)
{ }

PipelineViewportStateCreateInfo& PipelineViewportStateCreateInfo::Flags(PipelineViewportStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineViewportStateCreateInfo& PipelineViewportStateCreateInfo::Viewports(uint32_t count, const Viewport* pViewports)
{
	this->viewportCount = count;
	this->pViewports = pViewports;
	return *this;
}

PipelineViewportStateCreateInfo& PipelineViewportStateCreateInfo::Scissors(uint32_t count, const Rectangle* pScissors)
{
	this->scissorCount = count;
	this->pScissors = pScissors;
	return *this;
}
