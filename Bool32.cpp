#include "Bool32.hpp"

using namespace std;
using namespace vulkan;

Bool32::Bool32() :
	value(0)
{}

Bool32::Bool32(bool value) :
	value(value ? 1 : 0)
{}

Bool32::Bool32(uint32_t value) :
	value(value)
{}

Bool32 Bool32::operator&(Bool32 other) const
{
	return value & other.value;
}

Bool32 Bool32::operator|(Bool32 other) const
{
	return value | other.value;
}

Bool32 Bool32::operator^(Bool32 other) const
{
	return value ^ other.value;
}

Bool32& Bool32::operator&=(Bool32 other)
{
	value &= other.value;
	return *this;
}

Bool32& Bool32::operator|=(Bool32 other)
{
	value |= other.value;
	return *this;
}

Bool32& Bool32::operator^=(Bool32 other)
{
	value ^= other.value;
	return *this;
}

Bool32::operator bool() const
{
	return value == 1;
}

ostream& vulkan::operator<<(ostream& out, const Bool32& value)
{
	out << (value ? "TRUE" : "FALSE");
	return out;
}
