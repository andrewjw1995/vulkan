#include "SurfaceFormat.hpp"

using namespace vulkan;

SurfaceFormat::SurfaceFormat() :
	format(Format::UNDEFINED),
	colorSpace(ColorSpace::SRGB_NONLINEAR)
{ }

SurfaceFormat::SurfaceFormat(vulkan::Format format, vulkan::ColorSpace colorSpace) :
	format(format),
	colorSpace(colorSpace)
{ }

vulkan::Format SurfaceFormat::Format() const
{
	return this->format;
}

vulkan::ColorSpace SurfaceFormat::ColorSpace() const
{
	return this->colorSpace;
}
