#pragma once

#include "DeviceSize.hpp"

namespace vulkan
{
	class BufferCopy
	{
	public:
		BufferCopy(DeviceSize srcOffset, DeviceSize dstOffset, DeviceSize size);

	private:
		DeviceSize srcOffset;
		DeviceSize dstOffset;
		DeviceSize size;
	};
}