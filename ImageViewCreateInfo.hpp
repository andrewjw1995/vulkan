#pragma once

#include "ComponentMapping.hpp"
#include "Flags.hpp"
#include "ImageSubresourceRange.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class ImageViewCreateInfo : public Structure
	{
	public:
		ImageViewCreateInfo();

		ImageViewCreateInfo& Flags(ImageViewCreateFlags flags);
		ImageViewCreateInfo& Image(Image image);
		ImageViewCreateInfo& ViewType(ImageViewType type);
		ImageViewCreateInfo& Format(Format format);
		ImageViewCreateInfo& Components(ComponentMapping mapping);
		ImageViewCreateInfo& SubresourceRange(ImageSubresourceRange range);

	private:
		ImageViewCreateFlags     flags;
		vulkan::Image            image;
		ImageViewType            viewType;
		vulkan::Format           format;
		ComponentMapping         components;
		ImageSubresourceRange    subresourceRange;
	};
}