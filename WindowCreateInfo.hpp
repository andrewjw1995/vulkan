#pragma once

#include "Extent2D.hpp"
#include "Offset2D.hpp"

#include <string>

namespace vulkan
{
	class WindowCreateInfo
	{
	public:
		WindowCreateInfo& Title(std::string title);
		WindowCreateInfo& Position(Offset2D position);
		WindowCreateInfo& Size(Extent2D size);
		WindowCreateInfo& Visible(bool visible);
		WindowCreateInfo& Maximizable(bool maximizable);
		WindowCreateInfo& Resizable(bool resizable);
		WindowCreateInfo& Positionable(bool positionable);

		std::string title;
		Offset2D position;
		Extent2D size;
		bool visible;
		bool maximizable;
		bool resizable;
		bool positionable;
	};
}