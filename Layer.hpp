#pragma once

#include "Version.hpp"

namespace vulkan
{
	class Layer
	{
	public:
		static const unsigned int MAX_NAME_SIZE = 256;
		static const unsigned int MAX_DESCRIPTION_SIZE = 256;

		Layer();

		const char* Name() const;
		Version SpecificationVersion() const;
		uint32_t ImplementationVersion() const;
		const char* Description() const;

	private:
		char layerName[MAX_NAME_SIZE];
		uint32_t specVersion;
		uint32_t implementationVersion;
		char description[MAX_DESCRIPTION_SIZE];
	};
}
