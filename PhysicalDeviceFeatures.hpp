#pragma once

#include "Bool32.hpp"

namespace vulkan
{
	class PhysicalDeviceFeatures
	{
	public:
		PhysicalDeviceFeatures();

		PhysicalDeviceFeatures& RobustBufferAccess(Bool32 enable);
		PhysicalDeviceFeatures& FullDrawIndexUInt32(Bool32 enable);
		PhysicalDeviceFeatures& ImageCubeArray(Bool32 enable);
		PhysicalDeviceFeatures& IndependentBlend(Bool32 enable);
		PhysicalDeviceFeatures& GeometryShader(Bool32 enable);
		PhysicalDeviceFeatures& TessellationShader(Bool32 enable);
		PhysicalDeviceFeatures& SampleRateShading(Bool32 enable);
		PhysicalDeviceFeatures& DualSourceBlend(Bool32 enable);
		PhysicalDeviceFeatures& LogicOp(Bool32 enable);
		PhysicalDeviceFeatures& MultiDrawIndirect(Bool32 enable);
		PhysicalDeviceFeatures& DrawIndirectFirstInstance(Bool32 enable);
		PhysicalDeviceFeatures& DepthClamp(Bool32 enable);
		PhysicalDeviceFeatures& DepthBiasClamp(Bool32 enable);
		PhysicalDeviceFeatures& FillModeNonSolid(Bool32 enable);
		PhysicalDeviceFeatures& DepthBounds(Bool32 enable);
		PhysicalDeviceFeatures& WideLines(Bool32 enable);
		PhysicalDeviceFeatures& LargePoints(Bool32 enable);
		PhysicalDeviceFeatures& AlphaToOne(Bool32 enable);
		PhysicalDeviceFeatures& MultiViewport(Bool32 enable);
		PhysicalDeviceFeatures& SamplerAnisotropy(Bool32 enable);
		PhysicalDeviceFeatures& TextureCompressionETC2(Bool32 enable);
		PhysicalDeviceFeatures& TextureCompressionASTC_LDR(Bool32 enable);
		PhysicalDeviceFeatures& TextureCompressionBC(Bool32 enable);
		PhysicalDeviceFeatures& OcclusionQueryPrecise(Bool32 enable);
		PhysicalDeviceFeatures& PipelineStatisticsQuery(Bool32 enable);
		PhysicalDeviceFeatures& VertexPipelineStoresAndAtomics(Bool32 enable);
		PhysicalDeviceFeatures& FragmentStoresAndAtomics(Bool32 enable);
		PhysicalDeviceFeatures& ShaderTessellationAndGeometryPointSize(Bool32 enable);
		PhysicalDeviceFeatures& ShaderImageGatherExtended(Bool32 enable);
		PhysicalDeviceFeatures& ShaderStorageImageExtendedFormats(Bool32 enable);
		PhysicalDeviceFeatures& ShaderStorageImageMultisample(Bool32 enable);
		PhysicalDeviceFeatures& ShaderStorageImageReadWithoutFormat(Bool32 enable);
		PhysicalDeviceFeatures& ShaderStorageImageWriteWithoutFormat(Bool32 enable);
		PhysicalDeviceFeatures& ShaderUniformBufferArrayDynamicIndexing(Bool32 enable);
		PhysicalDeviceFeatures& ShaderSampledImageArrayDynamicIndexing(Bool32 enable);
		PhysicalDeviceFeatures& ShaderStorageBufferArrayDynamicIndexing(Bool32 enable);
		PhysicalDeviceFeatures& ShaderStorageImageArrayDynamicIndexing(Bool32 enable);
		PhysicalDeviceFeatures& ShaderClipDistance(Bool32 enable);
		PhysicalDeviceFeatures& ShaderCullDistance(Bool32 enable);
		PhysicalDeviceFeatures& ShaderFloat64(Bool32 enable);
		PhysicalDeviceFeatures& ShaderInt64(Bool32 enable);
		PhysicalDeviceFeatures& ShaderInt16(Bool32 enable);
		PhysicalDeviceFeatures& ShaderResourceResidency(Bool32 enable);
		PhysicalDeviceFeatures& ShaderResourceMinLod(Bool32 enable);
		PhysicalDeviceFeatures& SparseBinding(Bool32 enable);
		PhysicalDeviceFeatures& SparseResidencyBuffer(Bool32 enable);
		PhysicalDeviceFeatures& SparseResidencyImage2D(Bool32 enable);
		PhysicalDeviceFeatures& SparseResidencyImage3D(Bool32 enable);
		PhysicalDeviceFeatures& SparseResidency2Samples(Bool32 enable);
		PhysicalDeviceFeatures& SparseResidency4Samples(Bool32 enable);
		PhysicalDeviceFeatures& SparseResidency8Samples(Bool32 enable);
		PhysicalDeviceFeatures& SparseResidency16Samples(Bool32 enable);
		PhysicalDeviceFeatures& SparseResidencyAliased(Bool32 enable);
		PhysicalDeviceFeatures& VariableMultisampleRate(Bool32 enable);
		PhysicalDeviceFeatures& InheritedQueries(Bool32 enable);

		/// <summary>
		/// RobustBufferAccess indicates that out of bounds accesses to buffers via shader
		/// operations are well-defined.
		/// If not enabled, out of bounds accesses may cause undefined behaviour up-to and
		/// including process termination.
		/// </summary>
		Bool32 RobustBufferAccess() const;
		/// <summary>
		/// FullDrawIndexUInt32 indicates the full 32-bit range of indices is supported for indexed
		/// draw calls when using an IndexType of UINT32. MaxDrawIndexedIndexValue is the maximum
		/// index value that may be used (aside from the primitive restart index, which is always
		/// 2^32 - 1 when the IndexType is UINT32). If this feature is supported,
		/// MaxDrawIndexedIndexValue must be 2^32 - 1; otherwise it must be no smaller than
		/// 2^24 - 1.
		/// </summary>
		Bool32 FullDrawIndexUInt32() const;
		/// <summary>
		/// ImageCubeArray indicates whether image views with an ImageViewType of CUBE_ARRAY can be
		/// created, and that the corresponding SampledCubeArray and ImageCubeArray SPIR-V
		/// capabilities can be used in shader code.
		/// </summary>
		Bool32 ImageCubeArray() const;
		// TODO: Verify the consistency of name 'PipelineColorBlendAttachmentState'
		/// <summary>
		/// IndependentBlend indicates whether the PipelineColorBlendAttachmentState settings are
		/// controlled independently per attachment. If this feature is not enabled, the
		/// PipelineColorBlendAttachmentState settings for all color attachments must be identical.
		/// Otherwise, a different VkPipelineColorBlendAttachmentState can be provided for each
		/// bound color attachment.
		/// </summary>
		Bool32 IndependentBlend() const;
		/// <summary>
		/// GeometryShader indicates whether geometry shaders are supported. If this feature is not
		/// enabled, the ShaderStage GEOMETRY and PipelineStage GEOMETRY_SHADER enum values must
		/// not be used. This also indicates whether shader modules can declare the Geometry		/// capability.
		/// </summary>
		Bool32 GeometryShader() const;
		/// <summary>
		/// TessellationShader indicates whether tessellation control and evaluation shaders are
		/// supported. If this feature is not enabled, the ShaderStage TESSELLATION_CONTROL,
		/// TESSELLATION_EVALUATION, the PipelineStage TESSELLATION_CONTROL_SHADER,
		/// TESSELLATION_EVALUATION_SHADER, and class PipelineTesselationStateCreateInfo must not
		/// be used. This also indicates whether shader modules can declare the Tessellation
		/// capability
		/// </summary>
		Bool32 TessellationShader() const;
		/// <summary>
		/// SampleRateShading indicates whether per-sample shading and multisample interpolation
		/// are supported. If this feature is not enabled, the EnableSampleShading member of the
		/// PipelineMultisampleStateCreateInfo structure must be set to false and the
		/// MinSampleShading member is ignored. This also indicates whether shader modules can
		/// declare the SampleRateShading capability.
		/// </summary>
		Bool32 SampleRateShading() const;
		/// <summary>
		/// DualSourceBlend indicates whether blend operations which take two sources are
		/// supported. If this feature is not enabled, the BlendFactor SOURCE1_COLOR,
		/// ONE_MINUS_SOURCE1_COLOR, SOURCE1_ALPHA, and ONE_MINUS_SOURCE1_ALPHA enum values must
		/// not be used as source or destination blending factors.
		/// </summary>
		Bool32 DualSourceBlend() const;
		/// <summary>
		/// LogicOp indicates whether logic operations are supported. If this feature is not
		/// enabled, the logicOpEnable member of the PipelineColorBlendStateCreateInfo structure
		/// must be set to false, and the LogicOp member is ignored.
		/// </summary>
		Bool32 LogicOp() const;
		/// <summary>
		/// MultiDrawIndirect indicates whether multi-draw indirect is supported. If this feature
		/// is not enabled, the DrawCount parameter to the vkCmdDrawIndirect and
		/// vkCmdDrawIndexedIndirect commands must be 1. The MaxDrawIndirectCount member of the
		/// PhysicalDeviceLimits structure must also be 1 if this feature is not supported.
		/// </summary>
		Bool32 MultiDrawIndirect() const;
		/// <summary>
		/// DrawIndirectFirstInstance indicates whether indirect draw calls support the
		/// FirstInstance parameter. If this feature is not enabled, the FirstInstance member of
		/// all DrawIndirectCommand and DrawIndexedIndirectCommand structures that are provided to
		/// the vkCmdDrawIndirect and vkCmdDrawIndexedIndirect commands must be 0.
		/// </summary>
		Bool32 DrawIndirectFirstInstance() const;
		/// <summary>
		/// DepthClamp indicates whether depth clamping is supported. If this feature is not
		/// enabled, the EnableDepthClamp member of the PipelineRasterizationStateCreateInfo
		/// structure must be set to false. Otherwise, setting EnableDepthClamp to true will enable
		/// depth clamping.
		/// </summary>
		Bool32 DepthClamp() const;
		/// <summary>
		/// DepthBiasClamp indicates whether depth bias clamping is supported. If this feature is
		/// not enabled, the DepthBiasClamp member of the PipelineRasterizationStateCreateInfo
		/// structure must be set to 0.0.
		/// </summary>
		Bool32 DepthBiasClamp() const;
		/// <summary>
		/// FillModeNonSolid indicates whether point and wireframe fill modes are supported. If
		/// this feature is not enabled, the PolygonMode POINT and LINE enum values must not be
		/// used.
		/// </summary>
		Bool32 FillModeNonSolid() const;
		/// <summary>
		/// DepthBounds indicates whether depth bounds tests are supported. If this feature is not
		/// enabled, the EnableDepthBoundsTest member of the PipelineDepthStencilStateCreateInfo
		/// structure must be set to false. When EnableDepthBoundsTest is set to false, the values
		/// of the MinDepthBounds and MaxDepthBounds members of the		/// PipelineDepthStencilStateCreateInfo structure are ignored.
		/// </summary>
		Bool32 DepthBounds() const;
		/// <summary>
		/// WideLines indicates whether lines with width other than 1.0 are supported. If this
		/// feature is not enabled, the LineWidth member of the
		/// PipelineRasterizationStateCreateInfo structure must be set to 1.0. When this feature is
		/// supported, the range and granularity of supported line widths are indicated by the
		/// LineWidthRange and LineWidthGranularity members of the PhysicalDeviceLimits structure,
		/// respectively.
		/// </summary>
		Bool32 WideLines() const;
		/// <summary>
		/// LargePoints indicates whether points with size greater than 1.0 are supported. If this
		/// feature is not enabled, only a point size of 1.0 written by a shader is supported. The
		/// range and granularity of supported point sizes are indicated by the PointSizeRange and
		/// PointSizeGranularity members of the PhysicalDeviceLimits structure, respectively.
		/// </summary>
		Bool32 LargePoints() const;
		/// <summary>
		/// AlphaToOne indicates whether the implementation is able to replace the alpha value of
		/// the color fragment output from the fragment shader with the maximum representable alpha
		/// value for fixed-point colors or 1.0 for floating-point colors. If this feature is not
		/// enabled, then the EnableAlphaToOne member of the PipelineMultisampleStateCreateInfo
		/// structure must be set to false. Otherwise setting EnableAlphaToOne to true will enable		/// alpha-to-one behaviour.
		/// </summary>
		Bool32 AlphaToOne() const;
		/// <summary>
		/// MultiViewport indicates whether more than one viewport is supported. If this feature is
		/// not enabled, the ViewportCount and ScissorCount members of the
		/// PipelineViewportStateCreateInfo structure must be set to 1. Similarly, the
		/// ViewportCount parameter to the vkCmdSetViewport command and the ScissorCount parameter
		/// to the vkCmdSetScissor command must be 1, and the FirstViewport parameter to the
		/// vkCmdSetViewport command and the firstScissor parameter to the vkCmdSetScissor command
		/// must be 0.
		/// </summary>
		Bool32 MultiViewport() const;
		/// <summary>
		/// SamplerAnisotropy indicates whether anisotropic filtering is supported. If this feature
		/// is not enabled, the MaxAnisotropy member of the SamplerCreateInfo structure must be
		/// 1.0.
		/// </summary>
		Bool32 SamplerAnisotropy() const;
		/// <summary>
		/// TextureCompressionETC2 indicates whether the ETC2 and EAC compressed texture formats
		/// are supported. If this feature is not enabled, the formats whose names start with ETC2
		/// and EATC must not be used to create images.
		/// </summary>
		Bool32 TextureCompressionETC2() const;
		/// <summary>
		/// TextureCompressionASTC_LDR indicates whether the ASTC LDR compressed texture formats
		/// are supported. If this feature is not enabled, the formats whose names start with ASTC
		/// must not be used to create images.
		/// </summary>
		Bool32 TextureCompressionASTC_LDR() const;
		/// <summary>
		/// TextureCompressionBC indicates whether the BC compressed texture formats are supported.
		/// If this feature is not enabled, the formats whose names start with BC must not be used
		/// to create images.
		/// </summary>
		Bool32 TextureCompressionBC() const;
		/// <summary>
		/// OcclusionQueryPrecise indicates whether occlusion queries returning actual sample
		/// counts are supported. If this feature is enabled, queries of this type can use the
		/// QueryControl PRECISE flag in the flags parameter to vkCmdBeginQuery. If this feature
		/// is not supported, the implementation supports only boolean occlusion queries.
		/// </summary>
		Bool32 OcclusionQueryPrecise() const;
		/// <summary>
		/// PipelineStatisticsQuery indicates whether the pipeline statistics queries are
		/// supported. If this feature is not enabled, queries of type PIPELINE_STATISTICS cannot
		/// be created, and none of the QueryPipelineStatisticFlags bits can be set in the
		/// PipelineStatistics member of the QueryPoolCreateInfo structure.
		/// </summary>
		Bool32 PipelineStatisticsQuery() const;
		/// <summary>
		/// VertexPipelineStoresAndAtomics indicates whether storage buffers and images support
		/// stores and atomic operations in the vertex, tessellation, and geometry shader stages.
		/// If this feature is not enabled, all storage image, storage texel buffers, and storage
		/// buffer variables used by these stages in shader modules must be decorated with the
		/// NonWriteable decoration(or the readonly memory qualifier in GLSL).
		/// </summary>
		Bool32 VertexPipelineStoresAndAtomics() const;
		/// <summary>
		/// FragmentStoresAndAtomics indicates whether storage buffers and images support stores
		/// and atomic operations in the fragment shader stage. If this feature is not enabled, all
		/// storage image, storage texel buffers, and storage buffer variables used by the fragment
		/// stage in shader modules must be decorated with the NonWriteable decoration (or the
		/// readonly memory qualifier in GLSL).
		/// </summary>
		Bool32 FragmentStoresAndAtomics() const;
		/// <summary>
		/// ShaderTessellationAndGeometryPointSize indicates whether the PointSize built-in
		/// decoration is available in the tessellation control, tessellation evaluation, and
		/// geometry shader stages. If this feature is not enabled, the PointSize built-in
		/// decoration is not available in these shader stages and all points written from a
		/// tessellation or geometry shader will have a size of 1.0. This also indicates whether
		/// shader modules can declare the TessellationPointSize capability for tessellation
		/// control and evaluation shaders, or if the shader modules can declare the
		/// GeometryPointSize capability for geometry shaders. An implementation supporting this
		/// feature must also support one or both of the tessellationShader or geometryShader
		/// features.
		/// </summary>
		Bool32 ShaderTessellationAndGeometryPointSize() const;
		/// <summary>
		/// ShaderImageGatherExtended indicates whether the extended set of image gather
		/// instructions are available in shader code. If this feature is not enabled, the
		/// OpImage*Gather instructions do not support the Offset and ConstOffsets operands. This
		/// also indicates whether shader modules can declare the ImageGatherExtended capability.
		/// </summary>
		Bool32 ShaderImageGatherExtended() const;
		/// <summary>
		/// ShaderStorageImageExtendedFormats indicates whether the extended storage image formats
		/// are available in shader code. If this feature is not enabled, the formats requiring the
		/// StorageImageExtendedFormats capability are not supported for storage images. This also
		/// indicates whether shader modules can declare the StorageImageExtendedFormats
		/// capability.
		/// </summary>
		Bool32 ShaderStorageImageExtendedFormats() const;
		/// <summary>
		/// ShaderStorageImageMultisample indicates whether multisampled storage images are
		/// supported. If this feature is not enabled, images that are created with a usage that
		/// includes ImageUsage STORAGE must be created with samples equal to SampleCount X1. This
		/// also indicates whether shader modules can declare the StorageImageMultisample
		/// capability.
		/// </summary>
		Bool32 ShaderStorageImageMultisample() const;
		/// <summary>
		/// ShaderStorageImageReadWithoutFormat indicates whether storage images require a format
		/// qualifier to be specified when reading from storage images. If this feature is not
		/// enabled, the OpImageRead instruction must not have an OpTypeImage of Unknown. This also
		/// indicates whether shader modules can declare the StorageImageReadWithoutFormat
		/// capability.
		/// </summary>
		Bool32 ShaderStorageImageReadWithoutFormat() const;
		/// <summary>
		/// ShaderStorageImageWriteWithoutFormat indicates whether storage images require a format
		/// qualifier to be specified when writing to storage images.If this feature is not
		/// enabled, the OpImageWrite instruction must not have an OpTypeImage of Unknown. This
		/// also indicates whether shader modules can declare the StorageImageWriteWithoutFormat		/// capability.
		/// </summary>
		Bool32 ShaderStorageImageWriteWithoutFormat() const;
		/// <summary>
		/// ShaderUniformBufferArrayDynamicIndexing indicates whether arrays of uniform buffers can
		/// be indexed by dynamically uniform integer expressions in shader code. If this feature
		/// is not enabled, resources with a descriptor type of UNIFORM_BUFFER or
		/// UNIFORM_BUFFER_DYNAMIC must be indexed only by constant integral expressions when
		/// aggregated into arrays in shader code. This also indicates whether shader modules can
		/// declare the UniformBufferArrayDynamicIndexing capability.
		/// </summary>
		Bool32 ShaderUniformBufferArrayDynamicIndexing() const;
		/// <summary>
		/// ShaderSampledImageArrayDynamicIndexing indicates whether arrays of samplers or sampled
		/// images can be indexed by dynamically uniform integer expressions in shader code. If
		/// this feature is not enabled, resources with a descriptor type of SAMPLER,
		/// COMBINED_IMAGE_SAMPLER, or SAMPLED_IMAGE must be indexed only by constant integral
		/// expressions when aggregated into arrays in shader code. This also indicates whether
		/// shader modules can declare the SampledImageArrayDynamicIndexing capability.
		/// </summary>
		Bool32 ShaderSampledImageArrayDynamicIndexing() const;
		/// <summary>
		/// ShaderStorageBufferArrayDynamicIndexing indicates whether arrays of storage buffers can
		/// be indexed by dynamically uniform integer expressions in shader code. If this feature
		/// is not enabled, resources with a descriptor type of STORAGE_BUFFER or
		/// STORAGE_BUFFER_DYNAMIC must be indexed only by constant integral expressions when
		/// aggregated into arrays in shader code. This also indicates whether shader modules can
		/// declare the StorageBufferArrayDynamicIndexing capability.
		/// </summary>
		Bool32 ShaderStorageBufferArrayDynamicIndexing() const;
		/// <summary>
		/// ShaderStorageImageArrayDynamicIndexing indicates whether arrays of storage images can
		/// be indexed by dynamically uniform integer expressions in shader code. If this feature
		/// is not enabled, resources with a descriptor type of STORAGE_IMAGE must be indexed only
		/// by constant integral expressions when aggregated into arrays in shader code. This also
		/// indicates whether shader modules can declare the StorageImageArrayDynamicIndexing
		/// capability.
		/// </summary>
		Bool32 ShaderStorageImageArrayDynamicIndexing() const;
		/// <summary>
		/// ShaderClipDistance indicates whether clip distances are supported in shader code. If
		/// this feature is not enabled, the ClipDistance built-in decoration must not be used in
		/// shader modules. This also indicates whether shader modules can declare the
		/// ClipDistance capability.
		/// </summary>
		Bool32 ShaderClipDistance() const;
		/// <summary>
		/// ShaderCullDistance indicates whether cull distances are supported in shader code. If
		/// this feature is not enabled, the CullDistance built-in decoration must not be used in
		/// shader modules.This also indicates whether shader modules can declare the CullDistance		/// capability.
		/// </summary>
		Bool32 ShaderCullDistance() const;
		/// <summary>
		/// ShaderFloat64 indicates whether 64-bit floats (doubles) are supported in shader code.
		/// If this feature is not enabled, 64-bit floating-point types must not be used in shader
		/// code. This also indicates whether shader modules can declare the Float64 capability.
		/// </summary>
		Bool32 ShaderFloat64() const;
		/// <summary>
		/// ShaderInt64 indicates whether 64-bit integers (signed and unsigned) are supported in
		/// shader code. If this feature is not enabled, 64-bit integer types must not be used in
		/// shader code. This also indicates whether shader modules can declare the Int64
		/// capability.
		/// </summary>
		Bool32 ShaderInt64() const;
		/// <summary>
		/// ShaderInt16 indicates whether 16-bit integers (signed and unsigned) are supported in
		/// shader code. If this feature is not enabled, 16-bit integer types must not be used in
		/// shader code. This also indicates whether shader modules can declare the Int16		/// capability.
		/// </summary>
		Bool32 ShaderInt16() const;
		/// <summary>
		/// ShaderResourceResidency indicates whether image operations that return resource
		/// residency information are supported in shader code. If this feature is not enabled, the
		/// OpImageSparse* instructions must not be used in shader code. This also indicates
		/// whether shader modules can declare the SparseResidency capability. The feature requires
		/// at least one of the sparseResidency* features to be supported.
		/// </summary>
		Bool32 ShaderResourceResidency() const;
		/// <summary>
		/// ShaderResourceMinLod indicates whether image operations that specify the minimum
		/// resource level-of-detail(LOD) are supported in shader code. If this feature is not
		/// enabled, the MinLod image operand must not be used in shader code. This also indicates
		/// whether shader modules can declare the MinLod capability
		/// </summary>
		Bool32 ShaderResourceMinLod() const;
		/// <summary>
		/// SparseBinding indicates whether resource memory can be managed at opaque block level
		/// instead of at the object level. If this feature is not enabled, resource memory must be
		/// bound only on a per-object basis using the vkBindBufferMemory and vkBindImageMemory
		/// commands. In this case, buffers and images must not be created with SPARSE_BINDING bit
		/// set in the flags member of the BufferCreateInfo and ImageCreateInfo structures.
		/// Otherwise resource memory can be managed as described in Sparse Resource Features.
		/// </summary>
		Bool32 SparseBinding() const;
		/// <summary>
		/// SparseResidencyBuffer indicates whether the device can access partially resident
		/// buffers. If this feature is not enabled, buffers must not be created with
		/// SPARSE_RESIDENCY set in the flags member of the BufferCreateInfo structure.
		/// </summary>
		Bool32 SparseResidencyBuffer() const;
		/// <summary>
		/// SparseResidencyImage2D indicates whether the device can access partially resident 2D
		/// images with 1 sample per pixel. If this feature is not enabled, images with an
		/// ImageType of X2D and samples set to SampleCount X1 must not be created with
		/// SPARSE_RESIDENCY set in the flags member of the ImageCreateInfo structure.
		/// </summary>
		Bool32 SparseResidencyImage2D() const;
		/// <summary>
		/// SparseResidencyImage3D indicates whether the device can access partially resident 3D
		/// images. If this feature is not enabled, images with an ImageType of X3D must not be
		/// created with SPARSE_RESIDENCY set in the flags member of the ImageCreateInfo structure.
		/// </summary>
		Bool32 SparseResidencyImage3D() const;
		/// <summary>
		/// SparseResidency2Samples indicates whether the physical device can access partially
		/// resident 2D images with 2 samples per pixel. If this feature is not enabled, images
		/// with an ImageType of X2D and samples set to SampleCount X2 must not be created with
		/// SPARSE_RESIDENCY set in the flags member of the ImageCreateInfo structure.
		/// </summary>
		Bool32 SparseResidency2Samples() const;
		/// <summary>
		/// SparseResidency4Samples indicates whether the physical device can access partially
		/// resident 2D images with 4 samples per pixel. If this feature is not enabled, images
		/// with an ImageType of X2D and samples set to SampleCount X4 must not be created with
		/// SPARSE_RESIDENCY set in the flags member of the ImageCreateInfo structure.
		/// </summary>
		Bool32 SparseResidency4Samples() const;
		/// <summary>
		/// SparseResidency8Samples indicates whether the physical device can access partially
		/// resident 2D images with 8 samples per pixel. If this feature is not enabled, images
		/// with an ImageType of X2D and samples set to SampleCount X8 must not be created with
		/// SPARSE_RESIDENCY set in the flags member of the ImageCreateInfo structure.
		/// </summary>
		Bool32 SparseResidency8Samples() const;
		/// <summary>
		/// SparseResidency16Samples indicates whether the physical device can access partially
		/// resident 2D images with 16 samples per pixel. If this feature is not enabled, images
		/// with an ImageType of X2D and samples set to SampleCount X16 must not be created with
		/// SPARSE_RESIDENCY set in the flags member of the ImageCreateInfo structure.
		/// </summary>
		Bool32 SparseResidency16Samples() const;
		/// <summary>
		/// SparseResidencyAliased indicates whether the physical device can correctly access data
		/// aliased into multiple locations. If this feature is not enabled, the SPARSE_ALIASED
		/// bit must not be used in flags members of the BufferCreateInfo and ImageCreateInfo		/// structures, respectively.
		/// </summary>
		Bool32 SparseResidencyAliased() const;
		/// <summary>
		/// VariableMultisampleRate indicates whether all pipelines that will be bound to a command
		/// buffer during a subpass with no attachments must have the same value for
		/// PipelineMultisampleStateCreateInfo::RasterizationSamples. If set to true, the
		/// implementation supports variable multisample rates in a subpass with no attachments.
		/// If set to false, then all pipelines bound in such a subpass must have the same
		/// multisample rate. This has no effect in situations where a subpass uses any
		/// attachments.
		/// </summary>
		Bool32 VariableMultisampleRate() const;
		/// <summary>
		/// InheritedQueries indicates whether a secondary command buffer may be executed while a
		/// query is active.
		/// </summary>
		Bool32 InheritedQueries() const;

	private:
		Bool32 robustBufferAccess;
		Bool32 fullDrawIndexUint32;
		Bool32 imageCubeArray;
		Bool32 independentBlend;
		Bool32 geometryShader;
		Bool32 tessellationShader;
		Bool32 sampleRateShading;
		Bool32 dualSrcBlend;
		Bool32 logicOp;
		Bool32 multiDrawIndirect;
		Bool32 drawIndirectFirstInstance;
		Bool32 depthClamp;
		Bool32 depthBiasClamp;
		Bool32 fillModeNonSolid;
		Bool32 depthBounds;
		Bool32 wideLines;
		Bool32 largePoints;
		Bool32 alphaToOne;
		Bool32 multiViewport;
		Bool32 samplerAnisotropy;
		Bool32 textureCompressionETC2;
		Bool32 textureCompressionASTC_LDR;
		Bool32 textureCompressionBC;
		Bool32 occlusionQueryPrecise;
		Bool32 pipelineStatisticsQuery;
		Bool32 vertexPipelineStoresAndAtomics;
		Bool32 fragmentStoresAndAtomics;
		Bool32 shaderTessellationAndGeometryPointSize;
		Bool32 shaderImageGatherExtended;
		Bool32 shaderStorageImageExtendedFormats;
		Bool32 shaderStorageImageMultisample;
		Bool32 shaderStorageImageReadWithoutFormat;
		Bool32 shaderStorageImageWriteWithoutFormat;
		Bool32 shaderUniformBufferArrayDynamicIndexing;
		Bool32 shaderSampledImageArrayDynamicIndexing;
		Bool32 shaderStorageBufferArrayDynamicIndexing;
		Bool32 shaderStorageImageArrayDynamicIndexing;
		Bool32 shaderClipDistance;
		Bool32 shaderCullDistance;
		Bool32 shaderFloat64;
		Bool32 shaderInt64;
		Bool32 shaderInt16;
		Bool32 shaderResourceResidency;
		Bool32 shaderResourceMinLod;
		Bool32 sparseBinding;
		Bool32 sparseResidencyBuffer;
		Bool32 sparseResidencyImage2D;
		Bool32 sparseResidencyImage3D;
		Bool32 sparseResidency2Samples;
		Bool32 sparseResidency4Samples;
		Bool32 sparseResidency8Samples;
		Bool32 sparseResidency16Samples;
		Bool32 sparseResidencyAliased;
		Bool32 variableMultisampleRate;
		Bool32 inheritedQueries;
	};
}
