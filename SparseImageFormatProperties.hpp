#pragma once

#include "Extent3D.hpp"
#include "Flags.hpp"

namespace vulkan
{
	class SparseImageFormatProperties
	{
	public:
		ImageAspectFlags          AspectMask() const;
		Extent3D                  ImageGranularity() const;
		SparseImageFormatFlags    Flags() const;

	private:
		ImageAspectFlags          aspectMask;
		Extent3D                  imageGranularity;
		SparseImageFormatFlags    flags;
	};
}