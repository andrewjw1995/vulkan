#pragma once

#include <cstdint>

namespace vulkan
{
	class Extent2D
	{
	public:
		Extent2D();
		Extent2D(uint32_t width, uint32_t height);

		uint32_t width;
		uint32_t height;
	};
}