#pragma once

#include "Bool32.hpp"
#include "Enums.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineRasterizationStateCreateInfo : public Structure
	{
	public:
		PipelineRasterizationStateCreateInfo();

		PipelineRasterizationStateCreateInfo& Flags(PipelineRasterizationStateCreateFlags flags);
		PipelineRasterizationStateCreateInfo& EnableDepthClamp();
		PipelineRasterizationStateCreateInfo& DisableDepthClamp();
		PipelineRasterizationStateCreateInfo& PolygonMode(PolygonMode mode);
		PipelineRasterizationStateCreateInfo& CullMode(CullModeFlags mode);
		PipelineRasterizationStateCreateInfo& FrontFace(FrontFace face);
		PipelineRasterizationStateCreateInfo& EnableDepthBias(float constant, float clamp, float slope);
		PipelineRasterizationStateCreateInfo& DisableDepthBias();
		PipelineRasterizationStateCreateInfo& LineWidth(float width);

	private:
		PipelineRasterizationStateCreateFlags    flags;
		Bool32                                   depthClampEnable;
		Bool32                                   rasterizerDiscardEnable;
		vulkan::PolygonMode                      polygonMode;
		CullModeFlags                            cullMode;
		vulkan::FrontFace                        frontFace;
		Bool32                                   depthBiasEnable;
		float                                    depthBiasConstantFactor;
		float                                    depthBiasClamp;
		float                                    depthBiasSlopeFactor;
		float                                    lineWidth;
	};
}