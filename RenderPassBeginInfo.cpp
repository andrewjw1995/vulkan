#include "RenderPassBeginInfo.hpp"

using namespace vulkan;

RenderPassBeginInfo::RenderPassBeginInfo() :
	Structure(StructureType::RENDER_PASS_BEGIN_INFO),
	renderPass(),
	framebuffer(),
	renderArea(),
	clearValueCount(0),
	pClearValues(nullptr)
{ }

RenderPassBeginInfo& RenderPassBeginInfo::RenderPass(vulkan::RenderPass handle)
{
	this->renderPass = handle;
	return *this;
}

RenderPassBeginInfo& RenderPassBeginInfo::Framebuffer(vulkan::Framebuffer handle, Rectangle renderArea)
{
	this->framebuffer = handle;
	this->renderArea = renderArea;
	return *this;
}

RenderPassBeginInfo& RenderPassBeginInfo::ClearValues(uint32_t count, const ClearValue* pValues)
{
	this->clearValueCount = count;
	this->pClearValues = pValues;
	return *this;
}
