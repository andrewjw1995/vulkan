#include "PhysicalDeviceProperties.hpp"

using namespace vulkan;

Version PhysicalDeviceProperties::APIVersion() const
{
	return apiVersion;
}

Version PhysicalDeviceProperties::DriverVersion() const
{
	return driverVersion;
}

uint32_t PhysicalDeviceProperties::VendorID() const
{
	return vendorID;
}

uint32_t PhysicalDeviceProperties::DeviceID() const
{
	return deviceID;
}

PhysicalDeviceType PhysicalDeviceProperties::DeviceType() const
{
	return deviceType;
}

std::string PhysicalDeviceProperties::DeviceName() const
{
	return deviceName;
}

UUID PhysicalDeviceProperties::PipelineCacheUUID() const
{
	return pipelineCacheUUID;
}

const PhysicalDeviceLimits& PhysicalDeviceProperties::Limits() const
{
	return limits;
}

const PhysicalDeviceSparseProperties& PhysicalDeviceProperties::SparseProperties() const
{
	return sparseProperties;
}
