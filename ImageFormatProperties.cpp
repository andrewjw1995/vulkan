#include "ImageFormatProperties.hpp"

using namespace vulkan;

Extent3D ImageFormatProperties::MaxExtent() const
{
	return maxExtent;
}

uint32_t ImageFormatProperties::MaxMipLevels() const
{
	return maxMipLevels;
}

uint32_t ImageFormatProperties::MaxArrayLayers() const
{
	return maxArrayLayers;
}

SampleCountFlags ImageFormatProperties::SampleCounts() const
{
	return sampleCounts;
}

DeviceSize ImageFormatProperties::MaxResourceSize() const
{
	return maxResourceSize;
}
