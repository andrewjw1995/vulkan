#pragma once

#include "Bool32.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineMultisampleStateCreateInfo : public Structure
	{
	public:
		PipelineMultisampleStateCreateInfo();

		PipelineMultisampleStateCreateInfo& Flags(PipelineMultisampleStateCreateFlags flags);
		PipelineMultisampleStateCreateInfo& RasterizationSamples(SampleCountFlagBits samples, const uint32_t* pSampleMask);
		PipelineMultisampleStateCreateInfo& EnableSampleShading(float min);
		PipelineMultisampleStateCreateInfo& DisableSampleShading();
		PipelineMultisampleStateCreateInfo& EnableAlphaToCoverage();
		PipelineMultisampleStateCreateInfo& DisableAlphaToCoverage();
		PipelineMultisampleStateCreateInfo& EnableAlphaToOne();
		PipelineMultisampleStateCreateInfo& DisableAlphaToOne();

	private:
		PipelineMultisampleStateCreateFlags    flags;
		SampleCountFlagBits                    rasterizationSamples;
		Bool32                                 sampleShadingEnable;
		float                                  minSampleShading;
		const uint32_t*                        pSampleMask;
		Bool32                                 alphaToCoverageEnable;
		Bool32                                 alphaToOneEnable;
	};
}