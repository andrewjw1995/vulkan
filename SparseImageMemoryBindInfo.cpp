#include "SparseImageMemoryBindInfo.hpp"

using namespace vulkan;

SparseImageMemoryBindInfo::SparseImageMemoryBindInfo() = default;

SparseImageMemoryBindInfo& SparseImageMemoryBindInfo::Image(vulkan::Image image)
{
	this->image = image;
	return *this;
}

SparseImageMemoryBindInfo& SparseImageMemoryBindInfo::Binds(uint32_t count, const SparseImageMemoryBind* pBinds)
{
	this->bindCount = count;
	this->pBinds = pBinds;
	return *this;
}
