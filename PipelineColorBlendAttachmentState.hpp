#pragma once

#include "Bool32.hpp"
#include "Enums.hpp"
#include "Flags.hpp"

namespace vulkan
{
	class PipelineColorBlendAttachmentState
	{
	public:
		PipelineColorBlendAttachmentState();

		PipelineColorBlendAttachmentState& EnableBlend();
		PipelineColorBlendAttachmentState& DisableBlend();
		PipelineColorBlendAttachmentState& Color(BlendFactor source, BlendFactor destination, BlendOp op);
		PipelineColorBlendAttachmentState& Alpha(BlendFactor source, BlendFactor destination, BlendOp op);
		PipelineColorBlendAttachmentState& ColorWriteMask(ColorComponentFlags mask);

	private:
		Bool32                 blendEnable;
		BlendFactor            srcColorBlendFactor;
		BlendFactor            dstColorBlendFactor;
		BlendOp                colorBlendOp;
		BlendFactor            srcAlphaBlendFactor;
		BlendFactor            dstAlphaBlendFactor;
		BlendOp                alphaBlendOp;
		ColorComponentFlags    colorWriteMask;
	};
}