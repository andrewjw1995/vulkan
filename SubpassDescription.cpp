#include "SubpassDescription.hpp"

using namespace vulkan;

SubpassDescription::SubpassDescription() :
	flags(flags),
	pipelineBindPoint(PipelineBindPoint::GRAPHICS),
	inputAttachmentCount(0),
	pInputAttachments(nullptr),
	colorAttachmentCount(0),
	pColorAttachments(nullptr),
	pResolveAttachments(nullptr),
	pDepthStencilAttachment(nullptr),
	preserveAttachmentCount(0),
	pPreserveAttachments(nullptr)
{ }

SubpassDescription& SubpassDescription::Flags(SubpassDescriptionFlags flags)
{
	this->flags = flags;
	return *this;
}

SubpassDescription& SubpassDescription::BindPoint(PipelineBindPoint bindPoint)
{
	this->pipelineBindPoint = bindPoint;
	return *this;
}

SubpassDescription& SubpassDescription::InputAttachments(uint32_t count, const AttachmentReference* pAttachments)
{
	this->inputAttachmentCount = count;
	this->pInputAttachments = pAttachments;
	return *this;
}

SubpassDescription& SubpassDescription::ColorAttachments(uint32_t count, const AttachmentReference* pAttachments)
{
	this->colorAttachmentCount = count;
	this->pColorAttachments = pAttachments;
	return *this;
}

SubpassDescription& SubpassDescription::ColorAttachments(uint32_t count, const AttachmentReference* pAttachments, const AttachmentReference* pResolveAttachments)
{
	this->colorAttachmentCount = count;
	this->pColorAttachments = pAttachments;
	this->pResolveAttachments = pResolveAttachments;
	return *this;
}

SubpassDescription& SubpassDescription::DepthStencilAttachment(const AttachmentReference* pAttachment)
{
	this->pDepthStencilAttachment = pAttachment;
	return *this;
}

SubpassDescription& SubpassDescription::PreserveAttachments(uint32_t count, const uint32_t* pAttachments)
{
	this->preserveAttachmentCount = count;
	this->pPreserveAttachments = pAttachments;
	return *this;
}
