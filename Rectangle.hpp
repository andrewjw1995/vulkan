#pragma once

#include "Extent2D.hpp"
#include "Offset2D.hpp"

namespace vulkan
{
	class Rectangle
	{
	public:
		Rectangle();
		Rectangle(Offset2D offset, Extent2D extent);

		Offset2D offset;
		Extent2D extent;
	};
}