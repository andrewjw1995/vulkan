#pragma once

#include "ImageSubresourceLayers.hpp"
#include "Offset3D.hpp"

namespace vulkan
{
	class ImageBlit
	{
	public:
		ImageBlit();

		ImageBlit& Source(ImageSubresourceLayers subresource, Offset3D start, Offset3D end);
		ImageBlit& Destination(ImageSubresourceLayers subresource, Offset3D start, Offset3D end);

	private:
		ImageSubresourceLayers    srcSubresource;
		Offset3D                  srcOffsets[2];
		ImageSubresourceLayers    dstSubresource;
		Offset3D                  dstOffsets[2];
	};
}