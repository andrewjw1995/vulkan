#pragma once

#include "Bool32.hpp"
#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class CommandBufferInheritanceInfo : public Structure
	{
	public:
		CommandBufferInheritanceInfo();

		CommandBufferInheritanceInfo& RenderPass(RenderPass renderPass, uint32_t subpass);
		CommandBufferInheritanceInfo& Framebuffer(Framebuffer framebuffer);
		CommandBufferInheritanceInfo& EnableOcclusionQuery();
		CommandBufferInheritanceInfo& DisableOcclusionQuery();
		CommandBufferInheritanceInfo& QueryFlags(QueryControlFlags flags);
		CommandBufferInheritanceInfo& PipelineStatistics(QueryPipelineStatisticFlags flags);

	private:
		vulkan::RenderPass             renderPass;
		uint32_t                       subpass;
		vulkan::Framebuffer            framebuffer;
		Bool32                         occlusionQueryEnable;
		QueryControlFlags              queryFlags;
		QueryPipelineStatisticFlags    pipelineStatistics;
	};
}