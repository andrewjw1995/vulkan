#include "Functions.hpp"
#include "Instance.hpp"
#include "VulkanError.hpp"

using namespace std;
using namespace vulkan;

void Instance::Destroy()
{
	vkDestroyInstance(*this, nullptr);
	MakeNull();
}

vector<PhysicalDevice> Instance::EnumeratePhysicalDevices() const
{
	uint32_t count = 0;
	Result result = vkEnumeratePhysicalDevices(*this, &count, nullptr);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to count available physical devices");
	vector<PhysicalDevice> buffer(count);
	result = vkEnumeratePhysicalDevices(*this, &count, buffer.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to enumerate available physical devices");
	return buffer;
}

void Instance::Destroy(Surface surface) const
{
	vkDestroySurfaceKHR(*this, surface, nullptr);
}

Surface Instance::CreateWin32Surface(const Win32SurfaceCreateInfo& info) const
{
	Surface surface;
	Result result = vkCreateWin32SurfaceKHR(*this, &info, nullptr, &surface);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create surface");
	return surface;
}
