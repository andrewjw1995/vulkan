#include "RenderPassCreateInfo.hpp"

using namespace vulkan;

RenderPassCreateInfo::RenderPassCreateInfo() :
	Structure(StructureType::RENDER_PASS_CREATE_INFO),
	flags(),
	attachmentCount(0),
	pAttachments(nullptr),
	subpassCount(0),
	pSubpasses(nullptr),
	dependencyCount(0),
	pDependencies(nullptr)
{ }

RenderPassCreateInfo& RenderPassCreateInfo::Flags(RenderPassCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

RenderPassCreateInfo& RenderPassCreateInfo::Attachments(uint32_t count, const AttachmentDescription* pAttachments)
{
	this->attachmentCount = count;
	this->pAttachments = pAttachments;
	return *this;
}

RenderPassCreateInfo& RenderPassCreateInfo::Subpasses(uint32_t count, const SubpassDescription* pSubpasses)
{
	this->subpassCount = count;
	this->pSubpasses = pSubpasses;
	return *this;
}

RenderPassCreateInfo& RenderPassCreateInfo::Dependencies(uint32_t count, const SubpassDependency* pDependencies)
{
	this->dependencyCount = count;
	this->pDependencies = pDependencies;
	return *this;
}
