#pragma once

#include "Extent2D.hpp"
#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class FramebufferCreateInfo : public Structure
	{
	public:
		FramebufferCreateInfo();

		FramebufferCreateInfo& Flags(FramebufferCreateFlags flags);
		FramebufferCreateInfo& RenderPass(RenderPass handle);
		FramebufferCreateInfo& Attachments(Extent2D extent, uint32_t layers, uint32_t count, const ImageView* pAttachments);

	private:
		FramebufferCreateFlags flags;
		vulkan::RenderPass     renderPass;
		uint32_t               attachmentCount;
		const ImageView*       pAttachments;
		uint32_t               width;
		uint32_t               height;
		uint32_t               layers;
	};
}