#pragma once

#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PresentInfo : public Structure
	{
	public:
		PresentInfo();

		PresentInfo& WaitSemaphores(uint32_t count, const Semaphore* pSemaphores);
		PresentInfo& Swapchains(uint32_t count, const Swapchain* pSwapchains, const uint32_t* pImageIndices, Result* pResults);

	private:
		uint32_t         waitSemaphoreCount;
		const Semaphore* pWaitSemaphores;
		uint32_t         swapchainCount;
		const Swapchain* pSwapchains;
		const uint32_t*  pImageIndices;
		Result*          pResults;
	};
}