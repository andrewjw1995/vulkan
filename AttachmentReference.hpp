#pragma once

#include "Enums.hpp"

namespace vulkan
{
	class AttachmentReference
	{
	public:
		AttachmentReference(uint32_t attachment, ImageLayout layout);

	private:
		uint32_t    attachment;
		ImageLayout layout;
	};
}