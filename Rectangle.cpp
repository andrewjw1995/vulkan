#include "Rectangle.hpp"

using namespace vulkan;

Rectangle::Rectangle() :
	offset(),
	extent()
{ }

Rectangle::Rectangle(Offset2D offset, Extent2D extent) :
	offset(offset),
	extent(extent)
{ }
