#include "PhysicalDeviceFeatures.hpp"

using namespace vulkan;

PhysicalDeviceFeatures::PhysicalDeviceFeatures() :
	robustBufferAccess(),
	fullDrawIndexUint32(),
	imageCubeArray(),
	independentBlend(),
	geometryShader(),
	tessellationShader(),
	sampleRateShading(),
	dualSrcBlend(),
	logicOp(),
	multiDrawIndirect(),
	drawIndirectFirstInstance(),
	depthClamp(),
	depthBiasClamp(),
	fillModeNonSolid(),
	depthBounds(),
	wideLines(),
	largePoints(),
	alphaToOne(),
	multiViewport(),
	samplerAnisotropy(),
	textureCompressionETC2(),
	textureCompressionASTC_LDR(),
	textureCompressionBC(),
	occlusionQueryPrecise(),
	pipelineStatisticsQuery(),
	vertexPipelineStoresAndAtomics(),
	fragmentStoresAndAtomics(),
	shaderTessellationAndGeometryPointSize(),
	shaderImageGatherExtended(),
	shaderStorageImageExtendedFormats(),
	shaderStorageImageMultisample(),
	shaderStorageImageReadWithoutFormat(),
	shaderStorageImageWriteWithoutFormat(),
	shaderUniformBufferArrayDynamicIndexing(),
	shaderSampledImageArrayDynamicIndexing(),
	shaderStorageBufferArrayDynamicIndexing(),
	shaderStorageImageArrayDynamicIndexing(),
	shaderClipDistance(),
	shaderCullDistance(),
	shaderFloat64(),
	shaderInt64(),
	shaderInt16(),
	shaderResourceResidency(),
	shaderResourceMinLod(),
	sparseBinding(),
	sparseResidencyBuffer(),
	sparseResidencyImage2D(),
	sparseResidencyImage3D(),
	sparseResidency2Samples(),
	sparseResidency4Samples(),
	sparseResidency8Samples(),
	sparseResidency16Samples(),
	sparseResidencyAliased(),
	variableMultisampleRate(),
	inheritedQueries()
{ }

PhysicalDeviceFeatures& PhysicalDeviceFeatures::RobustBufferAccess(Bool32 enable)
{
	robustBufferAccess = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::FullDrawIndexUInt32(Bool32 enable)
{
	fullDrawIndexUint32 = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ImageCubeArray(Bool32 enable)
{
	imageCubeArray = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::IndependentBlend(Bool32 enable)
{
	independentBlend = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::GeometryShader(Bool32 enable)
{
	geometryShader = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::TessellationShader(Bool32 enable)
{
	tessellationShader = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SampleRateShading(Bool32 enable)
{
	sampleRateShading = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::DualSourceBlend(Bool32 enable)
{
	dualSrcBlend = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::LogicOp(Bool32 enable)
{
	logicOp = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::MultiDrawIndirect(Bool32 enable)
{
	multiDrawIndirect = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::DrawIndirectFirstInstance(Bool32 enable)
{
	drawIndirectFirstInstance = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::DepthClamp(Bool32 enable)
{
	depthClamp = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::DepthBiasClamp(Bool32 enable)
{
	depthBiasClamp = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::FillModeNonSolid(Bool32 enable)
{
	fillModeNonSolid = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::DepthBounds(Bool32 enable)
{
	depthBounds = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::WideLines(Bool32 enable)
{
	wideLines = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::LargePoints(Bool32 enable)
{
	largePoints = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::AlphaToOne(Bool32 enable)
{
	alphaToOne = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::MultiViewport(Bool32 enable)
{
	multiViewport = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SamplerAnisotropy(Bool32 enable)
{
	samplerAnisotropy = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::TextureCompressionETC2(Bool32 enable)
{
	textureCompressionETC2 = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::TextureCompressionASTC_LDR(Bool32 enable)
{
	textureCompressionASTC_LDR = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::TextureCompressionBC(Bool32 enable)
{
	textureCompressionBC = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::OcclusionQueryPrecise(Bool32 enable)
{
	occlusionQueryPrecise = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::PipelineStatisticsQuery(Bool32 enable)
{
	pipelineStatisticsQuery = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::VertexPipelineStoresAndAtomics(Bool32 enable)
{
	vertexPipelineStoresAndAtomics = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::FragmentStoresAndAtomics(Bool32 enable)
{
	fragmentStoresAndAtomics = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderTessellationAndGeometryPointSize(Bool32 enable)
{
	shaderTessellationAndGeometryPointSize = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderImageGatherExtended(Bool32 enable)
{
	shaderImageGatherExtended = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderStorageImageExtendedFormats(Bool32 enable)
{
	shaderStorageImageExtendedFormats = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderStorageImageMultisample(Bool32 enable)
{
	shaderStorageImageMultisample = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderStorageImageReadWithoutFormat(Bool32 enable)
{
	shaderStorageImageReadWithoutFormat = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderStorageImageWriteWithoutFormat(Bool32 enable)
{
	shaderStorageImageWriteWithoutFormat = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderUniformBufferArrayDynamicIndexing(Bool32 enable)
{
	shaderUniformBufferArrayDynamicIndexing = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderSampledImageArrayDynamicIndexing(Bool32 enable)
{
	shaderSampledImageArrayDynamicIndexing = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderStorageBufferArrayDynamicIndexing(Bool32 enable)
{
	shaderStorageBufferArrayDynamicIndexing = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderStorageImageArrayDynamicIndexing(Bool32 enable)
{
	shaderStorageImageArrayDynamicIndexing = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderClipDistance(Bool32 enable)
{
	shaderClipDistance = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderCullDistance(Bool32 enable)
{
	shaderCullDistance = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderFloat64(Bool32 enable)
{
	shaderFloat64 = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderInt64(Bool32 enable)
{
	shaderInt64 = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderInt16(Bool32 enable)
{
	shaderInt16 = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderResourceResidency(Bool32 enable)
{
	shaderResourceResidency = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::ShaderResourceMinLod(Bool32 enable)
{
	shaderResourceMinLod = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseBinding(Bool32 enable)
{
	sparseBinding = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseResidencyBuffer(Bool32 enable)
{
	sparseResidencyBuffer = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseResidencyImage2D(Bool32 enable)
{
	sparseResidencyImage2D = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseResidencyImage3D(Bool32 enable)
{
	sparseResidencyImage3D = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseResidency2Samples(Bool32 enable)
{
	sparseResidency2Samples = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseResidency4Samples(Bool32 enable)
{
	sparseResidency4Samples = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseResidency8Samples(Bool32 enable)
{
	sparseResidency8Samples = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseResidency16Samples(Bool32 enable)
{
	sparseResidency16Samples = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::SparseResidencyAliased(Bool32 enable)
{
	sparseResidencyAliased = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::VariableMultisampleRate(Bool32 enable)
{
	variableMultisampleRate = enable;
	return *this;
}

PhysicalDeviceFeatures& PhysicalDeviceFeatures::InheritedQueries(Bool32 enable)
{
	inheritedQueries = enable;
	return *this;
}

Bool32 PhysicalDeviceFeatures::RobustBufferAccess() const
{
	return robustBufferAccess;
}

Bool32 PhysicalDeviceFeatures::FullDrawIndexUInt32() const
{
	return fullDrawIndexUint32;
}

Bool32 PhysicalDeviceFeatures::ImageCubeArray() const
{
	return imageCubeArray;
}

Bool32 PhysicalDeviceFeatures::IndependentBlend() const
{
	return independentBlend;
}

Bool32 PhysicalDeviceFeatures::GeometryShader() const
{
	return geometryShader;
}

Bool32 PhysicalDeviceFeatures::TessellationShader() const
{
	return tessellationShader;
}

Bool32 PhysicalDeviceFeatures::SampleRateShading() const
{
	return sampleRateShading;
}

Bool32 PhysicalDeviceFeatures::DualSourceBlend() const
{
	return dualSrcBlend;
}

Bool32 PhysicalDeviceFeatures::LogicOp() const
{
	return logicOp;
}

Bool32 PhysicalDeviceFeatures::MultiDrawIndirect() const
{
	return multiDrawIndirect;
}

Bool32 PhysicalDeviceFeatures::DrawIndirectFirstInstance() const
{
	return drawIndirectFirstInstance;
}

Bool32 PhysicalDeviceFeatures::DepthClamp() const
{
	return depthClamp;
}

Bool32 PhysicalDeviceFeatures::DepthBiasClamp() const
{
	return depthBiasClamp;
}

Bool32 PhysicalDeviceFeatures::FillModeNonSolid() const
{
	return fillModeNonSolid;
}

Bool32 PhysicalDeviceFeatures::DepthBounds() const
{
	return depthBounds;
}

Bool32 PhysicalDeviceFeatures::WideLines() const
{
	return wideLines;
}

Bool32 PhysicalDeviceFeatures::LargePoints() const
{
	return largePoints;
}

Bool32 PhysicalDeviceFeatures::AlphaToOne() const
{
	return alphaToOne;
}

Bool32 PhysicalDeviceFeatures::MultiViewport() const
{
	return multiViewport;
}

Bool32 PhysicalDeviceFeatures::SamplerAnisotropy() const
{
	return samplerAnisotropy;
}

Bool32 PhysicalDeviceFeatures::TextureCompressionETC2() const
{
	return textureCompressionETC2;
}

Bool32 PhysicalDeviceFeatures::TextureCompressionASTC_LDR() const
{
	return textureCompressionASTC_LDR;
}

Bool32 PhysicalDeviceFeatures::TextureCompressionBC() const
{
	return textureCompressionBC;
}

Bool32 PhysicalDeviceFeatures::OcclusionQueryPrecise() const
{
	return occlusionQueryPrecise;
}

Bool32 PhysicalDeviceFeatures::PipelineStatisticsQuery() const
{
	return pipelineStatisticsQuery;
}

Bool32 PhysicalDeviceFeatures::VertexPipelineStoresAndAtomics() const
{
	return vertexPipelineStoresAndAtomics;
}

Bool32 PhysicalDeviceFeatures::FragmentStoresAndAtomics() const
{
	return fragmentStoresAndAtomics;
}

Bool32 PhysicalDeviceFeatures::ShaderTessellationAndGeometryPointSize() const
{
	return shaderTessellationAndGeometryPointSize;
}

Bool32 PhysicalDeviceFeatures::ShaderImageGatherExtended() const
{
	return shaderImageGatherExtended;
}

Bool32 PhysicalDeviceFeatures::ShaderStorageImageExtendedFormats() const
{
	return shaderStorageImageExtendedFormats;
}

Bool32 PhysicalDeviceFeatures::ShaderStorageImageMultisample() const
{
	return shaderStorageImageMultisample;
}

Bool32 PhysicalDeviceFeatures::ShaderStorageImageReadWithoutFormat() const
{
	return shaderStorageImageReadWithoutFormat;
}

Bool32 PhysicalDeviceFeatures::ShaderStorageImageWriteWithoutFormat() const
{
	return shaderStorageImageWriteWithoutFormat;
}

Bool32 PhysicalDeviceFeatures::ShaderUniformBufferArrayDynamicIndexing() const
{
	return shaderUniformBufferArrayDynamicIndexing;
}

Bool32 PhysicalDeviceFeatures::ShaderSampledImageArrayDynamicIndexing() const
{
	return shaderSampledImageArrayDynamicIndexing;
}

Bool32 PhysicalDeviceFeatures::ShaderStorageBufferArrayDynamicIndexing() const
{
	return shaderStorageBufferArrayDynamicIndexing;
}

Bool32 PhysicalDeviceFeatures::ShaderStorageImageArrayDynamicIndexing() const
{
	return shaderStorageImageArrayDynamicIndexing;
}

Bool32 PhysicalDeviceFeatures::ShaderClipDistance() const
{
	return shaderClipDistance;
}

Bool32 PhysicalDeviceFeatures::ShaderCullDistance() const
{
	return shaderCullDistance;
}

Bool32 PhysicalDeviceFeatures::ShaderFloat64() const
{
	return shaderFloat64;
}

Bool32 PhysicalDeviceFeatures::ShaderInt64() const
{
	return shaderInt64;
}

Bool32 PhysicalDeviceFeatures::ShaderInt16() const
{
	return shaderInt16;
}

Bool32 PhysicalDeviceFeatures::ShaderResourceResidency() const
{
	return shaderResourceResidency;
}

Bool32 PhysicalDeviceFeatures::ShaderResourceMinLod() const
{
	return shaderResourceMinLod;
}

Bool32 PhysicalDeviceFeatures::SparseBinding() const
{
	return sparseBinding;
}

Bool32 PhysicalDeviceFeatures::SparseResidencyBuffer() const
{
	return sparseResidencyBuffer;
}

Bool32 PhysicalDeviceFeatures::SparseResidencyImage2D() const
{
	return sparseResidencyImage2D;
}

Bool32 PhysicalDeviceFeatures::SparseResidencyImage3D() const
{
	return sparseResidencyImage3D;
}

Bool32 PhysicalDeviceFeatures::SparseResidency2Samples() const
{
	return sparseResidency2Samples;
}

Bool32 PhysicalDeviceFeatures::SparseResidency4Samples() const
{
	return sparseResidency4Samples;
}

Bool32 PhysicalDeviceFeatures::SparseResidency8Samples() const
{
	return sparseResidency8Samples;
}

Bool32 PhysicalDeviceFeatures::SparseResidency16Samples() const
{
	return sparseResidency16Samples;
}

Bool32 PhysicalDeviceFeatures::SparseResidencyAliased() const
{
	return sparseResidencyAliased;
}

Bool32 PhysicalDeviceFeatures::VariableMultisampleRate() const
{
	return variableMultisampleRate;
}

Bool32 PhysicalDeviceFeatures::InheritedQueries() const
{
	return inheritedQueries;
}
