#include "Version.hpp"

using namespace std;
using namespace vulkan;

Version::Version(uint32_t value)
{
	this->value = value;
}

Version::Version(uint32_t major, uint32_t minor, uint32_t patch)
{
	value = (major << 22) | (minor << 12) | patch;
}

uint32_t Version::Major() const
{
	return value >> 22;
}

uint32_t Version::Minor() const
{
	return (value >> 12) & 0x3ff;
}

uint32_t Version::Patch() const
{
	return value & 0xfff;
}

Version::operator uint32_t() const
{
	return value;
}

ostream& vulkan::operator <<(ostream& out, const Version& value)
{
	out << value.Major() << '.' << value.Minor() << '.' << value.Patch();
	return out;
}
