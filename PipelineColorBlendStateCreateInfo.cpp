#include "PipelineColorBlendStateCreateInfo.hpp"

using namespace vulkan;

PipelineColorBlendStateCreateInfo::PipelineColorBlendStateCreateInfo() :
	Structure(StructureType::PIPELINE_COLOR_BLEND_STATE_CREATE_INFO),
	flags(),
	logicOpEnable(false),
	logicOp(LogicOp::CLEAR),
	attachmentCount(0),
	pAttachments(nullptr),
	blendConstants()
{
	this->blendConstants[0] = 1.0;
	this->blendConstants[1] = 1.0;
	this->blendConstants[2] = 1.0;
	this->blendConstants[3] = 1.0;
}

PipelineColorBlendStateCreateInfo& PipelineColorBlendStateCreateInfo::Flags(PipelineColorBlendStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineColorBlendStateCreateInfo& PipelineColorBlendStateCreateInfo::EnableLogicOp(LogicOp op)
{
	this->logicOpEnable = true;
	this->logicOp = op;
	return *this;
}

PipelineColorBlendStateCreateInfo& PipelineColorBlendStateCreateInfo::DisableLogicOp()
{
	this->logicOpEnable = false;
	return *this;
}

PipelineColorBlendStateCreateInfo& PipelineColorBlendStateCreateInfo::Attachments(uint32_t count, const PipelineColorBlendAttachmentState* pAttachments)
{
	this->attachmentCount = count;
	this->pAttachments = pAttachments;
	return *this;
}

PipelineColorBlendStateCreateInfo& PipelineColorBlendStateCreateInfo::BlendConstants(float r, float g, float b, float a)
{
	this->blendConstants[0] = r;
	this->blendConstants[1] = g;
	this->blendConstants[2] = b;
	this->blendConstants[3] = a;
	return *this;
}
