#pragma once

#include "AttachmentDescription.hpp"
#include "Flags.hpp"
#include "Structure.hpp"
#include "SubpassDependency.hpp"
#include "SubpassDescription.hpp"

namespace vulkan
{
	class RenderPassCreateInfo : public Structure
	{
	public:
		RenderPassCreateInfo();

		RenderPassCreateInfo& Flags(RenderPassCreateFlags flags);
		RenderPassCreateInfo& Attachments(uint32_t count, const AttachmentDescription* pAttachments);
		RenderPassCreateInfo& Subpasses(uint32_t count, const SubpassDescription* pSubpasses);
		RenderPassCreateInfo& Dependencies(uint32_t count, const SubpassDependency* pDependencies);

	private:
		RenderPassCreateFlags           flags;
		uint32_t                        attachmentCount;
		const AttachmentDescription*    pAttachments;
		uint32_t                        subpassCount;
		const SubpassDescription*       pSubpasses;
		uint32_t                        dependencyCount;
		const SubpassDependency*        pDependencies;
	};
}