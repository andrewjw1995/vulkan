#pragma once

#include "DeviceSize.hpp"
#include "Extent3D.hpp"
#include "Flags.hpp"
#include "ImageSubresource.hpp"
#include "NonDispatchableHandle.hpp"
#include "Offset3D.hpp"

namespace vulkan
{
	class SparseImageMemoryBind
	{
	public:
		SparseImageMemoryBind();

		SparseImageMemoryBind& Subresource(ImageSubresource subresource);
		SparseImageMemoryBind& Offset(Offset3D offset);
		SparseImageMemoryBind& Extent(Extent3D extent);
		SparseImageMemoryBind& Memory(DeviceMemory memory);
		SparseImageMemoryBind& MemoryOffset(DeviceSize offset);
		SparseImageMemoryBind& Flags(SparseMemoryBindFlags flags);

	private:
		ImageSubresource         subresource;
		Offset3D                 offset;
		Extent3D                 extent;
		DeviceMemory             memory;
		DeviceSize               memoryOffset;
		SparseMemoryBindFlags    flags;
	};
}