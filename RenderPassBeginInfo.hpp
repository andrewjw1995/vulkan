#pragma once

#include "ClearValue.hpp"
#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "Rectangle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class RenderPassBeginInfo : public Structure
	{
	public:
		RenderPassBeginInfo();

		RenderPassBeginInfo& RenderPass(RenderPass handle);
		RenderPassBeginInfo& Framebuffer(Framebuffer handle, Rectangle renderArea);
		RenderPassBeginInfo& ClearValues(uint32_t count, const ClearValue* pValues);

	private:
		vulkan::RenderPass   renderPass;
		vulkan::Framebuffer  framebuffer;
		Rectangle            renderArea;
		uint32_t             clearValueCount;
		const ClearValue*    pClearValues;
	};
}