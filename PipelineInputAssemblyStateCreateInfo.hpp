#pragma once

#include "Bool32.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineInputAssemblyStateCreateInfo : public Structure
	{
	public:
		PipelineInputAssemblyStateCreateInfo();
		PipelineInputAssemblyStateCreateInfo(PrimitiveTopology topology, Bool32 primitiveRestartEnable);

		PipelineInputAssemblyStateCreateInfo& Flags(PipelineInputAssemblyStateCreateFlags flags);
		PipelineInputAssemblyStateCreateInfo& Topology(PrimitiveTopology topology);
		PipelineInputAssemblyStateCreateInfo& EnablePrimitiveRestart();
		PipelineInputAssemblyStateCreateInfo& DisablePrimitiveRestart();

	private:
		PipelineInputAssemblyStateCreateFlags    flags;
		PrimitiveTopology                        topology;
		Bool32                                   primitiveRestartEnable;
	};
}