#pragma once

#include "DeviceSize.hpp"
#include "Extent3D.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class ImageCreateInfo : public Structure
	{
	public:
		ImageCreateInfo();

		ImageCreateInfo& Flags(ImageCreateFlags flags);
		ImageCreateInfo& X1D(uint32_t width);
		ImageCreateInfo& X2D(uint32_t width, uint32_t height);
		ImageCreateInfo& X3D(uint32_t width, uint32_t height, uint32_t depth);
		ImageCreateInfo& Format(Format format);
		ImageCreateInfo& MipLevels(uint32_t levels);
		ImageCreateInfo& ArrayLayers(uint32_t layers);
		ImageCreateInfo& Samples(SampleCountFlagBits samples);
		ImageCreateInfo& Tiling(ImageTiling tiling);
		ImageCreateInfo& Usage(ImageUsageFlags usage);
		ImageCreateInfo& Exclusive();
		ImageCreateInfo& Concurrent(uint32_t count, uint32_t* pQueueFamilyIndices);
		ImageCreateInfo& InitialLayout(ImageLayout layout);

	private:
		ImageCreateFlags       flags;
		ImageType              imageType;
		vulkan::Format         format;
		Extent3D               extent;
		uint32_t               mipLevels;
		uint32_t               arrayLayers;
		SampleCountFlagBits    samples;
		ImageTiling            tiling;
		ImageUsageFlags        usage;
		SharingMode            sharingMode;
		uint32_t               queueFamilyIndexCount;
		const uint32_t*        pQueueFamilyIndices;
		ImageLayout            initialLayout;
	};
}