#include "CopyDescriptorSet.hpp"

using namespace vulkan;

CopyDescriptorSet::CopyDescriptorSet() :
	Structure(StructureType::COPY_DESCRIPTOR_SET),
	srcSet(),
	srcBinding(0),
	srcArrayElement(0),
	dstSet(),
	dstBinding(0),
	dstArrayElement(0),
	descriptorCount(0)
{ }

CopyDescriptorSet& CopyDescriptorSet::Source(DescriptorSet set, uint32_t binding, uint32_t arrayElement)
{
	this->srcSet = set;
	this->srcBinding = binding;
	this->srcArrayElement = arrayElement;
	return *this;
}

CopyDescriptorSet& CopyDescriptorSet::Destination(DescriptorSet set, uint32_t binding, uint32_t arrayElement)
{
	this->dstSet = set;
	this->dstBinding = binding;
	this->dstArrayElement = arrayElement;
	return *this;
}

CopyDescriptorSet& CopyDescriptorSet::DescriptorCount(uint32_t count)
{
	this->descriptorCount = count;
	return *this;
}
