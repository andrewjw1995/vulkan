#include "PipelineLayoutCreateInfo.hpp"

using namespace vulkan;

PipelineLayoutCreateInfo::PipelineLayoutCreateInfo() :
	Structure(StructureType::PIPELINE_LAYOUT_CREATE_INFO),
	flags(),
	setLayoutCount(0),
	pSetLayouts(nullptr),
	pushConstantRangeCount(0),
	pPushConstantRanges(nullptr)
{ }

PipelineLayoutCreateInfo& PipelineLayoutCreateInfo::Flags(PipelineLayoutCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineLayoutCreateInfo& PipelineLayoutCreateInfo::SetLayouts(uint32_t count, const DescriptorSetLayout* pSetLayouts)
{
	this->setLayoutCount = count;
	this->pSetLayouts = pSetLayouts;
	return *this;
}

PipelineLayoutCreateInfo& PipelineLayoutCreateInfo::PushConstantRanges(uint32_t count, const PushConstantRange* pRanges)
{
	this->pushConstantRangeCount = count;
	this->pPushConstantRanges = pRanges;
	return *this;
}
