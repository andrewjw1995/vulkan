#include "PipelineDynamicStateCreateInfo.hpp"

using namespace vulkan;

PipelineDynamicStateCreateInfo::PipelineDynamicStateCreateInfo() :
	Structure(StructureType::PIPELINE_DYNAMIC_STATE_CREATE_INFO),
	flags(),
	dynamicStateCount(0),
	pDynamicStates(nullptr)
{ }

PipelineDynamicStateCreateInfo& PipelineDynamicStateCreateInfo::Flags(PipelineDynamicStateCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

PipelineDynamicStateCreateInfo& PipelineDynamicStateCreateInfo::DynamicStates(uint32_t count, const DynamicState* pStates)
{
	this->dynamicStateCount = count;
	this->pDynamicStates = pStates;
	return *this;
}
