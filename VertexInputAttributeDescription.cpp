#include "VertexInputAttributeDescription.hpp"

using namespace vulkan;

VertexInputAttributeDescription::VertexInputAttributeDescription(uint32_t location, uint32_t binding, Format format, uint32_t offset) :
	location(location),
	binding(binding),
	format(format),
	offset(offset)
{ }
