#include "PresentInfo.hpp"

using namespace vulkan;

PresentInfo::PresentInfo() :
	Structure(StructureType::PRESENT_INFO_KHR),
	waitSemaphoreCount(0),
	pWaitSemaphores(nullptr),
	swapchainCount(0),
	pSwapchains(nullptr),
	pImageIndices(nullptr),
	pResults(nullptr)
{ }

PresentInfo& PresentInfo::WaitSemaphores(uint32_t count, const Semaphore* pSemaphores)
{
	this->waitSemaphoreCount = count;
	this->pWaitSemaphores = pSemaphores;
	return *this;
}

PresentInfo& PresentInfo::Swapchains(uint32_t count, const Swapchain* pSwapchains, const uint32_t* pImageIndices, Result* pResults)
{
	this->swapchainCount = count;
	this->pSwapchains = pSwapchains;
	this->pImageIndices = pImageIndices;
	this->pResults = pResults;
	return *this;
}
