#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineDynamicStateCreateInfo : public Structure
	{
	public:
		PipelineDynamicStateCreateInfo();

		PipelineDynamicStateCreateInfo& Flags(PipelineDynamicStateCreateFlags flags);
		PipelineDynamicStateCreateInfo& DynamicStates(uint32_t count, const DynamicState* pStates);

	private:
		PipelineDynamicStateCreateFlags    flags;
		uint32_t                           dynamicStateCount;
		const DynamicState*                pDynamicStates;
	};
}