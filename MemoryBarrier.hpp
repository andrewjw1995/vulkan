#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class MemoryBarrier : public Structure
	{
	public:
		MemoryBarrier();

		MemoryBarrier& Source(AccessFlags accessMask);
		MemoryBarrier& Destination(AccessFlags accessMask);

	private:
		AccessFlags srcAccessMask;
		AccessFlags dstAccessMask;
	};
}