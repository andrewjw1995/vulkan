#pragma once

#include "Bool32.hpp"
#include "Extent2D.hpp"
#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"
#include "SurfaceFormat.hpp"

namespace vulkan
{
	class SwapchainCreateInfo : public Structure
	{
	public:
		SwapchainCreateInfo();

		SwapchainCreateInfo& Flags(SwapchainCreateFlags flags);
		SwapchainCreateInfo& Surface(Surface surface);
		SwapchainCreateInfo& MinImageCount(uint32_t count);
		SwapchainCreateInfo& Image(SurfaceFormat format, Extent2D extent, uint32_t arrayLayers, ImageUsageFlags usage);
		SwapchainCreateInfo& Image(Format format, ColorSpace colorSpace, Extent2D extent, uint32_t arrayLayers, ImageUsageFlags usage);
		SwapchainCreateInfo& Exclusive();
		SwapchainCreateInfo& Concurrent(uint32_t count, const uint32_t* pQueueFamilyIndices);
		SwapchainCreateInfo& PreTransform(SurfaceTransformFlagBits transform);
		SwapchainCreateInfo& CompositeAlpha(CompositeAlphaFlagBits alpha);
		SwapchainCreateInfo& PresentMode(PresentMode mode);
		SwapchainCreateInfo& Clipped(bool clipped);
		SwapchainCreateInfo& OldSwapchain(Swapchain handle);

	private:
		SwapchainCreateFlags        flags;
		vulkan::Surface             surface;
		uint32_t                    minImageCount;
		Format                      imageFormat;
		ColorSpace                  imageColorSpace;
		Extent2D                    imageExtent;
		uint32_t                    imageArrayLayers;
		ImageUsageFlags             imageUsage;
		SharingMode                 imageSharingMode;
		uint32_t                    queueFamilyIndexCount;
		const uint32_t*             pQueueFamilyIndices;
		SurfaceTransformFlagBits    preTransform;
		CompositeAlphaFlagBits      compositeAlpha;
		vulkan::PresentMode         presentMode;
		Bool32                      clipped;
		Swapchain                   oldSwapchain;
	};
}