#include "NonDispatchableHandle.hpp"

using namespace vulkan;


NonDispatchableHandle::NonDispatchableHandle() :
	value(0)
{ }

bool NonDispatchableHandle::IsNull() const
{
	return value == 0;
}

void NonDispatchableHandle::MakeNull()
{
	value = 0;
}