#include "Device.hpp"
#include "Enums.hpp"
#include "Functions.hpp"
#include "VulkanError.hpp"

using namespace std;
using namespace vulkan;

void Device::Destroy()
{
	vkDestroyDevice(*this, nullptr);
}

Queue Device::GetQueue(uint32_t familyIndex, uint32_t queueIndex) const
{
	Queue queue;
	vkGetDeviceQueue(*this, familyIndex, queueIndex, &queue);
	return queue;
}

void Device::WaitIdle() const
{
	Result result = vkDeviceWaitIdle(*this);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to wait for device to become idle");
}

DeviceMemory Device::AllocateMemory(const MemoryAllocateInfo& info) const
{
	DeviceMemory memory;
	Result result = vkAllocateMemory(*this, &info, nullptr, &memory);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to allocate device memory");
	return memory;
}

void Device::Free(DeviceMemory memory) const
{
	vkFreeMemory(*this, memory, nullptr);
}

void* Device::MapMemory(DeviceMemory memory, DeviceSize offset, DeviceSize size) const
{
	void* p_data;
	Result result = vkMapMemory(*this, memory, offset, size, MemoryMapFlags(), &p_data);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to map memory");
	return p_data;
}

void Device::UnmapMemory(DeviceMemory memory)
{
	vkUnmapMemory(*this, memory);
}

void Device::FlushMappedMemoryRanges(uint32_t count, MappedMemoryRange* pRanges) const
{
	Result result = vkFlushMappedMemoryRanges(*this, count, pRanges);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to flush mapped memory ranges");
}

void Device::InvalidateMappedMemoryRanges(uint32_t count, MappedMemoryRange* pRanges) const
{
	Result result = vkInvalidateMappedMemoryRanges(*this, count, pRanges);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to invalidate mapped memory ranges");
}

DeviceSize Device::GetMemoryCommitment(DeviceMemory memory) const
{
	DeviceSize committed_memory;
	vkGetDeviceMemoryCommitment(*this, memory, &committed_memory);
	return committed_memory;
}

void Device::BindMemory(Buffer buffer, DeviceMemory memory, DeviceSize offset) const
{
	Result result = vkBindBufferMemory(*this, buffer, memory, offset);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to bind buffer memory");
}

void Device::BindMemory(Image image, DeviceMemory memory, DeviceSize offset) const
{
	Result result = vkBindImageMemory(*this, image, memory, offset);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to bind image memory");
}

MemoryRequirements Device::GetMemoryRequirements(Buffer buffer) const
{
	MemoryRequirements requirements;
	vkGetBufferMemoryRequirements(*this, buffer, &requirements);
	return requirements;
}

MemoryRequirements Device::GetMemoryRequirements(Image image) const
{
	MemoryRequirements requirements;
	vkGetImageMemoryRequirements(*this, image, &requirements);
	return requirements;
}

vector<SparseImageMemoryRequirements> Device::GetSparseMemoryRequirements(Image image)
{
	uint32_t count = 0;
	vkGetImageSparseMemoryRequirements(*this, image, &count, nullptr);
	vector<SparseImageMemoryRequirements> buffer(count);
	vkGetImageSparseMemoryRequirements(*this, image, &count, buffer.data());
	return buffer;
}

Fence Device::CreateFence(const FenceCreateInfo& info) const
{
	Fence fence;
	Result result = vkCreateFence(*this, &info, nullptr, &fence);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create fence");
	return fence;
}

void Device::Destroy(Fence fence) const
{
	vkDestroyFence(*this, fence, nullptr);
}

void Device::ResetFences(uint32_t count, const Fence* pFences) const
{
	Result result = vkResetFences(*this, count, pFences);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to reset fences");
}

bool Device::GetFenceStatus(Fence fence) const
{
	Result result = vkGetFenceStatus(*this, fence);
	switch (result)
	{
	case Result::SUCCESS:
		return true;
	case Result::NOT_READY:
		return false;
	default:
		throw VulkanError(result, "Failed to get fence status");
	}
}

bool Device::WaitForAny(uint32_t count, const Fence* pFences, uint64_t timeout) const
{
	Result result = vkWaitForFences(*this, count, pFences, false, timeout);
	switch (result)
	{
	case Result::SUCCESS:
		return true;
	case Result::TIMEOUT:
		return false;
	default:
		throw VulkanError(result, "Failed to wait for fences");
	}
}

bool Device::WaitForAll(uint32_t count, const Fence* pFences, uint64_t timeout) const
{
	Result result = vkWaitForFences(*this, count, pFences, true, timeout);
	switch (result)
	{
	case Result::SUCCESS:
		return true;
	case Result::TIMEOUT:
		return false;
	default:
		throw VulkanError(result, "Failed to wait for fences");
	}
}

Semaphore Device::CreateSemaphore(const SemaphoreCreateInfo& info) const
{
	Semaphore semaphore;
	Result result = vkCreateSemaphore(*this, &info, nullptr, &semaphore);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create semaphore");
	return semaphore;
}

void Device::Destroy(Semaphore semaphore) const
{
	vkDestroySemaphore(*this, semaphore, nullptr);
}

Event Device::CreateEvent(const EventCreateInfo& info) const
{
	Event event;
	Result result = vkCreateEvent(*this, &info, nullptr, &event);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create event");
	return event;
}

void Device::Destroy(Event event) const
{
	vkDestroyEvent(*this, event, nullptr);
}

bool Device::GetEventStatus(Event event) const
{
	Result result = vkGetEventStatus(*this, event);
	switch (result)
	{
	case Result::EVENT_SET:
		return true;
	case Result::EVENT_RESET:
		return false;
	default:
		throw VulkanError(result, "Failed to get event status");
	}
}

void Device::SetEvent(Event event) const
{
	Result result = vkSetEvent(*this, event);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to set event");
}

void Device::ResetEvent(Event event) const
{
	Result result = vkResetEvent(*this, event);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to reset event");
}

Buffer Device::CreateBuffer(const BufferCreateInfo& info) const
{
	Buffer buffer;
	Result result = vkCreateBuffer(*this, &info, nullptr, &buffer);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create buffer");
	return buffer;
}

void Device::Destroy(Buffer buffer) const
{
	vkDestroyBuffer(*this, buffer, nullptr);
}

BufferView Device::CreateBufferView(const BufferViewCreateInfo& info) const
{
	BufferView buffer_view;
	Result result = vkCreateBufferView(*this, &info, nullptr, &buffer_view);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create buffer view");
	return buffer_view;
}

void Device::Destroy(BufferView view) const
{
	vkDestroyBufferView(*this, view, nullptr);
}

Image Device::CreateImage(const ImageCreateInfo& info) const
{
	Image image;
	Result result = vkCreateImage(*this, &info, nullptr, &image);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create image");
	return image;
}

void Device::Destroy(Image image) const
{
	vkDestroyImage(*this, image, nullptr);
}

SubresourceLayout Device::GetImageSubresourceLayout(Image image, ImageSubresource subresource) const
{
	SubresourceLayout subresource_layout;
	vkGetImageSubresourceLayout(*this, image, &subresource, &subresource_layout);
	return subresource_layout;
}

ImageView Device::CreateImageView(const ImageViewCreateInfo& info) const
{
	ImageView image_view;
	Result result = vkCreateImageView(*this, &info, nullptr, &image_view);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create image view");
	return image_view;
}

void Device::Destroy(ImageView view) const
{
	vkDestroyImageView(*this, view, nullptr);
}

ShaderModule Device::CreateShaderModule(const ShaderModuleCreateInfo& info) const
{
	ShaderModule shader_module;
	Result result = vkCreateShaderModule(*this, &info, nullptr, &shader_module);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create shader module");
	return shader_module;
}

void Device::Destroy(ShaderModule module) const
{
	vkDestroyShaderModule(*this, module, nullptr);
}

PipelineCache Device::CreatePipelineCache(const PipelineCacheCreateInfo& info) const
{
	PipelineCache pipeline_cache;
	Result result = vkCreatePipelineCache(*this, &info, nullptr, &pipeline_cache);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create pipeline cache");
	return pipeline_cache;
}

void Device::Destroy(PipelineCache cache) const
{
	vkDestroyPipelineCache(*this, cache, nullptr);
}

// TODO: heap allocate, or use vector?
void* Device::GetPipelineCacheData(PipelineCache cache, size_t& size) const
{
	void* pointer = nullptr;
	vkGetPipelineCacheData(*this, cache, &size, pointer);
	return pointer;
}

void Device::MergePipelineCaches(PipelineCache destination, uint32_t count, const PipelineCache* pSources)
{
	Result result = vkMergePipelineCaches(*this, destination, count, pSources);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to merge pipeline caches");
}

std::vector<Pipeline> Device::CreateGraphicsPipelines(PipelineCache cache, uint32_t count, const GraphicsPipelineCreateInfo* pInfos) const
{
	vector<Pipeline> pipelines(count);
	Result result = vkCreateGraphicsPipelines(*this, cache, count, pInfos, nullptr, pipelines.data());
	if (result != Result::SUCCESS)
	{
		for (Pipeline pipeline : pipelines)
			vkDestroyPipeline(*this, pipeline, nullptr);
		throw VulkanError(result, "Failed to create graphics pipelines");
	}
	return pipelines;
}

std::vector<Pipeline> Device::CreateComputePipelines(PipelineCache cache, uint32_t count, const ComputePipelineCreateInfo* pInfos) const
{
	vector<Pipeline> pipelines(count);
	Result result = vkCreateComputePipelines(*this, cache, count, pInfos, nullptr, pipelines.data());
	if (result != Result::SUCCESS)
	{
		for (Pipeline pipeline : pipelines)
			vkDestroyPipeline(*this, pipeline, nullptr);
		throw VulkanError(result, "Failed to create graphics pipelines");
	}
	return pipelines;
}

void Device::Destroy(Pipeline pipeline) const
{
	vkDestroyPipeline(*this, pipeline, nullptr);
}

PipelineLayout Device::CreatePipelineLayout(const PipelineLayoutCreateInfo& info) const
{
	PipelineLayout pipeline_layout;
	Result result = vkCreatePipelineLayout(*this, &info, nullptr, &pipeline_layout);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create pipeline layout");
	return pipeline_layout;
}

void Device::Destroy(PipelineLayout layout)
{
	vkDestroyPipelineLayout(*this, layout, nullptr);
}

Sampler Device::CreateSampler(const SamplerCreateInfo& info) const
{
	Sampler sampler;
	Result result = vkCreateSampler(*this, &info, nullptr, &sampler);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create sampler");
	return sampler;
}

void Device::Destroy(Sampler sampler) const
{
	vkDestroySampler(*this, sampler, nullptr);
}

DescriptorSetLayout Device::CreateDescriptorSetLayout(const DescriptorSetLayoutCreateInfo& info) const
{
	DescriptorSetLayout descriptor_set_layout;
	Result result = vkCreateDescriptorSetLayout(*this, &info, nullptr, &descriptor_set_layout);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create descriptor set layout");
	return descriptor_set_layout;
}

void Device::Destroy(DescriptorSetLayout layout) const
{
	vkDestroyDescriptorSetLayout(*this, layout, nullptr);
}

DescriptorPool Device::CreateDescriptorPool(const DescriptorPoolCreateInfo& info) const
{
	DescriptorPool descriptor_pool;
	Result result = vkCreateDescriptorPool(*this, &info, nullptr, &descriptor_pool);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create descriptor pool");
	return descriptor_pool;
}

void Device::Destroy(DescriptorPool pool) const
{
	vkDestroyDescriptorPool(*this, pool, nullptr);
}

void Device::ResetDescriptorPool(DescriptorPool pool, DescriptorPoolResetFlags flags) const
{
	Result result = vkResetDescriptorPool(*this, pool, flags);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to reset descriptor pool");
}

std::vector<DescriptorSet> Device::AllocateDescriptorSets(const DescriptorSetAllocateInfo& info) const
{
	vector<DescriptorSet> descriptor_sets(info.DescriptorSetCount());
	Result result = vkAllocateDescriptorSets(*this, &info, descriptor_sets.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to allocate descriptor sets");
	return descriptor_sets;
}

void Device::Free(DescriptorPool pool, uint32_t count, const DescriptorSet* pSets) const
{
	Result result = vkFreeDescriptorSets(*this, pool, count, pSets);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to free descriptor sets");
}

void Device::UpdateDescriptorSets(uint32_t writeCount, const WriteDescriptorSet* pWrites, uint32_t copyCount, const CopyDescriptorSet* pCopies) const
{
	vkUpdateDescriptorSets(*this, writeCount, pWrites, copyCount, pCopies);
}

Framebuffer Device::CreateFramebuffer(const FramebufferCreateInfo& info) const
{
	Framebuffer framebuffer;
	Result result = vkCreateFramebuffer(*this, &info, nullptr, &framebuffer);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create framebuffer");
	return framebuffer;
}

void Device::Destroy(Framebuffer framebuffer) const
{
	vkDestroyFramebuffer(*this, framebuffer, nullptr);
}

RenderPass Device::CreateRenderPass(const RenderPassCreateInfo& info) const
{
	RenderPass render_pass;
	Result result = vkCreateRenderPass(*this, &info, nullptr, &render_pass);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create render pass");
	return render_pass;
}

void Device::Destroy(RenderPass renderPass) const
{
	vkDestroyRenderPass(*this, renderPass, nullptr);
}

Extent2D Device::GetRenderAreaGranularity(RenderPass renderPass) const
{
	Extent2D granularity;
	vkGetRenderAreaGranularity(*this, renderPass, &granularity);
	return granularity;
}

CommandPool Device::CreateCommandPool(const CommandPoolCreateInfo& info) const
{
	CommandPool command_pool;
	Result result = vkCreateCommandPool(*this, &info, nullptr, &command_pool);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create command pool");
	return command_pool;
}

void Device::Destroy(CommandPool pool) const
{
	vkDestroyCommandPool(*this, pool, nullptr);
}

void Device::ResetCommandPool(CommandPool pool, CommandPoolResetFlags flags)
{
	Result result = vkResetCommandPool(*this, pool, flags);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to reset command pool");
}

std::vector<CommandBuffer> Device::AllocateCommandBuffers(const CommandBufferAllocateInfo& info) const
{
	vector<CommandBuffer> command_buffers(info.CommandBufferCount());
	Result result = vkAllocateCommandBuffers(*this, &info, command_buffers.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to allocate command buffers");
	return command_buffers;
}

void Device::Free(CommandPool pool, uint32_t count, const CommandBuffer* pBuffers) const
{
	vkFreeCommandBuffers(*this, pool, count, pBuffers);
}

Swapchain Device::CreateSwapchain(const SwapchainCreateInfo& info) const
{
	Swapchain swapchain;
	Result result = vkCreateSwapchainKHR(*this, &info, nullptr, &swapchain);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create swapchain");
	return swapchain;
}

void Device::Destroy(Swapchain swapchain) const
{
	vkDestroySwapchainKHR(*this, swapchain, nullptr);
}

std::vector<Image> Device::GetSwapchainImages(Swapchain swapchain) const
{
	uint32_t count = 0;
	Result result = vkGetSwapchainImagesKHR(*this, swapchain, &count, nullptr);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to count swapchain images");
	vector<Image> buffer(count);
	result = vkGetSwapchainImagesKHR(*this, swapchain, &count, buffer.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to enumerate swapchain images");
	return buffer;
}

bool Device::AcquireNextImage(Swapchain swapchain, uint64_t timeout, Semaphore semaphore, Fence fence, uint32_t& imageIndex) const
{
	Result result = vkAcquireNextImageKHR(*this, swapchain, timeout, semaphore, fence, &imageIndex);
	switch (result)
	{
	case Result::SUCCESS:
		return true;
	case Result::TIMEOUT:
		return false;
	default:
		throw VulkanError(result, "Failed to acquire next image");
	}
}
