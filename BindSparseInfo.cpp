#include "BindSparseInfo.hpp"

using namespace vulkan;


BindSparseInfo::BindSparseInfo() :
	Structure(StructureType::BIND_SPARSE_INFO),
	waitSemaphoreCount(0),
	pWaitSemaphores(nullptr),
	bufferBindCount(0),
	pBufferBinds(nullptr),
	imageOpaqueBindCount(0),
	pImageOpaqueBinds(nullptr),
	imageBindCount(0),
	pImageBinds(nullptr),
	signalSemaphoreCount(0),
	pSignalSemaphores(nullptr)
{ }

BindSparseInfo& BindSparseInfo::WaitSemaphores(uint32_t count, const Semaphore* pSemaphores)
{
	this->waitSemaphoreCount = count;
	this->pWaitSemaphores = pSemaphores;
	return *this;
}

BindSparseInfo& BindSparseInfo::BufferBinds(uint32_t count, const SparseBufferMemoryBindInfo* pBinds)
{
	this->bufferBindCount = count;
	this->pBufferBinds = pBinds;
	return *this;
}

BindSparseInfo& BindSparseInfo::ImageOpaqueBinds(uint32_t count, const SparseImageOpaqueMemoryBindInfo* pBinds)
{
	this->imageOpaqueBindCount = count;
	this->pImageOpaqueBinds = pBinds;
	return *this;
}

BindSparseInfo& BindSparseInfo::ImageBinds(uint32_t count, const SparseImageMemoryBindInfo* pBinds)
{
	this->imageBindCount = count;
	this->pImageBinds = pBinds;
	return *this;
}

BindSparseInfo& BindSparseInfo::SignalSemaphores(uint32_t count, const Semaphore* pSemaphores)
{
	this->signalSemaphoreCount = count;
	this->pSignalSemaphores = pSemaphores;
	return *this;
}
