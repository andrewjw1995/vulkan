#include "EventCreateInfo.hpp"

using namespace vulkan;

EventCreateInfo::EventCreateInfo() :
	Structure(StructureType::EVENT_CREATE_INFO),
	flags()
{ }

EventCreateInfo::EventCreateInfo(EventCreateFlags flags) :
	Structure(StructureType::EVENT_CREATE_INFO),
	flags(flags)
{ }
