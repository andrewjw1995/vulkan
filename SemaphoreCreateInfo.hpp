#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class SemaphoreCreateInfo : public Structure
	{
	public:
		SemaphoreCreateInfo();
		SemaphoreCreateInfo(SemaphoreCreateFlags flags);

	private:
		SemaphoreCreateFlags flags;
	};
}