#pragma once

#include "Flags.hpp"

namespace vulkan
{
	class AttachmentDescription
	{
	public:
		AttachmentDescription();

		AttachmentDescription& Flags(AttachmentDescriptionFlags flags);
		AttachmentDescription& Format(Format format);
		AttachmentDescription& Samples(SampleCountFlagBits samples);
		AttachmentDescription& LoadOp(AttachmentLoadOp op);
		AttachmentDescription& StoreOp(AttachmentStoreOp op);
		AttachmentDescription& StencilLoadOp(AttachmentLoadOp op);
		AttachmentDescription& StencilStoreOp(AttachmentStoreOp op);
		AttachmentDescription& InitialLayout(ImageLayout layout);
		AttachmentDescription& FinalLayout(ImageLayout layout);

	private:
		AttachmentDescriptionFlags    flags;
		vulkan::Format                format;
		SampleCountFlagBits           samples;
		AttachmentLoadOp              loadOp;
		AttachmentStoreOp             storeOp;
		AttachmentLoadOp              stencilLoadOp;
		AttachmentStoreOp             stencilStoreOp;
		ImageLayout                   initialLayout;
		ImageLayout                   finalLayout;
	};
}