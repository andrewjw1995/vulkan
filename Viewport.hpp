#pragma once

#include "Extent2D.hpp"
#include "Offset2D.hpp"

namespace vulkan
{
	class Viewport
	{
	public:
		Viewport();
		Viewport(Offset2D offset, Extent2D extent, float minDepth, float maxDepth);
		Viewport(float x, float y, float width, float height, float minDepth, float maxDepth);

		Viewport& Offset(Offset2D offset);
		Viewport& Offset(float x, float y);
		Viewport& Extent(Extent2D extent);
		Viewport& Extent(float width, float height);
		Viewport& Depth(float min, float max);

		float    x;
		float    y;
		float    width;
		float    height;
		float    minDepth;
		float    maxDepth;
	};
}