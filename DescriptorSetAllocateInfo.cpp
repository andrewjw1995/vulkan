#include "DescriptorSetAllocateInfo.hpp"

using namespace vulkan;

DescriptorSetAllocateInfo::DescriptorSetAllocateInfo() :
	Structure(StructureType::DESCRIPTOR_SET_ALLOCATE_INFO),
	descriptorPool(),
	descriptorSetCount(0),
	pSetLayouts(nullptr)
{ }

DescriptorSetAllocateInfo& DescriptorSetAllocateInfo::DescriptorPool(vulkan::DescriptorPool pool)
{
	this->descriptorPool = pool;
	return *this;
}

DescriptorSetAllocateInfo& DescriptorSetAllocateInfo::DescriptorSets(uint32_t count, const DescriptorSetLayout* pSetLayouts)
{
	this->descriptorSetCount = count;
	this->pSetLayouts = pSetLayouts;
	return *this;
}

uint32_t DescriptorSetAllocateInfo::DescriptorSetCount() const
{
	return this->descriptorSetCount;
}
