#pragma once

#include "NonDispatchableHandle.hpp"
#include "SparseImageMemoryBind.hpp"

namespace vulkan
{
	class SparseImageMemoryBindInfo
	{
	public:
		SparseImageMemoryBindInfo();

		SparseImageMemoryBindInfo& Image(Image image);
		SparseImageMemoryBindInfo& Binds(uint32_t count, const SparseImageMemoryBind* pBinds);

	private:
		vulkan::Image                   image;
		uint32_t                        bindCount;
		const SparseImageMemoryBind*    pBinds;
	};
}