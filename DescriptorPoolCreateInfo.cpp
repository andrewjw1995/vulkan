#include "DescriptorPoolCreateInfo.hpp"

using namespace vulkan;

DescriptorPoolCreateInfo::DescriptorPoolCreateInfo() :
	Structure(StructureType::DESCRIPTOR_POOL_CREATE_INFO),
	flags(),
	maxSets(0),
	poolSizeCount(0),
	pPoolSizes(nullptr)
{ }

DescriptorPoolCreateInfo& DescriptorPoolCreateInfo::Flags(DescriptorPoolCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

DescriptorPoolCreateInfo& DescriptorPoolCreateInfo::MaxSets(uint32_t max)
{
	this->maxSets = max;
	return *this;
}

DescriptorPoolCreateInfo& DescriptorPoolCreateInfo::PoolSizes(uint32_t count, const DescriptorPoolSize* pSizes)
{
	this->poolSizeCount = count;
	this->pPoolSizes = pSizes;
	return *this;
}
