#pragma once

#include "ApplicationInfo.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

#include <vector>

namespace vulkan
{
	class InstanceCreateInfo : public Structure
	{
	public:
		InstanceCreateInfo(const ApplicationInfo& applicationInfo);

		InstanceCreateInfo& Flags(InstanceCreateFlags flags);
		InstanceCreateInfo& EnabledLayers(uint32_t count, const char* const* ppNames);
		InstanceCreateInfo& EnabledExtensions(uint32_t count, const char* const* ppNames);

	private:
		InstanceCreateFlags flags;
		const ApplicationInfo* pApplicationInfo;
		uint32_t enabledLayerCount;
		const char* const* ppEnabledLayerNames;
		uint32_t enabledExtensionCount;
		const char* const* ppEnabledExtensionNames;
	};
}