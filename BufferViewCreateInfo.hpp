#pragma once

#include "DeviceSize.hpp"
#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class BufferViewCreateInfo : public Structure
	{
	public:
		BufferViewCreateInfo();

		BufferViewCreateInfo& Flags(BufferViewCreateFlags flags);
		BufferViewCreateInfo& Buffer(Buffer buffer);
		BufferViewCreateInfo& Format(Format format);
		BufferViewCreateInfo& Offset(DeviceSize offset);
		BufferViewCreateInfo& Range(DeviceSize range);

	private:
		BufferViewCreateFlags    flags;
		vulkan::Buffer           buffer;
		vulkan::Format           format;
		DeviceSize               offset;
		DeviceSize               range;
	};
}