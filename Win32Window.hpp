#pragma once

#include "Window.hpp"
#include "WindowListener.hpp"

struct HINSTANCE__;
struct HWND__;
typedef HINSTANCE__ *HINSTANCE;
typedef HWND__ *HWND;
typedef void *PVOID;
typedef PVOID HANDLE;
#if defined(_WIN64)
typedef __int64 LONG_PTR;
typedef unsigned __int64 UINT_PTR;
#else
typedef long LONG_PTR;
typedef unsigned int UINT_PTR;
#endif
typedef LONG_PTR LPARAM;
typedef LONG_PTR LRESULT;
typedef unsigned int UINT;
typedef UINT_PTR WPARAM;

namespace vulkan
{
	class Win32Window : public Window
	{
	public:
		Win32Window(WindowListener& listener);
		~Win32Window() override;

		Extent2D Size() const override;
		Offset2D Position() const override;
		bool IsVisible() const override;
		bool IsMaximised() const override;
		bool IsResizeable() const override;
		bool IsPositionable() const override;
		void Hide() override;
		void Show() override;
		void Minimize() override;
		void Maximize() override;
		void Size(Extent2D size) override;
		void Position(Offset2D position) override;
		void ProcessEvents() override;
		LRESULT __stdcall WindowProcedure(UINT message, WPARAM wparam, LPARAM lparam);
		static LRESULT __stdcall StaticWindowProcedure(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam);

		HWND hwnd;
		WindowListener& listener;
	};
}