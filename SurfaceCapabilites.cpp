#include "SurfaceCapabilities.hpp"

using namespace vulkan;

uint32_t SurfaceCapabilities::MinImageCount() const
{
	return this->minImageCount;
}

uint32_t SurfaceCapabilities::MaxImageCount() const
{
	return this->maxImageCount;
}

Extent2D SurfaceCapabilities::CurrentExtent() const
{
	return this->currentExtent;
}

Extent2D SurfaceCapabilities::MinImageExtent() const
{
	return this->minImageExtent;
}

Extent2D SurfaceCapabilities::MaxImageExtent() const
{
	return this->maxImageExtent;
}

uint32_t SurfaceCapabilities::MaxImageArrayLayers() const
{
	return this->maxImageArrayLayers;
}

SurfaceTransformFlags SurfaceCapabilities::SupportedTransforms() const
{
	return this->supportedTransforms;
}

SurfaceTransformFlagBits SurfaceCapabilities::CurrentTransform() const
{
	return this->currentTransform;
}

CompositeAlphaFlags SurfaceCapabilities::SupportedCompositeAlpha() const
{
	return this->supportedCompositeAlpha;
}

ImageUsageFlags SurfaceCapabilities::SupportedUsageFlags() const
{
	return this->supportedUsageFlags;
}
