#pragma once

#include "Flags.hpp"
#include "PipelineColorBlendAttachmentState.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineColorBlendStateCreateInfo : public Structure
	{
	public:
		PipelineColorBlendStateCreateInfo();

		PipelineColorBlendStateCreateInfo& Flags(PipelineColorBlendStateCreateFlags flags);
		PipelineColorBlendStateCreateInfo& EnableLogicOp(LogicOp op);
		PipelineColorBlendStateCreateInfo& DisableLogicOp();
		PipelineColorBlendStateCreateInfo& Attachments(uint32_t count, const PipelineColorBlendAttachmentState* pAttachments);
		PipelineColorBlendStateCreateInfo& BlendConstants(float r, float g, float b, float a);

	private:
		PipelineColorBlendStateCreateFlags          flags;
		Bool32                                      logicOpEnable;
		LogicOp                                     logicOp;
		uint32_t                                    attachmentCount;
		const PipelineColorBlendAttachmentState*    pAttachments;
		float                                       blendConstants[4];
	};
}