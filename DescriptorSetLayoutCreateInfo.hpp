#pragma once

#include "DescriptorSetLayoutBinding.hpp"
#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class DescriptorSetLayoutCreateInfo : public Structure
	{
	public:
		DescriptorSetLayoutCreateInfo();

		DescriptorSetLayoutCreateInfo& Flags(DescriptorSetLayoutCreateFlags flags);
		DescriptorSetLayoutCreateInfo& Bindings(uint32_t count, const DescriptorSetLayoutBinding* pBindings);

	private:
		DescriptorSetLayoutCreateFlags       flags;
		uint32_t                             bindingCount;
		const DescriptorSetLayoutBinding*    pBindings;
	};
}