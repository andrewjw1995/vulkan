#include "Extent3D.hpp"

using namespace vulkan;

Extent3D::Extent3D() :
	width(0),
	height(0),
	depth(0)
{ }

Extent3D::Extent3D(uint32_t width, uint32_t height, uint32_t depth) :
	width(width),
	height(height),
	depth(depth)
{ }
