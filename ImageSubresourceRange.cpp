#include "ImageSubresourceRange.hpp"

using namespace vulkan;

ImageSubresourceRange::ImageSubresourceRange() :
	aspectMask(), // TODO: Sensible default?
	baseMipLevel(0),
	levelCount(1),
	baseArrayLayer(0),
	layerCount(1)
{ }

ImageSubresourceRange& ImageSubresourceRange::AspectMask(ImageAspectFlags mask)
{
	this->aspectMask = mask;
	return *this;
}

ImageSubresourceRange& ImageSubresourceRange::MipLevels(uint32_t base, uint32_t count)
{
	this->baseMipLevel = base;
	this->levelCount = count;
	return *this;
}

ImageSubresourceRange& ImageSubresourceRange::ArrayLayers(uint32_t base, uint32_t count)
{
	this->baseArrayLayer = base;
	this->layerCount = count;
	return *this;
}
