#include "Functions.hpp"
#include "Vulkan.hpp"
#include "VulkanError.hpp"

using namespace std;
using namespace vulkan;

vector<Extension> vulkan::EnumerateExtensions(const char* pLayer)
{
	Result result;
	uint32_t count = 0;
	result = vkEnumerateInstanceExtensionProperties(pLayer, &count, nullptr);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to count instance-level extensions");
	vector<Extension> extensions(count);
	result = vkEnumerateInstanceExtensionProperties(pLayer, &count, extensions.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to enumerate instance-level extensions");
	return extensions;
}

vector<Layer> vulkan::EnumerateLayers()
{
	Result result;
	uint32_t count = 0;
	result = vkEnumerateInstanceLayerProperties(&count, nullptr);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to count instance-level layers");
	vector<Layer> layers(count);
	result = vkEnumerateInstanceLayerProperties(&count, layers.data());
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to enumerate instance-level layers");
	return layers;
}

Instance vulkan::CreateInstance(const InstanceCreateInfo& info)
{
	Instance instance_handle;
	Result result = vkCreateInstance(&info, nullptr, &instance_handle);
	if (result != Result::SUCCESS)
		throw VulkanError(result, "Failed to create vulkan instance");
	return instance_handle;
}
