#pragma once

#include "DeviceSize.hpp"
#include "Flags.hpp"

namespace vulkan
{
	class MemoryHeap
	{
	public:
		DeviceSize       Size() const;
		MemoryHeapFlags  Flags() const;

	private:
		DeviceSize       size;
		MemoryHeapFlags  flags;
	};
}