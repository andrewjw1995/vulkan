#pragma once

#include "Version.hpp"

#include <string>

namespace vulkan
{
	class Extension
	{
	public:
		static const unsigned int MAX_NAME_SIZE = 256;

		Extension();

		const char* Name() const;
		uint32_t SpecificationVersion() const;

	private:
		char extensionName[MAX_NAME_SIZE];
		uint32_t specVersion;
	};
}
