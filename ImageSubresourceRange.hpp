#pragma once

#include "Flags.hpp"

namespace vulkan
{
	class ImageSubresourceRange
	{
	public:
		ImageSubresourceRange();

		ImageSubresourceRange& AspectMask(ImageAspectFlags mask);
		ImageSubresourceRange& MipLevels(uint32_t base, uint32_t count);
		ImageSubresourceRange& ArrayLayers(uint32_t base, uint32_t count);

	private:
		ImageAspectFlags    aspectMask;
		uint32_t            baseMipLevel;
		uint32_t            levelCount;
		uint32_t            baseArrayLayer;
		uint32_t            layerCount;
	};
}