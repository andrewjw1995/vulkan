#pragma once

#include "Extent3D.hpp"
#include "ImageSubresourceLayers.hpp"
#include "Offset3D.hpp"

namespace vulkan
{
	class ImageResolve
	{
	public:
		ImageResolve();

		ImageResolve& Source(ImageSubresourceLayers subresource, Offset3D offset);
		ImageResolve& Destination(ImageSubresourceLayers subresource, Offset3D offset);
		ImageResolve& Extent(Extent3D extent);

	private:
		ImageSubresourceLayers    srcSubresource;
		Offset3D                  srcOffset;
		ImageSubresourceLayers    dstSubresource;
		Offset3D                  dstOffset;
		Extent3D                  extent;
	};
}