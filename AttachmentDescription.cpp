#include "AttachmentDescription.hpp"

using namespace vulkan;

AttachmentDescription::AttachmentDescription() :
	flags(),
	format(Format::UNDEFINED),
	samples(SampleCountFlagBits::X1),
	loadOp(AttachmentLoadOp::LOAD), // TODO: CLEAR might be a more reasonable default
	storeOp(AttachmentStoreOp::STORE),
	stencilLoadOp(AttachmentLoadOp::LOAD), // TODO: CLEAR might be a more reasonable default
	stencilStoreOp(AttachmentStoreOp::STORE),
	initialLayout(ImageLayout::UNDEFINED),
	finalLayout(ImageLayout::UNDEFINED)
{ }

AttachmentDescription& AttachmentDescription::Flags(AttachmentDescriptionFlags flags)
{
	this->flags = flags;
	return *this;
}

AttachmentDescription& AttachmentDescription::Format(vulkan::Format format)
{
	this->format = format;
	return *this;
}

AttachmentDescription& AttachmentDescription::Samples(SampleCountFlagBits samples)
{
	this->samples = samples;
	return *this;
}

AttachmentDescription& AttachmentDescription::LoadOp(AttachmentLoadOp op)
{
	this->loadOp = op;
	return *this;
}

AttachmentDescription& AttachmentDescription::StoreOp(AttachmentStoreOp op)
{
	this->storeOp = op;
	return *this;
}

AttachmentDescription& AttachmentDescription::StencilLoadOp(AttachmentLoadOp op)
{
	this->stencilLoadOp = op;
	return *this;
}

AttachmentDescription& AttachmentDescription::StencilStoreOp(AttachmentStoreOp op)
{
	this->stencilStoreOp = op;
	return *this;
}

AttachmentDescription& AttachmentDescription::InitialLayout(ImageLayout layout)
{
	this->initialLayout = layout;
	return *this;
}

AttachmentDescription& AttachmentDescription::FinalLayout(ImageLayout layout)
{
	this->finalLayout = layout;
	return *this;
}
