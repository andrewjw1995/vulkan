#pragma once

#include "Structure.hpp"
#include "Version.hpp"

namespace vulkan
{
	class ApplicationInfo : public Structure
	{
	public:
		ApplicationInfo(Version apiVersion);

		/// <summary>
		/// Sets the name and version of the application, and returns itself, to allow chaining of
		/// the member-setter calls.
		/// </summary>
		/// <param name="pName">
		/// a pointer to a null-terminated UTF-8 string containing the name of the application
		/// </param>
		/// <param name="version">
		/// a Version containing the developer-supplied version number of the application
		/// </param>
		ApplicationInfo& Application(const char* pName, Version version);

		/// <summary>
		/// Sets the name of the application, and returns itself, to allow chaining of the
		/// member-setter calls.
		/// </summary>
		/// <param name="pName">
		/// a pointer to a null-terminated UTF-8 string containing the name of the application
		/// </param>
		ApplicationInfo& ApplicationName(const char* pName);

		/// <summary>
		/// Sets the version of the application, and returns itself, to allow chaining of the
		/// member-setter calls.
		/// </summary>
		/// <param name="version">
		/// a Version containing the developer-supplied version number of the application
		/// </param>
		ApplicationInfo& ApplicationVersion(Version version);

		/// <summary>
		/// Sets the name and version of the engine, and returns itself, to allow chaining of the
		/// member-setter calls.
		/// </summary>
		/// <param name="pName">
		/// a pointer to a null-terminated UTF-8 string containing the name of the engine (if any)
		/// used to create the application.
		/// </param>
		/// <param name="version">
		/// a Version containing the developer-supplied version number of the engine used to create
		/// the application.
		/// </param>
		ApplicationInfo& Engine(const char* pName, Version version);

		/// <summary>
		/// Sets the name of the engine, and returns itself, to allow chaining of the member-setter
		/// calls.
		/// </summary>
		/// <param name="pName">
		/// a pointer to a null-terminated UTF-8 string containing the name of the engine (if any)
		/// used to create the application.
		/// </param>
		ApplicationInfo& EngineName(const char* pName);

		/// <summary>
		/// Sets the version of the engine, and returns itself, to allow chaining of the
		/// member-setter calls.
		/// </summary>
		/// <param name="version">
		/// a Version containing the developer-supplied version number of the engine used to create
		/// the application.
		/// </param>
		ApplicationInfo& EngineVersion(Version version);

		const char* const& ApplicationName() const;
		const Version& ApplicationVersion() const;
		const char* const& EngineName() const;
		const Version& EngineVersion() const;
		const Version& APIVersion() const;

	private:
		const char* pApplicationName;
		uint32_t applicationVersion;
		const char* pEngineName;
		uint32_t engineVersion;
		uint32_t apiVersion;
	};
}
