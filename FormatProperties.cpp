#include "FormatProperties.hpp"

using namespace vulkan;

FormatFeatureFlags FormatProperties::LinearTilingFeatures() const
{
	return linearTilingFeatures;
}

FormatFeatureFlags FormatProperties::OptimalTilingFeatures() const
{
	return optimalTilingFeatures;
}

FormatFeatureFlags FormatProperties::BufferFeatures() const
{
	return bufferFeatures;
}
