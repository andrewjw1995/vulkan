#include "WriteDescriptorSet.hpp"

using namespace vulkan;

WriteDescriptorSet::WriteDescriptorSet() :
	Structure(StructureType::WRITE_DESCRIPTOR_SET),
	dstSet(),
	dstBinding(0),
	dstArrayElement(0),
	descriptorCount(0),
	descriptorType(DescriptorType::SAMPLER),
	pImageInfo(nullptr),
	pBufferInfo(nullptr),
	pTexelBufferView(nullptr)
{ }

WriteDescriptorSet& WriteDescriptorSet::Destination(DescriptorSet set, uint32_t binding, uint32_t arrayElement)
{
	this->dstSet = set;
	this->dstBinding = binding;
	this->dstArrayElement = arrayElement;
	return *this;
}

WriteDescriptorSet& WriteDescriptorSet::Descriptor(DescriptorType type, uint32_t count)
{
	this->descriptorCount = count;
	this->descriptorType = type;
	this->pImageInfo = nullptr;
	this->pBufferInfo = nullptr;
	this->pTexelBufferView = nullptr;
	return *this;
}

WriteDescriptorSet& WriteDescriptorSet::Descriptor(DescriptorType type, uint32_t count, const DescriptorImageInfo* pInfo)
{
	this->descriptorCount = count;
	this->descriptorType = type;
	this->pImageInfo = pInfo;
	this->pBufferInfo = nullptr;
	this->pTexelBufferView = nullptr;
	return *this;
}

WriteDescriptorSet& WriteDescriptorSet::Descriptor(DescriptorType type, uint32_t count, const DescriptorBufferInfo* pInfo)
{
	this->descriptorCount = count;
	this->descriptorType = type;
	this->pImageInfo = nullptr;
	this->pBufferInfo = pInfo;
	this->pTexelBufferView = nullptr;
	return *this;
}

WriteDescriptorSet& WriteDescriptorSet::Descriptor(DescriptorType type, uint32_t count, const BufferView* pInfo)
{
	this->descriptorCount = count;
	this->descriptorType = type;
	this->pImageInfo = nullptr;
	this->pBufferInfo = nullptr;
	this->pTexelBufferView = pInfo;
	return *this;
}
