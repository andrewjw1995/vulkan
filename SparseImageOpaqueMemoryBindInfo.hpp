#pragma once

#include "NonDispatchableHandle.hpp"
#include "SparseMemoryBind.hpp"

namespace vulkan
{
	class SparseImageOpaqueMemoryBindInfo
	{
	public:
		SparseImageOpaqueMemoryBindInfo();

		SparseImageOpaqueMemoryBindInfo& Image(Image image);
		SparseImageOpaqueMemoryBindInfo& Binds(uint32_t count, const SparseMemoryBind* pBinds);

	private:
		vulkan::Image              image;
		uint32_t                   bindCount;
		const SparseMemoryBind*    pBinds;
	};
}