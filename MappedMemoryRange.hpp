#pragma once

#include "DeviceSize.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class MappedMemoryRange : public Structure
	{
	public:
		MappedMemoryRange();
		MappedMemoryRange(DeviceMemory memory, DeviceSize offset, DeviceSize size);

		MappedMemoryRange& Memory(DeviceMemory memory);
		MappedMemoryRange& Offset(DeviceSize offset);
		MappedMemoryRange& Size(DeviceSize size);
		DeviceMemory Memory() const;
		DeviceSize Offset() const;
		DeviceSize Size() const;

	private:
		DeviceMemory     memory;
		DeviceSize       offset;
		DeviceSize       size;
	};
}