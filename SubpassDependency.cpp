#include "SubpassDependency.hpp"

using namespace vulkan;

SubpassDependency::SubpassDependency() :
	srcSubpass(0),
	dstSubpass(0),
	srcStageMask(),
	dstStageMask(),
	srcAccessMask(),
	dstAccessMask(),
	dependencyFlags()
{ }

SubpassDependency& SubpassDependency::Source(uint32_t index, PipelineStageFlags stageMask, AccessFlags accessMask)
{
	this->srcSubpass = index;
	this->srcStageMask = stageMask;
	this->srcAccessMask = accessMask;
	return *this;
}

SubpassDependency& SubpassDependency::Destination(uint32_t index, PipelineStageFlags stageMask, AccessFlags accessMask)
{
	this->dstSubpass = index;
	this->dstStageMask = stageMask;
	this->dstAccessMask = accessMask;
	return *this;
}

SubpassDependency& SubpassDependency::Flags(DependencyFlags flags)
{
	this->dependencyFlags = flags;
	return *this;
}
