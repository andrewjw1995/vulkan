#pragma once

#include <cstdint>

namespace vulkan
{
	typedef uint64_t DeviceSize;
}