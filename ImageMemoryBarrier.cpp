#include "ImageMemoryBarrier.hpp"

using namespace vulkan;

ImageMemoryBarrier::ImageMemoryBarrier() :
	Structure(StructureType::IMAGE_MEMORY_BARRIER),
	srcAccessMask(),
	dstAccessMask(),
	oldLayout(ImageLayout::UNDEFINED),
	newLayout(ImageLayout::UNDEFINED),
	srcQueueFamilyIndex(0),
	dstQueueFamilyIndex(0),
	image(),
	subresourceRange()
{ }

ImageMemoryBarrier& ImageMemoryBarrier::Source(AccessFlags accessMask, uint32_t queueFamily)
{
	this->srcAccessMask = accessMask;
	this->srcQueueFamilyIndex = queueFamily;
	return *this;
}

ImageMemoryBarrier& ImageMemoryBarrier::Destination(AccessFlags accessMask, uint32_t queueFamily)
{
	this->dstAccessMask = accessMask;
	this->dstQueueFamilyIndex = queueFamily;
	return *this;
}

ImageMemoryBarrier& ImageMemoryBarrier::Image(vulkan::Image image, ImageSubresourceRange subresourceRange, ImageLayout oldLayout, ImageLayout newLayout)
{
	this->image = image;
	this->subresourceRange = subresourceRange;
	this->oldLayout = oldLayout;
	this->newLayout = newLayout;
	return *this;
}
