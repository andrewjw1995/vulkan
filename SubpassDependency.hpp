#pragma once

#include "Flags.hpp"

namespace vulkan
{
	class SubpassDependency
	{
	public:
		SubpassDependency();

		SubpassDependency& Source(uint32_t index, PipelineStageFlags stageMask, AccessFlags accessMask);
		SubpassDependency& Destination(uint32_t index, PipelineStageFlags stageMask, AccessFlags accessMask);
		SubpassDependency& Flags(DependencyFlags flags);

	private:
		uint32_t              srcSubpass;
		uint32_t              dstSubpass;
		PipelineStageFlags    srcStageMask;
		PipelineStageFlags    dstStageMask;
		AccessFlags           srcAccessMask;
		AccessFlags           dstAccessMask;
		DependencyFlags       dependencyFlags;
	};
}