#pragma once

#include "BufferCreateInfo.hpp"
#include "BufferViewCreateInfo.hpp"
#include "CommandBuffer.hpp"
#include "CommandBufferAllocateInfo.hpp"
#include "CommandPoolCreateInfo.hpp"
#include "ComputePipelineCreateInfo.hpp"
#include "CopyDescriptorSet.hpp"
#include "DescriptorPoolCreateInfo.hpp"
#include "DescriptorSetAllocateInfo.hpp"
#include "DescriptorSetLayoutCreateInfo.hpp"
#include "DispatchableHandle.hpp"
#include "EventCreateInfo.hpp"
#include "FenceCreateInfo.hpp"
#include "FramebufferCreateInfo.hpp"
#include "GraphicsPipelineCreateInfo.hpp"
#include "ImageCreateInfo.hpp"
#include "ImageViewCreateInfo.hpp"
#include "MappedMemoryRange.hpp"
#include "MemoryAllocateInfo.hpp"
#include "MemoryRequirements.hpp"
#include "PipelineCacheCreateInfo.hpp"
#include "PipelineLayoutCreateInfo.hpp"
#include "Queue.hpp"
#include "RenderPassCreateInfo.hpp"
#include "SamplerCreateInfo.hpp"
#include "SemaphoreCreateInfo.hpp"
#include "ShaderModuleCreateInfo.hpp"
#include "SparseImageMemoryRequirements.hpp"
#include "SubresourceLayout.hpp"
#include "SwapchainCreateInfo.hpp"
#include "WriteDescriptorSet.hpp"

#include <vector>

namespace vulkan
{
	class Device : public DispatchableHandle
	{
	public:
		void Destroy();
		Queue GetQueue(uint32_t familyIndex, uint32_t queueIndex) const;
		void WaitIdle() const;
		DeviceMemory AllocateMemory(const MemoryAllocateInfo& info) const;
		void Free(DeviceMemory memory) const;
		void* MapMemory(DeviceMemory memory, DeviceSize offset, DeviceSize size) const;
		void UnmapMemory(DeviceMemory memory);
		void FlushMappedMemoryRanges(uint32_t count, MappedMemoryRange* pRanges) const;
		void InvalidateMappedMemoryRanges(uint32_t count, MappedMemoryRange* pRanges) const;
		DeviceSize GetMemoryCommitment(DeviceMemory memory) const;
		void BindMemory(Buffer buffer, DeviceMemory memory, DeviceSize offset) const;
		void BindMemory(Image image, DeviceMemory memory, DeviceSize offset) const;
		MemoryRequirements GetMemoryRequirements(Buffer buffer) const;
		MemoryRequirements GetMemoryRequirements(Image image) const;
		std::vector<SparseImageMemoryRequirements> GetSparseMemoryRequirements(Image image);
		Fence CreateFence(const FenceCreateInfo& info) const;
		void Destroy(Fence fence) const;
		void ResetFences(uint32_t count, const Fence* pFences) const;
		bool GetFenceStatus(Fence fence) const;
		bool WaitForAny(uint32_t count, const Fence* pFences, uint64_t timeout) const;
		bool WaitForAll(uint32_t count, const Fence* pFences, uint64_t timeout) const;
		Semaphore CreateSemaphore(const SemaphoreCreateInfo& info) const;
		void Destroy(Semaphore semaphore) const;
		Event CreateEvent(const EventCreateInfo& info) const;
		void Destroy(Event event) const;
		bool GetEventStatus(Event event) const;
		void SetEvent(Event event) const;
		void ResetEvent(Event event) const;
		Buffer CreateBuffer(const BufferCreateInfo& info) const;
		void Destroy(Buffer buffer) const;
		BufferView CreateBufferView(const BufferViewCreateInfo& info) const;
		void Destroy(BufferView view) const;
		Image CreateImage(const ImageCreateInfo& info) const;
		void Destroy(Image image) const;
		SubresourceLayout GetImageSubresourceLayout(Image image, ImageSubresource subresource) const;
		ImageView CreateImageView(const ImageViewCreateInfo& info) const;
		void Destroy(ImageView view) const;
		ShaderModule CreateShaderModule(const ShaderModuleCreateInfo& info) const;
		void Destroy(ShaderModule module) const;
		PipelineCache CreatePipelineCache(const PipelineCacheCreateInfo& info) const;
		void Destroy(PipelineCache cache) const;
		void* GetPipelineCacheData(PipelineCache cache, size_t& size) const;
		void MergePipelineCaches(PipelineCache destination, uint32_t count, const PipelineCache* pSources);
		std::vector<Pipeline> CreateGraphicsPipelines(PipelineCache cache, uint32_t count, const GraphicsPipelineCreateInfo* pInfos) const;
		std::vector<Pipeline> CreateComputePipelines(PipelineCache cache, uint32_t count, const ComputePipelineCreateInfo* pInfos) const;
		void Destroy(Pipeline pipeline) const;
		PipelineLayout CreatePipelineLayout(const PipelineLayoutCreateInfo& info) const;
		void Destroy(PipelineLayout layout);
		Sampler CreateSampler(const SamplerCreateInfo& info) const;
		void Destroy(Sampler sampler) const;
		DescriptorSetLayout CreateDescriptorSetLayout(const DescriptorSetLayoutCreateInfo& info) const;
		void Destroy(DescriptorSetLayout layout) const;
		DescriptorPool CreateDescriptorPool(const DescriptorPoolCreateInfo& info) const;
		void Destroy(DescriptorPool pool) const;
		void ResetDescriptorPool(DescriptorPool pool, DescriptorPoolResetFlags flags) const;
		std::vector<DescriptorSet> AllocateDescriptorSets(const DescriptorSetAllocateInfo& info) const;
		void Free(DescriptorPool pool, uint32_t count, const DescriptorSet* pSets) const;
		void UpdateDescriptorSets(uint32_t writeCount, const WriteDescriptorSet* pWrites, uint32_t copyCount, const CopyDescriptorSet* pCopies) const;
		Framebuffer CreateFramebuffer(const FramebufferCreateInfo& info) const;
		void Destroy(Framebuffer framebuffer) const;
		RenderPass CreateRenderPass(const RenderPassCreateInfo& info) const;
		void Destroy(RenderPass renderPass) const;
		Extent2D GetRenderAreaGranularity(RenderPass renderPass) const;
		CommandPool CreateCommandPool(const CommandPoolCreateInfo& info) const;
		void Destroy(CommandPool pool) const;
		void ResetCommandPool(CommandPool pool, CommandPoolResetFlags flags);
		std::vector<CommandBuffer> AllocateCommandBuffers(const CommandBufferAllocateInfo& info) const;
		void Free(CommandPool pool, uint32_t count, const CommandBuffer* pBuffers) const;
		Swapchain CreateSwapchain(const SwapchainCreateInfo& info) const;
		void Destroy(Swapchain swapchain) const;
		std::vector<Image> GetSwapchainImages(Swapchain swapchain) const;
		bool AcquireNextImage(Swapchain swapchain, uint64_t timeout, Semaphore semaphore, Fence fence, uint32_t& imageIndex) const;
	};
}