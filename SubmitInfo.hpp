#pragma once

#include "CommandBuffer.hpp"
#include "Enums.hpp"
#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class SubmitInfo : public Structure
	{
	public:
		SubmitInfo();

		SubmitInfo& WaitSemaphores(uint32_t count, const Semaphore* pSemaphores, const PipelineStageFlags* pDestinations);
		SubmitInfo& CommandBuffers(uint32_t count, const CommandBuffer* pBuffers);
		SubmitInfo& SignalSemaphores(uint32_t count, const Semaphore* pSemaphores);

	private:
		uint32_t                       waitSemaphoreCount;
		const Semaphore*               pWaitSemaphores;
		const PipelineStageFlags*      pWaitDstStageMask;
		uint32_t                       commandBufferCount;
		const CommandBuffer*           pCommandBuffers;
		uint32_t                       signalSemaphoreCount;
		const Semaphore*               pSignalSemaphores;
	};
}