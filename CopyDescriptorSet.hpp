#pragma once

#include "NonDispatchableHandle.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class CopyDescriptorSet : public Structure
	{
	public:
		CopyDescriptorSet();

		CopyDescriptorSet& Source(DescriptorSet set, uint32_t binding, uint32_t arrayElement);
		CopyDescriptorSet& Destination(DescriptorSet set, uint32_t binding, uint32_t arrayElement);
		CopyDescriptorSet& DescriptorCount(uint32_t count);

	private:
		DescriptorSet srcSet;
		uint32_t      srcBinding;
		uint32_t      srcArrayElement;
		DescriptorSet dstSet;
		uint32_t      dstBinding;
		uint32_t      dstArrayElement;
		uint32_t      descriptorCount;
	};
}