#include "DescriptorSetLayoutCreateInfo.hpp"

using namespace vulkan;

DescriptorSetLayoutCreateInfo::DescriptorSetLayoutCreateInfo() :
	Structure(StructureType::DESCRIPTOR_SET_LAYOUT_CREATE_INFO),
	flags(),
	bindingCount(0),
	pBindings(nullptr)
{ }

DescriptorSetLayoutCreateInfo& DescriptorSetLayoutCreateInfo::Flags(DescriptorSetLayoutCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

DescriptorSetLayoutCreateInfo& DescriptorSetLayoutCreateInfo::Bindings(uint32_t count, const DescriptorSetLayoutBinding* pBindings)
{
	this->bindingCount = count;
	this->pBindings = pBindings;
	return *this;
}
