#pragma once

#include "DispatchableHandle.hpp"
#include "PhysicalDevice.hpp"
#include "NonDispatchableHandle.hpp"
#include "Win32SurfaceCreateInfo.hpp"

#include <vector>

namespace vulkan
{
	class Instance : public DispatchableHandle
	{
	public:
		void Destroy();
		std::vector<PhysicalDevice> EnumeratePhysicalDevices() const;
		void Destroy(Surface surface) const;
		Surface CreateWin32Surface(const Win32SurfaceCreateInfo& info) const;
	};
}