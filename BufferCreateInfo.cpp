#include "BufferCreateInfo.hpp"

using namespace vulkan;

BufferCreateInfo::BufferCreateInfo() :
	Structure(StructureType::BUFFER_CREATE_INFO),
	flags(0),
	size(0),
	usage(0),
	sharingMode(SharingMode::EXCLUSIVE),
	queueFamilyIndexCount(0),
	pQueueFamilyIndices(nullptr)
{ }

BufferCreateInfo& BufferCreateInfo::Flags(BufferCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

BufferCreateInfo& BufferCreateInfo::Size(DeviceSize size)
{
	this->size = size;
	return *this;
}

BufferCreateInfo& BufferCreateInfo::Usage(BufferUsageFlags usage)
{
	this->usage = usage;
	return *this;
}

BufferCreateInfo& BufferCreateInfo::Concurrent(uint32_t count, const uint32_t* pQueueFamilyIndices)
{
	this->sharingMode = SharingMode::CONCURRENT;
	this->queueFamilyIndexCount = count;
	this->pQueueFamilyIndices = pQueueFamilyIndices;
	return *this;
}

BufferCreateInfo& BufferCreateInfo::Exclusive()
{
	this->sharingMode = SharingMode::EXCLUSIVE;
	this->queueFamilyIndexCount = 0;
	this->pQueueFamilyIndices = nullptr;
	return *this;
}
