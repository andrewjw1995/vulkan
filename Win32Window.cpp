#include "Win32Window.hpp"

#include <stdexcept>
#include <Windows.h>

using namespace std;
using namespace vulkan;

Win32Window::Win32Window(WindowListener& listener) :
	listener(listener)
{ }

Win32Window::~Win32Window()
{
	DestroyWindow(hwnd);
}

Extent2D Win32Window::Size() const
{
	RECT bounds;
	if (!GetWindowRect(hwnd, &bounds))
		throw runtime_error("Failed to get window bounds");
	return Extent2D(bounds.right - bounds.left, bounds.bottom - bounds.top);
}

Offset2D Win32Window::Position() const
{
	RECT bounds;
	if (!GetWindowRect(hwnd, &bounds))
		throw runtime_error("Failed to get window bounds");
	return Offset2D(bounds.left, bounds.top);
}

bool Win32Window::IsVisible() const
{
	return IsWindowVisible(hwnd) == TRUE;
}

bool Win32Window::IsMaximised() const
{
	return IsZoomed(hwnd) == TRUE;
}

bool Win32Window::IsResizeable() const
{
	return true;
}

bool Win32Window::IsPositionable() const
{
	return true;
}

void Win32Window::Hide()
{
	ShowWindow(hwnd, SW_HIDE);
}

void Win32Window::Show()
{
	ShowWindow(hwnd, SW_SHOW);
}

void Win32Window::Minimize()
{
	ShowWindow(hwnd, SW_MINIMIZE);
}

void Win32Window::Maximize()
{
	ShowWindow(hwnd, SW_MAXIMIZE);
}

void Win32Window::Size(Extent2D size)
{

}

void Win32Window::Position(Offset2D position)
{

}

void Win32Window::ProcessEvents()
{
	MSG message;
	int return_code = PeekMessage(&message, hwnd, 0, 0, PM_REMOVE);
	while (return_code != 0)
	{
		TranslateMessage(&message);
		DispatchMessage(&message);
		return_code = PeekMessage(&message, hwnd, 0, 0, PM_REMOVE);
	}
}

LRESULT __stdcall Win32Window::WindowProcedure(UINT message, WPARAM wparam, LPARAM lparam)
{
	switch (message)
	{
	case WM_CLOSE:
		PostQuitMessage(0);
		return DefWindowProc(hwnd, message, wparam, lparam);

	default:
		return DefWindowProc(hwnd, message, wparam, lparam);
	}
}

LRESULT __stdcall Win32Window::StaticWindowProcedure(HWND hwnd, UINT message, WPARAM wparam, LPARAM lparam)
{
	Win32Window* window;

	if (message == WM_NCCREATE)
	{
		window = static_cast<Win32Window*>(reinterpret_cast<CREATESTRUCT*>(lparam)->lpCreateParams);
		SetWindowLongPtr(hwnd, GWLP_USERDATA, reinterpret_cast<LONG>(window));
		window->hwnd = hwnd;
	}
	else
		window = reinterpret_cast<Win32Window*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

	if (window != nullptr)
	{
		if (window->hwnd != hwnd)
			throw runtime_error("Inconsistent state");
		return window->WindowProcedure(message, wparam, lparam);
	}
	else
		return DefWindowProc(hwnd, message, wparam, lparam);
}
