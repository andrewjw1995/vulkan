#include "Win32WindowFactory.hpp"

#include <stdexcept>
#include <Windows.h>

using namespace std;
using namespace vulkan;


Win32WindowFactory::Win32WindowFactory(Instance instance) :
	instance(instance)
{
	hinstance = GetModuleHandle(NULL);
}

#undef CreateWindow
Win32Window* Win32WindowFactory::CreateWindow(const WindowCreateInfo& config, WindowListener& listener)
{
	WNDCLASSEX wc = {
		/* cbSize */        sizeof(WNDCLASSEX),
		/* style */         CS_HREDRAW | CS_VREDRAW,
		/* lpfnWndProc */   Win32Window::StaticWindowProcedure,
		/* cbClsExtra */    0,
		/* cbWndExtra */    0,
		/* hInstance */     hinstance,
		/* hIcon */         NULL,
		/* hCursor */       NULL,
		/* hbrBackground */ (HBRUSH)GetStockObject(WHITE_BRUSH),
		/* lpszMenuName */  NULL,
		/* lpszClassName */ config.title.c_str(),
		/* hIconSm */       NULL
	};

	if (!RegisterClassEx(&wc))
		throw runtime_error("Failed to create window");

	Win32Window* window = new Win32Window(listener);

	RECT wr = {
		(long)(config.position.x),                     // left
		(long)(config.position.y),                     // top
		(long)(config.position.x + config.size.width), // right
		(long)(config.position.y + config.size.height) // bottom
	};
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);
	HWND handle = CreateWindowEx(
		/* dwExStyle */    0,
		/* lpClassName */  config.title.c_str(),
		/* lpWindowName */ config.title.c_str(),
		/* dwStyle */      WS_OVERLAPPEDWINDOW | WS_VISIBLE | WS_SYSMENU,
		/* x */            wr.left,
		/* y */            wr.top,
		/* nWidth */       wr.right - wr.left,
		/* nHeight */      wr.bottom - wr.top,
		/* hWndParent */   NULL,
		/* hMenu */        NULL,
		/* hInstance */    hinstance,
		/* lpParam */      window
		);

	if (!handle)
	{
		DWORD error = GetLastError();
		char buffer[256];
		FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
			MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), buffer, 256, NULL);

		SetWindowLongPtr(handle, GWLP_USERDATA, reinterpret_cast<LONG>(nullptr));
		delete window;
		throw runtime_error(buffer);
	}

	if (config.visible)
		window->Show();

	return window;
}
