#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineCacheCreateInfo : public Structure
	{
	public:
		PipelineCacheCreateInfo();
		PipelineCacheCreateInfo(size_t initialDataSize, const void* pInitialData);

		PipelineCacheCreateInfo& Flags(PipelineCacheCreateFlags flags);
		PipelineCacheCreateInfo& InitialData(size_t size, const void* pData);

	private:
		PipelineCacheCreateFlags      flags;
		size_t                        initialDataSize;
		const void*                   pInitialData;
	};
}