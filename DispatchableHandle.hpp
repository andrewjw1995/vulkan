#pragma once

#include <cstdint>

namespace vulkan
{
	class DispatchableHandle
	{
	public:
		DispatchableHandle();

		bool IsNull() const;
		void MakeNull();

	private:
		void* value;
	};
}