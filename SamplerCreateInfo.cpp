#include "SamplerCreateInfo.hpp"

using namespace vulkan;

SamplerCreateInfo::SamplerCreateInfo() :
	Structure(StructureType::SAMPLER_CREATE_INFO),
	flags(),
	magFilter(Filter::NEAREST),
	minFilter(Filter::NEAREST),
	mipmapMode(SamplerMipmapMode::NEAREST),
	addressModeU(SamplerAddressMode::REPEAT),
	addressModeV(SamplerAddressMode::REPEAT),
	addressModeW(SamplerAddressMode::REPEAT),
	mipLodBias(0),
	anisotropyEnable(false),
	maxAnisotropy(0),
	compareEnable(false),
	compareOp(CompareOp::NEVER),
	minLod(0),
	maxLod(0),
	borderColor(BorderColor::FLOAT_TRANSPARENT_BLACK),
	unnormalizedCoordinates(false)
{ }

SamplerCreateInfo& SamplerCreateInfo::Flags(SamplerCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::Filters(Filter min, Filter mag)
{
	this->minFilter = min;
	this->magFilter = mag;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::MipmapMode(SamplerMipmapMode mode)
{
	this->mipmapMode = mode;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::AddressModes(SamplerAddressMode u, SamplerAddressMode v, SamplerAddressMode w)
{
	this->addressModeU = u;
	this->addressModeV = v;
	this->addressModeW = w;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::MipLodBias(float bias)
{
	this->mipLodBias = bias;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::EnableAnisotropy(float max)
{
	this->anisotropyEnable = true;
	this->maxAnisotropy = max;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::DisableAnisotropy()
{
	this->anisotropyEnable = false;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::EnableCompare(CompareOp op)
{
	this->compareEnable = true;
	this->compareOp = op;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::DisableCompare()
{
	this->compareEnable = false;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::Lod(float min, float max)
{
	this->minLod = min;
	this->maxLod = max;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::BorderColor(vulkan::BorderColor color)
{
	this->borderColor = color;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::UnnormalizedCoordinates()
{
	this->unnormalizedCoordinates = true;
	return *this;
}

SamplerCreateInfo& SamplerCreateInfo::NormalizedCoordinates()
{
	this->unnormalizedCoordinates = false;
	return *this;
}
