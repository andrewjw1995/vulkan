#pragma once

#include "DeviceSize.hpp"
#include "NonDispatchableHandle.hpp"

namespace vulkan
{
	class DescriptorBufferInfo
	{
	public:
		DescriptorBufferInfo(Buffer buffer, DeviceSize offset, DeviceSize range);

	private:
		Buffer     buffer;
		DeviceSize offset;
		DeviceSize range;
	};
}