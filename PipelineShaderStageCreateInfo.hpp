#pragma once

#include "Flags.hpp"
#include "NonDispatchableHandle.hpp"
#include "SpecializationInfo.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class PipelineShaderStageCreateInfo : public Structure
	{
	public:
		PipelineShaderStageCreateInfo();
		PipelineShaderStageCreateInfo(ShaderStageFlagBits stage, ShaderModule module, const char* pName);

		PipelineShaderStageCreateInfo& Flags(PipelineShaderStageCreateFlags flags);
		PipelineShaderStageCreateInfo& Stage(ShaderStageFlagBits stage);
		PipelineShaderStageCreateInfo& Module(ShaderModule module);
		PipelineShaderStageCreateInfo& Name(const char* pName);
		PipelineShaderStageCreateInfo& SpecializationInfo(const SpecializationInfo* pInfo);
		
	private:
		PipelineShaderStageCreateFlags    flags;
		ShaderStageFlagBits               stage;
		ShaderModule                      module;
		const char*                       pName;
		const vulkan::SpecializationInfo* pSpecializationInfo;
	};
}