#include "SparseMemoryBind.hpp"

using namespace vulkan;

SparseMemoryBind::SparseMemoryBind() :
	resourceOffset(0),
	size(0),
	memory(DeviceMemory()),
	memoryOffset(0),
	flags(0)
{ }

SparseMemoryBind& SparseMemoryBind::ResourceOffset(DeviceSize offset)
{
	this->resourceOffset = offset;
	return *this;
}

SparseMemoryBind& SparseMemoryBind::Size(DeviceSize size)
{
	this->size = size;
	return *this;
}

SparseMemoryBind& SparseMemoryBind::Memory(DeviceMemory memory)
{
	this->memory = memory;
	return *this;
}

SparseMemoryBind& SparseMemoryBind::MemoryOffset(DeviceSize offset)
{
	this->memoryOffset = offset;
	return *this;
}

SparseMemoryBind& SparseMemoryBind::Flags(SparseMemoryBindFlags flags)
{
	this->flags = flags;
	return *this;
}

DeviceSize SparseMemoryBind::ResourceOffset() const
{
	return resourceOffset;
}

DeviceSize SparseMemoryBind::Size() const
{
	return size;
}

DeviceMemory SparseMemoryBind::Memory() const
{
	return memory;
}

DeviceSize SparseMemoryBind::MemoryOffset() const
{
	return memoryOffset;
}

SparseMemoryBindFlags SparseMemoryBind::Flags() const
{
	return flags;
}
