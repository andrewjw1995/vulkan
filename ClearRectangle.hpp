#pragma once

#include "Rectangle.hpp"

namespace vulkan
{
	class ClearRectangle
	{
	public:
		ClearRectangle();
		ClearRectangle(Rectangle rectangle, uint32_t baseArrayLayer, uint32_t layerCount);

		ClearRectangle& Rectangle(Rectangle rectangle);
		ClearRectangle& ArrayLayers(uint32_t base, uint32_t count);

	private:
		vulkan::Rectangle   rect;
		uint32_t    baseArrayLayer;
		uint32_t    layerCount;
	};
}