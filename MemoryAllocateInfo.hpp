#pragma once

#include "DeviceSize.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class MemoryAllocateInfo : public Structure
	{
	public:
		MemoryAllocateInfo();

		MemoryAllocateInfo& AllocationSize(DeviceSize size);
		MemoryAllocateInfo& MemoryTypeIndex(uint32_t index);
		DeviceSize AllocationSize() const;
		uint32_t MemoryTypeIndex() const;

	private:
		DeviceSize allocationSize;
		uint32_t memoryTypeIndex;
	};
}