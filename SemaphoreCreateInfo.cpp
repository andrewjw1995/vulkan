#include "SemaphoreCreateInfo.hpp"

using namespace vulkan;

SemaphoreCreateInfo::SemaphoreCreateInfo() :
	Structure(StructureType::SEMAPHORE_CREATE_INFO),
	flags()
{ }

SemaphoreCreateInfo::SemaphoreCreateInfo(SemaphoreCreateFlags flags) :
	Structure(StructureType::SEMAPHORE_CREATE_INFO),
	flags(flags)
{ }
