#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

struct HINSTANCE__;
struct HWND__;
typedef HINSTANCE__ *HINSTANCE;
typedef HWND__ *HWND;

namespace vulkan
{
	class Win32SurfaceCreateInfo : public Structure
	{
	public:
		Win32SurfaceCreateInfo();

		Win32SurfaceCreateInfo& Flags(Win32SurfaceCreateFlags flags);
		Win32SurfaceCreateInfo& Instance(HINSTANCE hinstance);
		Win32SurfaceCreateInfo& Window(HWND hwnd);

	private:
		Win32SurfaceCreateFlags flags;
		HINSTANCE               hinstance;
		HWND                    hwnd;
	};
}