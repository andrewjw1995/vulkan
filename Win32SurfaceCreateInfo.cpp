#include "Win32SurfaceCreateInfo.hpp"

using namespace vulkan;

Win32SurfaceCreateInfo::Win32SurfaceCreateInfo() :
	Structure(StructureType::WIN32_SURFACE_CREATE_INFO_KHR),
	flags(),
	hinstance(nullptr),
	hwnd(nullptr)
{ }

Win32SurfaceCreateInfo& Win32SurfaceCreateInfo::Flags(Win32SurfaceCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

Win32SurfaceCreateInfo& Win32SurfaceCreateInfo::Instance(HINSTANCE hinstance)
{
	this->hinstance = hinstance;
	return *this;
}

Win32SurfaceCreateInfo& Win32SurfaceCreateInfo::Window(HWND hwnd)
{
	this->hwnd = hwnd;
	return *this;
}
