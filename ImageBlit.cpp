#include "ImageBlit.hpp"

using namespace vulkan;

ImageBlit::ImageBlit() :
	srcSubresource(),
	srcOffsets(),
	dstSubresource(),
	dstOffsets()
{ }

ImageBlit& ImageBlit::Source(ImageSubresourceLayers subresource, Offset3D start, Offset3D end)
{
	this->srcSubresource = subresource;
	this->srcOffsets[0] = start;
	this->srcOffsets[1] = end;
	return *this;
}

ImageBlit& ImageBlit::Destination(ImageSubresourceLayers subresource, Offset3D start, Offset3D end)
{
	this->dstSubresource = subresource;
	this->dstOffsets[0] = start;
	this->dstOffsets[1] = end;
	return *this;
}
