#pragma once

#include "Flags.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class FenceCreateInfo : public Structure
	{
	public:
		FenceCreateInfo();
		FenceCreateInfo(FenceCreateFlags flags);

		FenceCreateInfo& Flags(FenceCreateFlags flags);

	private:
		FenceCreateFlags flags;
	};
}