#include "MemoryRequirements.hpp"

using namespace vulkan;

DeviceSize MemoryRequirements::Size() const
{
	return size;
}

DeviceSize MemoryRequirements::Alignment() const
{
	return alignment;
}

uint32_t MemoryRequirements::MemoryTypeBits() const
{
	return memoryTypeBits;
}
