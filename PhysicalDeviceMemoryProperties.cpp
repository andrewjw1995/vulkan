#include "PhysicalDeviceMemoryProperties.hpp"

using namespace std;
using namespace vulkan;

vector<MemoryType> PhysicalDeviceMemoryProperties::MemoryTypes() const
{
	vector<MemoryType> buffer(memoryTypeCount);
	for (uint32_t i = 0; i < memoryTypeCount; i++)
		buffer[i] = memoryTypes[i];
	return buffer;
}

vector<MemoryHeap> PhysicalDeviceMemoryProperties::MemoryHeaps() const
{
	vector<MemoryHeap> buffer(memoryHeapCount);
	for (uint32_t i = 0; i < memoryHeapCount; i++)
		buffer[i] = memoryHeaps[i];
	return buffer;
}

uint32_t PhysicalDeviceMemoryProperties::GetMemoryType(MemoryPropertyFlags flags, uint32_t memoryTypeBits) const
{
	if (uint32_t(flags) == 0)
		flags = MemoryPropertyFlags(UINT32_MAX);

	for (uint32_t index = 0; index < memoryTypeCount; index++)
	{
		if (((1 << index) & memoryTypeBits) == 0)
			continue;
		if (flags & memoryTypes[index].Flags())
			return index;
	}
	return MAX_MEMORY_TYPES;
}
