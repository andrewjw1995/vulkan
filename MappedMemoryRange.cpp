#include "MappedMemoryRange.hpp"

using namespace vulkan;

MappedMemoryRange::MappedMemoryRange() :
	Structure(StructureType::MAPPED_MEMORY_RANGE),
	memory(DeviceMemory()),
	offset(0),
	size(0)
{ }

MappedMemoryRange::MappedMemoryRange(DeviceMemory memory, DeviceSize offset, DeviceSize size) :
	Structure(StructureType::MAPPED_MEMORY_RANGE),
	memory(memory),
	offset(offset),
	size(size)
{ }

MappedMemoryRange& MappedMemoryRange::Memory(DeviceMemory memory)
{
	this->memory = memory;
	return *this;
}

MappedMemoryRange& MappedMemoryRange::Offset(DeviceSize offset)
{
	this->offset = offset;
	return *this;
}

MappedMemoryRange& MappedMemoryRange::Size(DeviceSize size)
{
	this->size = size;
	return *this;
}

DeviceMemory MappedMemoryRange::Memory() const
{
	return memory;
}

DeviceSize MappedMemoryRange::Offset() const
{
	return offset;
}

DeviceSize MappedMemoryRange::Size() const
{
	return size;
}
