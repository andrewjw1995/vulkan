#include "BufferViewCreateInfo.hpp"

using namespace vulkan;

BufferViewCreateInfo::BufferViewCreateInfo() :
	Structure(StructureType::BUFFER_VIEW_CREATE_INFO),
	flags(),
	buffer(),
	format(Format::UNDEFINED),
	offset(0),
	range(0)
{ }

BufferViewCreateInfo& BufferViewCreateInfo::Flags(BufferViewCreateFlags flags)
{
	this->flags = flags;
	return *this;
}

BufferViewCreateInfo& BufferViewCreateInfo::Buffer(vulkan::Buffer buffer)
{
	this->buffer = buffer;
	return *this;
}

BufferViewCreateInfo& BufferViewCreateInfo::Format(vulkan::Format format)
{
	this->format = format;
	return *this;
}

BufferViewCreateInfo& BufferViewCreateInfo::Offset(DeviceSize offset)
{
	this->offset = offset;
	return *this;
}

BufferViewCreateInfo& BufferViewCreateInfo::Range(DeviceSize range)
{
	this->range = range;
	return *this;
}
