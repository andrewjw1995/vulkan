#pragma once

#include "NonDispatchableHandle.hpp"
#include "SparseBufferMemoryBindInfo.hpp"
#include "SparseImageMemoryBindInfo.hpp"
#include "SparseImageOpaqueMemoryBindInfo.hpp"
#include "Structure.hpp"

namespace vulkan
{
	class BindSparseInfo : public Structure
	{
	public:
		BindSparseInfo();

		BindSparseInfo& WaitSemaphores(uint32_t count, const Semaphore* pSemaphores);
		BindSparseInfo& BufferBinds(uint32_t count, const SparseBufferMemoryBindInfo* pBinds);
		BindSparseInfo& ImageOpaqueBinds(uint32_t count, const SparseImageOpaqueMemoryBindInfo* pBinds);
		BindSparseInfo& ImageBinds(uint32_t count, const SparseImageMemoryBindInfo* pBinds);
		BindSparseInfo& SignalSemaphores(uint32_t count, const Semaphore* pSemaphores);

	private:
		uint32_t                                  waitSemaphoreCount;
		const Semaphore*                          pWaitSemaphores;
		uint32_t                                  bufferBindCount;
		const SparseBufferMemoryBindInfo*         pBufferBinds;
		uint32_t                                  imageOpaqueBindCount;
		const SparseImageOpaqueMemoryBindInfo*    pImageOpaqueBinds;
		uint32_t                                  imageBindCount;
		const SparseImageMemoryBindInfo*          pImageBinds;
		uint32_t                                  signalSemaphoreCount;
		const Semaphore*                          pSignalSemaphores;
	};
}