#include "ImageSubresource.hpp"

using namespace vulkan;

ImageSubresource::ImageSubresource() :
	aspectMask(0),
	mipLevel(0),
	arrayLayer(0)
{ }

ImageSubresource::ImageSubresource(ImageAspectFlags aspectMask, uint32_t mipLevel, uint32_t arrayLayer) :
	aspectMask(aspectMask),
	mipLevel(mipLevel),
	arrayLayer(arrayLayer)
{ }

ImageSubresource& ImageSubresource::AspectMask(ImageAspectFlags mask)
{
	aspectMask = mask;
	return *this;
}

ImageSubresource& ImageSubresource::MipLevel(uint32_t level)
{
	mipLevel = level;
	return *this;
}

ImageSubresource& ImageSubresource::ArrayLayer(uint32_t layer)
{
	arrayLayer = layer;
	return *this;
}

ImageAspectFlags ImageSubresource::AspectMask() const
{
	return aspectMask;
}

uint32_t ImageSubresource::MipLevel() const
{
	return mipLevel;
}

uint32_t ImageSubresource::ArrayLayer() const
{
	return arrayLayer;
}
