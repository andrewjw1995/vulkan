#include "CommandBufferAllocateInfo.hpp"

using namespace vulkan;

CommandBufferAllocateInfo::CommandBufferAllocateInfo(CommandPool commandPool, CommandBufferLevel level, uint32_t commandBufferCount) :
	Structure(StructureType::COMMAND_BUFFER_ALLOCATE_INFO),
	commandPool(commandPool),
	level(level),
	commandBufferCount(commandBufferCount)
{ }

uint32_t CommandBufferAllocateInfo::CommandBufferCount() const
{
	return this->commandBufferCount;
}
