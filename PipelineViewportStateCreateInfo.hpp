#pragma once

#include "Flags.hpp"
#include "Rectangle.hpp"
#include "Structure.hpp"
#include "Viewport.hpp"

namespace vulkan
{
	class PipelineViewportStateCreateInfo : public Structure
	{
	public:
		PipelineViewportStateCreateInfo();

		PipelineViewportStateCreateInfo& Flags(PipelineViewportStateCreateFlags flags);
		PipelineViewportStateCreateInfo& Viewports(uint32_t count, const Viewport* pViewports);
		PipelineViewportStateCreateInfo& Scissors(uint32_t count, const Rectangle* pScissors);

	private:
		PipelineViewportStateCreateFlags      flags;
		uint32_t                              viewportCount;
		const Viewport*                       pViewports;
		uint32_t                              scissorCount;
		const Rectangle*                      pScissors;
	};
}