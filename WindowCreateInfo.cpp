#include "WindowCreateInfo.hpp"

using namespace vulkan;

WindowCreateInfo& WindowCreateInfo::Title(std::string title)
{
	this->title = title;
	return *this;
}

WindowCreateInfo& WindowCreateInfo::Position(Offset2D position)
{
	this->position = position;
	return *this;
}

WindowCreateInfo& WindowCreateInfo::Size(Extent2D size)
{
	this->size = size;
	return *this;
}

WindowCreateInfo& WindowCreateInfo::Visible(bool visible)
{
	this->visible = visible;
	return *this;
}

WindowCreateInfo& WindowCreateInfo::Maximizable(bool maximizable)
{
	this->maximizable = maximizable;
	return *this;
}

WindowCreateInfo& WindowCreateInfo::Resizable(bool resizable)
{
	this->resizable = resizable;
	return *this;
}

WindowCreateInfo& WindowCreateInfo::Positionable(bool positionable)
{
	this->positionable = positionable;
	return *this;
}
