#include "SparseBufferMemoryBindInfo.hpp"

using namespace vulkan;

SparseBufferMemoryBindInfo::SparseBufferMemoryBindInfo() :
	buffer(Buffer()),
	bindCount(0),
	pBinds(nullptr)
{ }

SparseBufferMemoryBindInfo& SparseBufferMemoryBindInfo::Buffer(vulkan::Buffer buffer)
{
	this->buffer = buffer;
	return *this;
}

SparseBufferMemoryBindInfo& SparseBufferMemoryBindInfo::Binds(uint32_t count, const SparseMemoryBind* pBinds)
{
	this->bindCount = count;
	this->pBinds = pBinds;
	return *this;
}

vulkan::Buffer SparseBufferMemoryBindInfo::Buffer() const
{
	return buffer;
}

uint32_t SparseBufferMemoryBindInfo::BindCount() const
{
	return bindCount;
}

const SparseMemoryBind* SparseBufferMemoryBindInfo::Binds() const
{
	return pBinds;
}
