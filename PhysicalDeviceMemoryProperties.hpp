#pragma once

#include "MemoryHeap.hpp"
#include "MemoryType.hpp"

#include <vector>

namespace vulkan
{
	class PhysicalDeviceMemoryProperties
	{
	public:
		static const unsigned int MAX_MEMORY_HEAPS = 16;
		static const unsigned int MAX_MEMORY_TYPES = 32;

		std::vector<MemoryType> MemoryTypes() const;
		std::vector<MemoryHeap> MemoryHeaps() const;
		uint32_t GetMemoryType(MemoryPropertyFlags flags, uint32_t memoryTypeBits) const;

	private:
		uint32_t      memoryTypeCount;
		MemoryType    memoryTypes[MAX_MEMORY_TYPES];
		uint32_t      memoryHeapCount;
		MemoryHeap    memoryHeaps[MAX_MEMORY_HEAPS];
	};
}