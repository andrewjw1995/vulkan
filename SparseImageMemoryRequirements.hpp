#pragma once

#include "DeviceSize.hpp"
#include "SparseImageFormatProperties.hpp"

namespace vulkan
{
	class SparseImageMemoryRequirements
	{
	public:
		SparseImageFormatProperties    FormatProperties() const;
		uint32_t                       MipTailFirstLod() const;
		DeviceSize                     MipTailSize() const;
		DeviceSize                     MipTailOffset() const;
		DeviceSize                     MipTailStride() const;

	private:
		SparseImageFormatProperties    formatProperties;
		uint32_t                       imageMipTailFirstLod;
		DeviceSize                     imageMipTailSize;
		DeviceSize                     imageMipTailOffset;
		DeviceSize                     imageMipTailStride;
	};
}