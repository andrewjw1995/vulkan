#pragma once

#include <cstdint>

namespace vulkan
{
	class SpecializationMapEntry
	{
	public:
		SpecializationMapEntry(uint32_t constantID, uint32_t offset, size_t size);

	private:
		uint32_t    constantID;
		uint32_t    offset;
		size_t      size;
	};
}