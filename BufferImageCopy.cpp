#include "BufferImageCopy.hpp"

using namespace vulkan;

BufferImageCopy::BufferImageCopy() :
	bufferOffset(0),
	bufferRowLength(0),
	bufferImageHeight(0),
	imageSubresource(),
	imageOffset(),
	imageExtent()
{ }

BufferImageCopy& BufferImageCopy::Buffer(DeviceSize offset, uint32_t rowLength, uint32_t height)
{
	this->bufferOffset = offset;
	this->bufferRowLength = rowLength;
	this->bufferImageHeight = height;
	return *this;
}

BufferImageCopy& BufferImageCopy::Image(ImageSubresourceLayers subresource, Offset3D offset, Extent3D extent)
{
	this->imageSubresource = subresource;
	this->imageOffset = offset;
	this->imageExtent = extent;
	return *this;
}
