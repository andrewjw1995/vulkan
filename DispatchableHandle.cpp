#include "DispatchableHandle.hpp"

using namespace vulkan;

DispatchableHandle::DispatchableHandle() :
	value(nullptr)
{ }

bool DispatchableHandle::IsNull() const
{
	return value == nullptr;
}

void DispatchableHandle::MakeNull()
{
	value = nullptr;
}
