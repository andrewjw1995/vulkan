#include "VertexInputBindingDescription.hpp"

using namespace vulkan;

VertexInputBindingDescription::VertexInputBindingDescription(uint32_t binding, uint32_t stride, VertexInputRate inputRate) :
	binding(binding),
	stride(stride),
	inputRate(inputRate)
{ }